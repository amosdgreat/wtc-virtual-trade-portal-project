<?php
/**
 * PHPB2B cache file, DO NOT change me!
 * Created: Jul 29, 2020, 10:50
 * Id: 0479e40972d7726fca782eac4b589ad9
 */

$_PB_CACHE['brand'] = array (
  'a' => 
  array (
    0 => 
    array (
      'id' => '32',
      'name' => 'Alcatel',
      'picture' => 'sample/brand/10.jpg',
    ),
  ),
  'f' => 
  array (
    0 => 
    array (
      'id' => '5',
      'name' => 'Philips',
      'picture' => 'sample/brand/5.jpg',
    ),
  ),
  'l' => 
  array (
    0 => 
    array (
      'id' => '2',
      'name' => 'LG',
      'picture' => 'sample/brand/2.jpg',
    ),
  ),
  'm' => 
  array (
    0 => 
    array (
      'id' => '3',
      'name' => 'Motorola',
      'picture' => 'sample/brand/3.jpg',
    ),
  ),
  'n' => 
  array (
    0 => 
    array (
      'id' => '4',
      'name' => 'Nokia',
      'picture' => 'sample/brand/4.jpg',
    ),
  ),
  'p' => 
  array (
    0 => 
    array (
      'id' => '1',
      'name' => 'palm',
      'picture' => 'sample/brand/1.jpg',
    ),
  ),
  's' => 
  array (
    0 => 
    array (
      'id' => '28',
      'name' => 'Sony Ericsson',
      'picture' => 'sample/brand/8.jpg',
    ),
    1 => 
    array (
      'id' => '27',
      'name' => 'Matsushita',
      'picture' => 'sample/brand/7.jpg',
    ),
    2 => 
    array (
      'id' => '26',
      'name' => 'Samsung',
      'picture' => 'sample/brand/6.jpg',
    ),
  ),
  'x' => 
  array (
    0 => 
    array (
      'id' => '29',
      'name' => 'Siemens',
      'picture' => 'sample/brand/9.jpg',
    ),
  ),
)
?>