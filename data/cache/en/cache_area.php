<?php
/**
 * PHPB2B cache file, DO NOT change me!
 * Modified: Aug 20, 2020, 11:37
 * Id: 7ec0c45b25c9c5f7d1f4c92c34926aa5
 */

$_PB_CACHE['area'] = array (
  1 => 
  array (
    51 => 'South Africa',
    37 => 'Denmark',
    36 => 'Turkey',
    35 => 'Romania',
    34 => 'Iceland',
    33 => 'Czech Republic',
    32 => 'Switzerland',
    31 => 'Portugal',
    30 => 'Greece',
    29 => 'Bulgaria',
    28 => 'Sweden',
    38 => 'Italy',
    39 => 'Russia',
    40 => 'Ukraine',
    50 => 'Egypt',
    49 => 'Saudi Arabia',
    48 => 'Emirates',
    47 => 'United Arab ',
    46 => 'Syria',
    45 => 'Israel',
    44 => 'United Kingdom',
    43 => 'Spain',
    42 => 'Netherlands',
    41 => 'France',
    27 => 'Poland',
    26 => 'Germany',
    25 => 'Belgium',
    11 => 'Philippines',
    10 => 'Japan',
    9 => 'Hong Kong',
    8 => 'Taiwan',
    7 => 'Pakistan',
    6 => 'Iran',
    5 => 'China',
    4 => 'South Korea',
    3 => 'New Zealand',
    2 => 'Indonesia',
    12 => 'Thailand',
    13 => 'India',
    14 => 'Malaysia',
    24 => 'United States',
    23 => 'Mexico',
    22 => 'Chile',
    21 => 'Brazil',
    20 => 'Peru',
    19 => 'Colombia',
    18 => 'Canada',
    17 => 'Argentina',
    16 => 'Vietnam',
    15 => 'Singapore',
    1 => 'Australia',
  ),
);


?>