<?php
/**
 * PHPB2B cache file, DO NOT change me!
 * Modified: Sep 16, 2020, 15:23
 * Id: 8caa21e4a0ebf47f096d589b07edbd89
 */

$_PB_CACHE['setting'] = array (
  'site_name' => 'WTC Virtual Trade Portal',
  'site_title' => 'WTC Virtual Trade Portal - Powered By PHPB2B',
  'site_banner_word' => 'The most professional e-commerce website industry',
  'company_name' => 'Web site copyright',
  'site_url' => 'http://localhost/wtc_b2b_new/',
  'icp_number' => 'ICP Number',
  'service_tel' => '0302631437',
  'sale_tel' => '0302631437',
  'service_qq' => '0302631437',
  'service_msn' => 'info@wtcaccra.com',
  'service_email' => 'info@wtcaccra.com',
  'cp_picture' => '0',
  'register_picture' => '0',
  'login_picture' => '0',
  'vispost_auth' => '1',
  'watermark' => '1',
  'watertext' => 'http://localhost/wtc_b2b_new/',
  'watercolor' => '#990000',
  'add_market_check' => '1',
  'regcheck' => '0',
  'vis_post' => '1',
  'vis_post_check' => '1',
  'sell_logincheck' => '1',
  'buy_logincheck' => '0',
  'install_dateline' => '1596019810',
  'last_backup' => '1596019821',
  'reg_filename' => 'register.php',
  'new_userauth' => '0',
  'post_filename' => 'post.php',
  'forbid_ip' => '',
  'ip_reg_sep' => '0',
  'backup_dir' => 'cAJTfy',
  'backup_type' => '1',
  'register_type' => 'open_common_reg',
  'auth_key' => 't8bdx!3q',
  'keyword_bidding' => '0',
  'passport_support' => '0',
  'site_logo' => 'images/logo.jpg',
  'languages' => 'a:1:{s:2:"en";a:2:{s:5:"title";s:7:"English";s:3:"img";s:21:"languages/en/icon.gif";}}',
  'main_cache' => '0',
  'member_cache' => '0',
  'space_cache' => '0',
  'label_cache' => '0',
  'main_cache_lifetime' => '3600',
  'main_cache_check' => '0',
  'auth_protocol' => 'TLS',
  'testemail' => 'buyitgh@gmail.com',
  'mail' => 'a:11:{s:9:"send_mail";s:1:"2";s:13:"auth_protocol";s:3:"TLS";s:11:"smtp_server";s:18:"smtp.office365.com";s:9:"smtp_port";s:3:"587";s:9:"smtp_auth";s:1:"1";s:9:"mail_from";s:20:"amensah@wtcaccra.com";s:12:"mail_fromwho";s:11:"Amos Mensah";s:13:"auth_username";s:20:"amensah@wtcaccra.com";s:13:"auth_password";s:8:"M$3sah2#";s:14:"mail_delimiter";s:1:"0";s:15:"sendmail_silent";s:1:"1";}',
  'capt_auth' => 43,
  'navs' => 
  array (
    1 => 
    array (
      'nav' => '<a href="index.php" title="Home" id="mn_1"><span>Home</span></a>',
      'level' => '1',
    ),
    2 => 
    array (
      'nav' => '<a href="buy/" title="Buyer" id="mn_2"><span>Buyer</span></a>',
      'level' => '2',
    ),
    3 => 
    array (
      'nav' => '<a href="sell/" title="Seller" id="mn_3"><span>Seller</span></a>',
      'level' => '3',
    ),
    4 => 
    array (
      'nav' => '<a href="offer/list.php?typeid=7&navid=4" title="Wholesale" id="mn_4"><span>Wholesale</span></a>',
      'level' => '4',
    ),
    5 => 
    array (
      'nav' => '<a href="product/price.php" title="Price" id="mn_5"><span>Price</span></a>',
      'level' => '5',
    ),
    10 => 
    array (
      'nav' => '<a href="brand.php" title="Brands" id="mn_10"><span>Brands</span></a>',
      'level' => '6',
    ),
    6 => 
    array (
      'nav' => '<a href="market/quote.php" title="Quote" id="mn_6"><span>Quote</span></a>',
      'level' => '7',
    ),
    7 => 
    array (
      'nav' => '<a href="market/index.php" title="Market" id="mn_7"><span>Market</span></a>',
      'level' => '8',
    ),
    8 => 
    array (
      'nav' => '<a href="fair/index.php" title="Fair" id="mn_8"><span>Fair</span></a>',
      'level' => '9',
    ),
    9 => 
    array (
      'nav' => '<a href="hr/index.php" title="Jobs" id="mn_9"><span>Jobs</span></a>',
      'level' => '10',
    ),
    11 => 
    array (
      'nav' => '<a href="dict/" title="Dictionay" id="mn_11"><span>Dictionay</span></a>',
      'level' => '11',
    ),
  ),
);


?>