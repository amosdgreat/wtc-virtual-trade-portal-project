<?php /* Smarty version 2.6.18, created on 2020-09-16 15:39:51
         compiled from space.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'company', 'space.html', 17, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_diy_website'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrap clearfix">
<div class="sidebar">
   <div class="sidebar_menu">
	 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
   </div>
</div>
 <div class="main_content">
 <div class="blank"></div>
 <div class="offer_banner"><img src="images/offer_01.gif" /></div>
 <div class="offer_info_title"><h2><?php echo $this->_tpl_vars['page_title']; ?>
</h2></div>
  <table class="bggray">
	<tr>
	  <td class="gray" style="width:545px;"><span class="orange"><?php echo $this->_tpl_vars['_friendly_tip']; ?>
</span><?php echo $this->_tpl_vars['_space_name']; ?>
<?php echo $this->_tpl_vars['_change_style']; ?>
</td>
	  <td style="width:90px;"><a href="javascript:;" onclick="$('#SpaceNameModify').toggle();" class="btn_publish"><?php echo $this->_tpl_vars['_modify']; ?>
<?php echo $this->_tpl_vars['_space_name']; ?>
<?php echo $this->_tpl_vars['_name']; ?>
</a> </td>
      <td style="width:100px;"> <?php $this->_tag_stack[] = array('company', array('id' => ($this->_tpl_vars['COMPANYINFO']['id']))); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?><a href="<?php echo $this->_tpl_vars['COMPANYINFO']['space_url']; ?>
" target="_blank" class="btn_publish" ><?php echo $this->_tpl_vars['_click_preview']; ?>
</a><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></td>
	</tr>
	<tbody id="SpaceNameModify" style="display:none;">
	<form name="space_form" action="space.php" method="post">
	<tr>
	  <td  colspan="3" class="gray"><?php echo $this->_tpl_vars['_space_name']; ?>
<?php echo $this->_tpl_vars['_name_n']; ?>
<input type="text" name="data[space_name]" id="dataSpaceName" value="<?php echo $this->_tpl_vars['COMPANYINFO']['cache_spacename']; ?>
" style="text-align:center;ime-mode:disabled;width:350px;" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" maxlength="100">&nbsp;<input type="submit" name="updateSpaceName" value="<?php echo $this->_tpl_vars['_submit_changes']; ?>
">&nbsp;<?php echo $this->_tpl_vars['_left_bracket']; ?>
<?php echo $this->_tpl_vars['_space_name']; ?>
<?php echo $this->_tpl_vars['_english_number_portfolio']; ?>
<?php echo $this->_tpl_vars['_right_bracket']; ?>

	  </td>
      
	</tr>
	</form>
	</tbody>
  </table>      
  <form name="stylefrm" action="space.php" method="post">
  <input type="hidden" name="formhash" value="<?php echo $this->_tpl_vars['formhash']; ?>
">
  <table class="temp_style">
	<tr>
	<?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['templet'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['templet']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['templet']['iteration']++;
?>
	  <td>
	  <label for="style_<?php echo $this->_tpl_vars['item']['id']; ?>
"><img height="155" src="<?php echo $this->_tpl_vars['item']['picture']; ?>
" disabled="disabled"></label>
		<br>
		<label for="style_<?php echo $this->_tpl_vars['item']['id']; ?>
"><input type="radio" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" name="data[member][styleid]" id="style_<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['id'] == $this->_tpl_vars['templet_id']): ?>checked<?php endif; ?>><?php echo $this->_tpl_vars['item']['title']; ?>
</label>
	</td>
	<?php if (!($this->_foreach['templet']['iteration'] % 4)): ?>
	</tr><tr>
	<?php endif; ?>
	<?php endforeach; else: ?>
	<td><?php echo $this->_tpl_vars['_no_provide_template']; ?>
</td>
	<?php endif; unset($_from); ?>
	</tr>
	</table>
	  <table class="trade_line">
		<tr>
		  <td height="1" background="images/index_trade_line.gif"></td>
		</tr>
	   <tr align="center" valign="bottom">
	  <td height="40">
	  <input name="save" type="submit" id="Save" value="<?php echo $this->_tpl_vars['_choose_submit']; ?>
">
	  </td>
	</tr>
  </table>
  </form>
	<table class="attentions">
		<tr>
		<?php echo $this->_tpl_vars['_change_style_tips']; ?>

		</tr>
	</table>
</div>
</div>
<script type="text/javascript"> 
var lb=document.getElementsByTagName('label'); 
for (i=0;i<lb.length;i++) { 
	lb[i].onclick=function () { 
		var lbfor=this.getAttribute('for')?this.getAttribute('for'):this.getAttribute('HTMLfor')+''; 
		document.getElementById(lbfor).click(); 
		document.getElementById(lbfor).focus(); 
	}             
} 
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>