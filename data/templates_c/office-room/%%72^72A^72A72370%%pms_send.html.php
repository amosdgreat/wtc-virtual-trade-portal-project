<?php /* Smarty version 2.6.18, created on 2020-09-16 17:43:52
         compiled from pms_send.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_send_message'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrap clearfix">
    <div class="sidebar">
       <div class="sidebar_menu">
         <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
       </div>
    </div>
     <div class="main_content">
     <div class="blank"></div>
	 <div class="offer_banner"><img src="images/offer_01.gif" /></div>
     <div class="offer_info_title"><h2><?php echo $this->_tpl_vars['page_title']; ?>
</h2></div>
     <div class="hint"><?php echo $this->_tpl_vars['_must_input_with_star']; ?>
</div>
	  <form name="sendmsgfrm" action="pms.php" method="post">
	  <input type="hidden" name="formhash" value="<?php echo $this->_tpl_vars['formhash']; ?>
">
       <table class="offer_info_content">
          <tr>
            <th class="circle_left"><font color="#FF6600">*</font><?php echo $this->_tpl_vars['_send_to']; ?>
</th>
             <td class="circle_right"><input name="to" type="text" id="To" size="30" value="<?php echo $this->_tpl_vars['item']['to']; ?>
"><font color="#666666"><?php echo $this->_tpl_vars['_send_other_username']; ?>
</font></td>
          </tr>
          <tr>
            <th> <font color="#FF6600">*</font><?php echo $this->_tpl_vars['_theme_n']; ?>
</th>
            <td><input name="pms[title]" type="text" id="title" size="30" value=""></td>
          </tr>
          <tr>
             <th><font color="#FF6600">*</font><?php echo $this->_tpl_vars['_content']; ?>
</th>
             <td><textarea name="pms[content]" cols="50" rows="6" wrap="VIRTUAL"></textarea></td>
          </tr>
          <tr>
             <th class="circle_bottomleft"></th>
             <td class="circle_bottomright"><input name="send" type=submit id="Send" value=" <?php echo $this->_tpl_vars['_send']; ?>
 ">&nbsp;&nbsp;<input name=reset type=reset id="reset" value=" <?php echo $this->_tpl_vars['_cancel']; ?>
 "></td>
          </tr>
           
        </table>
	  </form>
 </div>
 </div> 
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>