<?php /* Smarty version 2.6.18, created on 2020-08-03 15:25:32
         compiled from default%5Chelp.index.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', ($this->_tpl_vars['nav_id'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="contentbox">
		<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
		<div class="blank6"></div>
    <div class="content clearfix">
                <div class="fl help_l">
                <div class="help_ltop">
                     <span class="help_top_left"></span><span class="help_top_right"></span>
                </div>
                <div class="help_lcont">
                    <div class="help_telborder">
                        <div class="help_tel">
                            <div class="help_telcont"><?php echo $this->_tpl_vars['_help_center']; ?>

                            <p class="tar"><?php echo $this->_tpl_vars['service_tel']; ?>
</p>
                            </div>
                        </div>
                    </div>
                    <?php $_from = $this->_tpl_vars['Helptypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['Helptypes'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['Helptypes']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['helptype']):
        $this->_foreach['Helptypes']['iteration']++;
?>
                    <h2 class="tar helpbor_01"><a href="help/index.php?typeid=<?php echo $this->_tpl_vars['helptype']['id']; ?>
"><?php echo $this->_tpl_vars['helptype']['name']; ?>
</a></h2>
                    <ul>
                        <?php if ($this->_tpl_vars['helptype']['sub']): ?>
                        <?php $_from = $this->_tpl_vars['helptype']['sub']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['sub_helptype'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['sub_helptype']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['sub_helptype']):
        $this->_foreach['sub_helptype']['iteration']++;
?>
                        <li class="help_lcontnav"><a href="help/index.php?typeid=<?php echo $this->_tpl_vars['sub_helptype']['id']; ?>
"><?php echo $this->_tpl_vars['sub_helptype']['name']; ?>
</a></li>
                        <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; endif; unset($_from); ?>
                </div>
                <div class="help_lcontnavs"></div>
                </div>
                <div class="fr help_r clearfix">
                   
                    <div class="help_rcontbg">
                        <form method="get" action="help/index.php" >
                        <input type="hidden" name="search" value="search"/>
                         <p class="help_rsearch">
                            <?php echo $this->_tpl_vars['_search']; ?>
<?php echo $this->_tpl_vars['_help']; ?>
<?php echo $this->_tpl_vars['_colon']; ?>
<input name="q" value="" type="text" /><input name="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
" type="submit" class="btn_submit_w45 search_en" />
                          </p>
                        </form>
                       
                        <p class="help_rbg">
                        <?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['helps'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['helps']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['helps']['iteration']++;
?>
                        <?php echo $this->_foreach['helps']['iteration']; ?>
.<a href="help/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['title']; ?>
</a><br /> 
                        <?php endforeach; endif; unset($_from); ?>
                        </p>
                    </div>
                </div>
            </div>

	</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>