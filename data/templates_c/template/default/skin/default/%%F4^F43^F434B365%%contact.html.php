<?php /* Smarty version 2.6.18, created on 2020-09-16 15:42:50
         compiled from contact.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'mailto', 'contact.html', 17, false),array('function', 'plugin', 'contact.html', 21, false),)), $this); ?>
<?php $this->assign('PageTitle', ($this->_tpl_vars['_contact_us'])); ?>
<?php $this->assign('cur', 'space_contact'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "banner.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<div class="content">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "leftbar.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<div class="rightbarcontact">
			<div class="playercontact">
			<div class="playerheadcontact"><span class="playlistcontact"><?php echo $this->_tpl_vars['_contact_us']; ?>
</span><span class="player_head_concontact"></span><img src="images/contactus_13.jpg" /></div>
			<div class="clear"></div>
			<div class="contact_list">
				<p><span>Company name:</span><?php echo $this->_tpl_vars['COMPANY']['name']; ?>
</p>
				<p><span>Contact:</span><?php echo $this->_tpl_vars['COMPANY']['link_man']; ?>
</p>
				<p><span>phone:</span><?php echo $this->_tpl_vars['COMPANY']['tel']; ?>
</p>
				<p><span>fax:</span><?php echo $this->_tpl_vars['COMPANY']['fax']; ?>
</p>
				<p><span>e-mail:</span><?php echo smarty_function_mailto(array('address' => ($this->_tpl_vars['COMPANY']['email']),'encode' => 'javascript','text' => ($this->_tpl_vars['COMPANY']['email'])), $this);?>
</p>
				<p><span>Address:</span><?php echo $this->_tpl_vars['COMPANY']['address']; ?>
</p>
				<p><span>Postal Code:</span><?php echo $this->_tpl_vars['COMPANY']['zipcode']; ?>
</p>
				<p><span>URL:</span><?php echo $this->_tpl_vars['COMPANY']['site_url']; ?>
</p>
				<?php echo smarty_function_plugin(array('name' => 'googlemap','lng' => ($this->_tpl_vars['COMPANY']['map_longitude']),'lat' => ($this->_tpl_vars['COMPANY']['map_latitude']),'title' => ($this->_tpl_vars['COMPANY']['name'])), $this);?>

			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>