<?php /* Smarty version 2.6.18, created on 2020-07-29 12:07:12
         compiled from index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'index.html', 11, false),)), $this); ?>
<?php $this->assign('PageTitle', ($this->_tpl_vars['_home_page'])); ?>
<?php $this->assign('cur', 'space_index'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "banner.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<div class="content">
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "leftbar.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<div class="rightbar">
			<div class="welcome">
				<img src="<?php echo $this->_tpl_vars['COMPANY']['logo']; ?>
"/>
				<p><span class="title"><?php echo $this->_tpl_vars['_you_are_welcome']; ?>
<?php echo $this->_tpl_vars['COMPANY']['name']; ?>
</span><br/><br/>  <?php echo ((is_array($_tmp=$this->_tpl_vars['COMPANY']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 300) : smarty_modifier_truncate($_tmp, 300)); ?>
<br/><br/><span class="more"><a title="更多介绍" href="<?php echo $this->_tpl_vars['Menus']['intro']; ?>
">更多公司情况>></a></span></p>
				<div class="clear"></div>
			</div>
			<br clear="all"/>
			<?php if ($this->_tpl_vars['Products']): ?>
			<div class="player">
				<div class="playerhead"><span class="playlist">产品展示</span><span class="playerheadcon"><a href="<?php echo $this->_tpl_vars['Menus']['product']; ?>
" title="查看更多产品"><img src="images/pic_08.gif"/></a></span><img src="images/contactus_13.jpg" /></div>
				<div class="clear"></div>
				<div class="playercon clearfix">
					<ul>
					<?php $_from = $this->_tpl_vars['Products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['product'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['product']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['product']['iteration']++;
?>
						<li><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['item']['picture']; ?>
" alt="<?php echo $this->_tpl_vars['item']['name']; ?>
" width="80" height="80" border="0" /></a>
						<p><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
" target="_blank" title="查看详细信息"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></p></li>
					<?php endforeach; endif; unset($_from); ?>
					</ul>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="clear"></div>
	</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>