<?php /* Smarty version 2.6.18, created on 2020-09-16 15:42:25
         compiled from intro.html */ ?>
<?php $this->assign('PageTitle', ($this->_tpl_vars['_space_intro'])); ?>
<?php $this->assign('cur', 'space_intro'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "banner.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<div class="content">
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "leftbar.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<div class="right_barintro">
				<div class="pic"><img src="<?php echo $this->_tpl_vars['COMPANY']['logo']; ?>
" /><p><span><?php echo $this->_tpl_vars['_space_intro']; ?>
</span><br /><br />    <?php echo $this->_tpl_vars['COMPANY']['description']; ?>
</p>
		<div class="clear"></div></div>
		<div class="text">
			<ul>
				<li><p><span>法人代表/负责人：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['boss_name']; ?>
</p></li>
				<li><p><span>主营行业：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['main_prod']; ?>
</p></li>
				<li><p><span>公司类型：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['ecnomy']; ?>
</p></li>
				<li><p><span>经营模式：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['manage_type']; ?>
</p></li>
				<li><p><span>主要经营地点：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['main_biz_place']; ?>
</p></li>
				<li><p><span>注册资本：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['reg_fund']; ?>
</p></li>
				<li><p><span>注册年份：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['found_year']; ?>
</p></li>
				<li><p><span>注 册 地：</span></p><p><?php echo $this->_tpl_vars['COMPANY']['reg_address']; ?>
</p></li>
			</ul>
		</div>
		</div>
		<div class="clear"></div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>