<?php /* Smarty version 2.6.18, created on 2020-09-16 17:13:35
         compiled from default%5Cprice.list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'default\\price.list.html', 24, false),array('modifier', 'truncate', 'default\\price.list.html', 25, false),array('block', 'product', 'default\\price.list.html', 46, false),array('block', 'company', 'default\\price.list.html', 53, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', '5'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
	<div class="qiugoucontent clearfix">
	<div class="qiugoucontentleft">
    <div class="base_title">
       <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_search_result']; ?>
</span><span class="corner_t_r"></span></h2>
	</div>
		<div class="qiugouleftcon box_bord">
			<form>
            <table width="100%" border="0">
              <tr>
                <th class="offer_info"><?php echo $this->_tpl_vars['_title']; ?>
</th>
                <th class="offer_area"><?php echo $this->_tpl_vars['_user_name_n']; ?>
</th>
                <th class="offer_level"><?php echo $this->_tpl_vars['_category']; ?>
</th>
				 <th class="offer_level"><?php echo $this->_tpl_vars['_sorts']; ?>
</th>
				 <th class="offer_level"><?php echo $this->_tpl_vars['_the_area']; ?>
</th>
                <th class="offer_contact"><?php echo $this->_tpl_vars['_the_price']; ?>
</th>
              </tr>
              <?php $_from = $this->_tpl_vars['Prices']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
              <tr>
                <td class="offer_info title_link"><span><a href="product/price.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
" title="<?php echo $this->_tpl_vars['item']['title']; ?>
"><?php echo $this->_tpl_vars['item']['title']; ?>
</a></span>[<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['created'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
]<br />
						<?php echo $this->_tpl_vars['_abstract']; ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
<br /></td>
                <td class="offer_area"><?php if ($this->_tpl_vars['item']['companyname']): ?><a href="space.php?userid=<?php echo $this->_tpl_vars['item']['username']; ?>
"><?php echo $this->_tpl_vars['item']['companyname']; ?>
</a><?php else: ?><?php echo $this->_tpl_vars['item']['username']; ?>
<?php endif; ?></td>
                <td class="offer_level"><?php echo $this->_tpl_vars['PriceTypes'][$this->_tpl_vars['item']['type_id']]; ?>
</td>
				<td class="offer_level"><a href="product/price.php?do=search&catid=<?php echo $this->_tpl_vars['item']['category_id']; ?>
"><?php echo $this->_tpl_vars['item']['categoryname']; ?>
</a></td>
				<td class="offer_level"><?php echo $this->_tpl_vars['Areas'][$this->_tpl_vars['item']['area_id']]; ?>
</td>
                <td  class="offer_contact">
                <?php echo $this->_tpl_vars['item']['price']; ?>
<?php echo $this->_tpl_vars['Monetaries'][$this->_tpl_vars['item']['currency']]; ?>
/<?php echo $this->_tpl_vars['Measuries'][$this->_tpl_vars['item']['units']]; ?>

               </td>
              </tr>
              	<?php endforeach; endif; unset($_from); ?>
            </table>

			<div>
				<span><?php echo $this->_tpl_vars['ByPages']; ?>
</span>
			</div>
			</form>
		</div>
	</div>
	<div class="qiugoucontentright">
		<div class="recommendcompany">
			<div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" />&nbsp;<?php echo $this->_tpl_vars['_commend']; ?>
<?php echo $this->_tpl_vars['_product']; ?>
</div>
			<?php $this->_tag_stack[] = array('product', array('row' => 10,'type' => 'commend')); $_block_repeat=true;smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<p><a href="[link:title]">[field:title]</a></p>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			<div class="clear"></div>
		</div>
		<div class="recommendcompany">
			<div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" />&nbsp;<?php echo $this->_tpl_vars['_recommended_company']; ?>
</div>
			<?php $this->_tag_stack[] = array('company', array('row' => 15)); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<p><a href="[link:title]" target="_blank">[field:title]</a></p>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			<div class="clear"></div>
		</div>
</div>
</div>
<script>
$("#SearchFrm").attr("action","offer/list.php");
$("#topMenuProduct").addClass("lcur");
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>