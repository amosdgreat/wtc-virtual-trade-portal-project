<?php /* Smarty version 2.6.18, created on 2020-09-16 16:22:30
         compiled from default/header.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'cacheless', 'default/header.html', 87, false),array('function', 'category', 'default/header.html', 103, false),array('modifier', 'default', 'default/header.html', 127, false),)), $this); ?>
<?php $this->_cache_serials['C:\wamp64\www\wtc_b2b_new\data\templates_c\template\default\\%%81^81F^81F6D955%%header.html.inc'] = '0d05ee97dfd220f0f3eb462001e99700'; ?>

<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_tpl_vars['Charset']; ?>
" />
      <title><?php echo $this->_tpl_vars['page_title']; ?>
 - <?php echo $this->_tpl_vars['site_title']; ?>
</title>
      <base href="<?php echo $this->_tpl_vars['SiteUrl']; ?>
">
      <meta name ="keywords" content="<?php echo $this->_tpl_vars['metakeywords']; ?>
">
      <meta name ="description" content="<?php echo $this->_tpl_vars['metadescription']; ?>
">
      <meta http-equiv="X-UA-Compatible" content="IE=7" />
      <!-- <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['theme_img_path']; ?>
base.css" />
         <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['theme_img_path']; ?>
general.css" />
         <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
style.css" />
         <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/css/flatnav.css" />
         
         <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/css/examples.css" /> -->
      <!-- new style -->
      <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/favicon-16x16.png">
      <link rel="manifest" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/site.html">
      <link rel="mask-icon" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/safari-pinned-tab.svg" color="#666666">
      <link rel="shortcut icon" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/favicon.ico">
      <meta name="apple-mobile-web-app-title" content="Molla">
      <meta name="application-name" content="Molla">
      <meta name="msapplication-TileColor" content="#cc9966">
      <meta name="msapplication-config" content="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/images/icons/browserconfig.xml">
      <meta name="theme-color" content="#ffffff">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css">
      <!-- Plugins CSS File -->
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/plugins/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/plugins/magnific-popup/magnific-popup.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/plugins/jquery.countdown.css">
      <!-- Main CSS File -->
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/style.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/skins/skin-demo-14.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/css/demos/demo-14.css">
      <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/css/font-awesome.min.css">
      <!-- End of new style -->
      <!-- <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/css/bootstrap.min.css" /> -->
      <!-- jQuery library -->
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
      <!-- Popper JS -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script> -->
      <!-- Latest compiled JavaScript -->
      <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script> -->
      <!-- fontawesome -->
      <!-- <link rel="stylesheet" href="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/css/font-awesome.min.css" /> -->
      <!-- <link rel="stylesheet" type="text/css" href="css/common.css" /> -->
      <link rel="shorcut icon" type="image/x-ico" href="favicon.ico" />
      <link rel="bookmark" type="image/x-ico" href="favicon.ico" />
      <script src="scripts/jquery.js"></script>
      <script src="scripts/general.js" charset="<?php echo $this->_tpl_vars['Charset']; ?>
"></script>
      <script src="https://use.fontawesome.com/f5aeabb01e.js"></script>
      <script language="javascript">
         <!--
         $(document).ready(function() {
          if($("#mn_<?php echo $this->_tpl_vars['nav_id']; ?>
").length>0){
            $("#mn_<?php echo $this->_tpl_vars['nav_id']; ?>
").addClass("current_nav");
          }
          $("#SearchKeyword").focus(function(){
            if($("#SearchKeyword").val()=='<?php echo $this->_tpl_vars['_keywords_sample']; ?>
'){
              $("#SearchKeyword").val("").css('color', '#000000')
              };
            }).blur(function(){
            if($("#SearchKeyword").val()==''){
              $("#SearchKeyword").val("<?php echo $this->_tpl_vars['_keywords_sample']; ?>
").css('color', '#ccc')
              };
          });
          $("#BtnSearch").click(function(){
            if($('#SearchKeyword').val()=='<?php echo $this->_tpl_vars['_keywords_sample']; ?>
') {alert('<?php echo $this->_tpl_vars['_input_keywords']; ?>
');$('#SearchKeyword').focus();return false;}
          })
         })
         //-->
      </script>
      <script>
         $(document).ready( function() {
             $('.dropdown-toggle').dropdown();
         });
         
      </script>
   </head>
   <body id="bd_<?php echo @CURSCRIPT; ?>
">
      <!-- <div id="header">
         <div class="headtop">
         <?php if ($this->caching && !$this->_cache_including): echo '{nocache:0d05ee97dfd220f0f3eb462001e99700#0}'; endif;$this->_tag_stack[] = array('cacheless', array()); $_block_repeat=true;smarty_block_cacheless($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
         <p class="headtopcon">
             <a class="index" href="javascript:;" onclick="this.style.behavior='url(#default#homepage)';this.setHomePage('<?php echo $this->_tpl_vars['SiteUrl']; ?>
');"><?php echo $this->_tpl_vars['_set_home_page']; ?>
</a>
             <a href="javascript: myAddPanel(document.title,location.href,document.title);" rel="sidebar" title="<?php echo $this->_tpl_vars['site_name']; ?>
"><?php echo $this->_tpl_vars['_add_favorite']; ?>
</a>
             <?php if ($this->_tpl_vars['pb_username'] != ""): ?>
             <em><?php echo $this->_tpl_vars['_hello']; ?>
<?php echo $this->_tpl_vars['pb_username']; ?>
</em>
             <a href="redirect.php?url=office-room">[<?php echo $this->_tpl_vars['_my_office_room']; ?>
]</a>
             <?php if ($this->_tpl_vars['is_admin']): ?><a  href="pb-admin/login.php" target="_blank">[<?php echo $this->_tpl_vars['_control_pannel']; ?>
]</a><?php endif; ?>
             <a href="logging.php?action=logout">[<?php echo $this->_tpl_vars['_login_out']; ?>
]</a>
             <?php else: ?>
             <em><?php echo $this->_tpl_vars['_hello_welcome_to']; ?>
<?php echo $this->_tpl_vars['site_name']; ?>
</em>
             <a href="logging.php">&nbsp;[<?php echo $this->_tpl_vars['_pls_login']; ?>
]</a>
             <a href="member.php" title="<?php echo $this->_tpl_vars['_register']; ?>
" ><strong>[<?php echo $this->_tpl_vars['_register']; ?>
 <?php echo $this->_tpl_vars['_free']; ?>
]</strong></a>
             <?php endif; ?>
             <a href="service.php"  style="float:right;"><?php echo $this->_tpl_vars['_customer_service']; ?>
</a>
             <a href="offer/post.php" style="float:right;"><?php echo $this->_tpl_vars['_free_post']; ?>
</a>
             <?php echo smarty_function_category(array('type' => 'language'), $this);?>

         </p>
         <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_cacheless($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); if ($this->caching && !$this->_cache_including): echo '{/nocache:0d05ee97dfd220f0f3eb462001e99700#0}'; endif;?>
         </div>
         <div class="logo_search clearfix">
             <h1 id="logo"><a href="<?php echo $this->_tpl_vars['SiteUrl']; ?>
"><img src="<?php echo $this->_tpl_vars['site_logo']; ?>
" alt="<?php echo $this->_tpl_vars['site_banner_word']; ?>
" /></a></h1>
             <div class="search_bar"> -->
      <!-- <div class="search_nav">
         <ul>
             <li id="topMenuProduct"><a href="product/"><span><?php echo $this->_tpl_vars['_search_product']; ?>
</span></a></li>
             <li id="topMenuCompany"><a href="company/"><span><?php echo $this->_tpl_vars['_search_company']; ?>
</span></a></li>
             <li id="topMenuOffer"><a href="offer/"><span><?php echo $this->_tpl_vars['_search_offer']; ?>
</span></a></li>
             <li id="topMenuNews"><a href="news/"><span><?php echo $this->_tpl_vars['_search_news']; ?>
</span></a></li>
         </ul>
         <button type="button" class="btn btn-outline-primary">Primary</button>
         
         <a href="advsearch.php" class="fl lhighs"><?php echo $this->_tpl_vars['_adv_search']; ?>
</a>
         </div> -->
      <!-- <ul class="nav"> -->
      <!-- <li>
         <a href="#">Board</a>
         </li> -->
      <!-- <li id="search">
         <form name="search_frm" id="SearchFrm" action="offer/list.php" method="get">
           <input type="text" name="q" id="search_text" value="<?php echo ((is_array($_tmp=@$_GET['q'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['_keywords_sample']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['_keywords_sample'])); ?>
" placeholder="Search for a product"/>
           <input type="button" name="search_button" id="search_button">
         </form>
         </li>
         <li id="options">
         <a href="#">For Buyers</a>
         <ul class="subnav">
           <li><a href="#">New Products</a></li>
           <li><a href="#">New Added selling leds</a></li>
           <li><a href="#">Product Videos</a></li>
           <li><a href="#">Post Buying Request</a></li>
         </ul>
         </li>
         
         <li id="options">
         <a href="#">For Sellers</a>
         <ul class="subnav">
           <li><a href="#">Post Selling Leads</a></li>
           <li><a href="#">Videos</a></li>
           <li><a href="#">Premium Membership</a></li>
           <li><a href="#">Global Buyer Search</a></li>
         </ul>
         </li>
         </ul> -->
      <!-- <form name="search_frm" id="SearchFrm" action="offer/list.php" method="get">
         <input type="hidden" name="do" value="search" />
          <div class="search_panel">
              <span class="search_input_box">
                 <input class="form-control mr-sm-2" type="text" name="q" id="SearchKeyword" value="<?php echo ((is_array($_tmp=@$_GET['q'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['_keywords_sample']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['_keywords_sample'])); ?>
" />
               </span>
               <span class="search_btn_box">
               <button type="submit" class="btn btn-primary btn-sm" id="BtnSearch" value="<?php echo $this->_tpl_vars['_search']; ?>
"><?php echo $this->_tpl_vars['_search']; ?>
</button>
               </span>
         
               <span class="history break underline">
                 <p class="font-italic"><i class="fa fa-recycle"></i> <?php echo $this->_tpl_vars['_latest_search']; ?>
</p>
                 <?php echo $this->_tpl_vars['SearchHistory']; ?>

               </span> 
          </div>
         
         </form> -->
      <!--  </div>
         </div>
         </div> -->
      <!-- <div class="header_nav">    
         <div id="nav_inner">
             <ul>
                 <?php $_from = $this->_tpl_vars['navs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['navs'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['navs']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['navs']['iteration']++;
?>
                     <li><?php echo $this->_tpl_vars['item']['nav']; ?>
</li>
                 <?php endforeach; endif; unset($_from); ?>
             </ul>
         </div>
         </div> -->
      <!-- 
         <div class="promo-carousel" id="grouploop-3">
             <div class="item-wrap">
               <div class="item active">
                 <a href="#">Recent Deals</a>
               </div>
               <div class="item">
                 <a href="#">Super Special</a>
               </div>
               <div class="item">
                 <a href="#">Promo Code
                   <br/>SAVE30</a>
               </div>
               <div class="item">
                 <a href="#">20% Off Sale!</a>
               </div>
               <div class="item">
                 <a href="#">New Products!</a>
               </div>
               <div class="item">
                 <a href="#">Used deals</a>
               </div>
             </div>
           </div> -->
      <div class="page-wrapper">
      <header class="header header-14">
         <div class="header-top">
            <?php if ($this->caching && !$this->_cache_including): echo '{nocache:0d05ee97dfd220f0f3eb462001e99700#1}'; endif;$this->_tag_stack[] = array('cacheless', array()); $_block_repeat=true;smarty_block_cacheless($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
            <div class="container">
               <div class="header-left">
                  <a href="tel:#"><i class="icon-phone"></i>Call: +0123 456 789</a>&nbsp;&nbsp;
                  <a href="service.php">&nbsp;<i class="icon-envelope"></i><?php echo $this->_tpl_vars['_customer_service']; ?>
</a>
                  <a href="offer/post.php">&nbsp; <i class="icon-speaker"></i><?php echo $this->_tpl_vars['_free_post']; ?>
</a>
               </div>
               <!-- End .header-left -->
               <div class="header-right">
                  <ul class="top-menu">
                     <li>
                        <a href="#">Links</a>
                        <ul class="menus">
                           <li>
                              <div class="header-dropdown">
                                 <a href="#">For Buyers</a>
                                 <div class="header-menu">
                                    <ul>
                                       <li><a href="#">New Products</a></li>
                                       <li><a href="#">New Added selling leds</a></li>
                                       <li><a href="#">Product Videos</a></li>
                                       <li><a href="#">Post Buying Request</a></li>
                                    </ul>
                                 </div>
                                 <!-- End .header-menu -->
                              </div>
                              <!-- End .header-dropdown -->
                           </li>
                           <li>
                              <div class="header-dropdown">
                                 <a href="#">For Sellers</a>
                                 <div class="header-menu">
                                    <ul>
                                       <li><a href="#">Post Selling Leads</a></li>
                                       <li><a href="#">Videos</a></li>
                                       <li><a href="#">Premium Membership</a></li>
                                       <li><a href="#">Global Buyer Search</a></li>
                                    </ul>
                                 </div>
                                 <!-- End .header-menu -->
                              </div>
                              <!-- End .header-dropdown -->
                           </li>
                           <li class="login">
                              <?php if ($this->_tpl_vars['pb_username'] != ""): ?>
                              <em><?php echo $this->_tpl_vars['_hello']; ?>
<?php echo $this->_tpl_vars['pb_username']; ?>
</em>
                              <a href="redirect.php?url=office-room">[<?php echo $this->_tpl_vars['_my_office_room']; ?>
]</a>
                              <?php if ($this->_tpl_vars['is_admin']): ?><a  href="pb-admin/login.php" target="_blank">[<?php echo $this->_tpl_vars['_control_pannel']; ?>
]</a><?php endif; ?>
                              <a href="logging.php?action=logout">[<?php echo $this->_tpl_vars['_login_out']; ?>
]</a>
                              <?php else: ?>
                              <em><?php echo $this->_tpl_vars['_hello_welcome_to']; ?>
<?php echo $this->_tpl_vars['site_name']; ?>
</em>
                              <a href="logging.php">&nbsp;[<?php echo $this->_tpl_vars['_pls_login']; ?>
]</a>
                              <a href="member.php" title="<?php echo $this->_tpl_vars['_register']; ?>
">&nbsp;[<?php echo $this->_tpl_vars['_register']; ?>
 <?php echo $this->_tpl_vars['_free']; ?>
]</a>
                              <?php endif; ?>
                           </li>
                        </ul>
                     </li>
                  </ul>
                  <!-- End .top-menu -->
               </div>
               <!-- End .header-right -->
            </div>
            <!-- End .container -->
            <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_cacheless($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); if ($this->caching && !$this->_cache_including): echo '{/nocache:0d05ee97dfd220f0f3eb462001e99700#1}'; endif;?> <!-- cacheless end -->
         </div>
         <!-- End .header-top -->
         <div class="header-middle">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-auto col-lg-3 col-xl-3 col-xxl-2">
                     <button class="mobile-menu-toggler">
                     <span class="sr-only">Toggle mobile menu</span>
                     <i class="icon-bars"></i>
                     </button>
                     <a href="<?php echo $this->_tpl_vars['SiteUrl']; ?>
" class="logo">
                        <!-- <h1 id="logo"><a href="<?php echo $this->_tpl_vars['SiteUrl']; ?>
"><img src="<?php echo $this->_tpl_vars['site_logo']; ?>
" alt="<?php echo $this->_tpl_vars['site_banner_word']; ?>
" /></a></h1> -->
                        <img src="<?php echo $this->_tpl_vars['site_logo']; ?>
" alt="<?php echo $this->_tpl_vars['site_banner_word']; ?>
" width="105" height="25">
                     </a>
                  </div>
                  <!-- End .col-xl-3 col-xxl-2 -->
                  <div class="col col-lg-9 col-xl-9 col-xxl-10 header-middle-right">
                     <div class="row">
                        <div class="col-lg-8 col-xxl-4-5col d-none d-lg-block">
                           <div class="header-search header-search-extended header-search-visible header-search-no-radius">
                              <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                              <form name="search_frm" id="SearchFrm" action="offer/list.php" method="get">
                                 <input type="hidden" name="do" value="search" />
                                 <div class="header-search-wrapper search-wrapper-wide">
                                    <div class="select-custom">
                                       <select id="cat" name="cat">
                                          <option value="">All Categories</option>
                                          <option value="1">Fashion</option>
                                          <option value="2">- Women</option>
                                          <option value="3">- Men</option>
                                          <option value="4">- Jewellery</option>
                                          <option value="5">- Kids Fashion</option>
                                          <option value="6">Electronics</option>
                                          <option value="7">- Smart TVs</option>
                                          <option value="8">- Cameras</option>
                                          <option value="9">- Games</option>
                                          <option value="10">Home &amp; Garden</option>
                                          <option value="11">Motors</option>
                                          <option value="12">- Cars and Trucks</option>
                                          <option value="15">- Boats</option>
                                          <option value="16">- Auto Tools &amp; Supplies</option>
                                       </select>
                                    </div>
                                    <!-- End .select-custom -->
                                    <!-- <form name="search_frm" id="SearchFrm" action="offer/list.php" method="get">
                                       <input type="hidden" name="do" value="search" />
                                        <div class="search_panel">
                                            <span class="search_input_box">
                                               <input class="form-control mr-sm-2" type="text" name="q" id="SearchKeyword" value="<?php echo ((is_array($_tmp=@$_GET['q'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['_keywords_sample']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['_keywords_sample'])); ?>
" />
                                             </span>
                                             <span class="search_btn_box">
                                             <button type="submit" class="btn btn-primary btn-sm" id="BtnSearch" value="<?php echo $this->_tpl_vars['_search']; ?>
"><?php echo $this->_tpl_vars['_search']; ?>
</button>
                                             </span>
                                       
                                             <span class="history break underline">
                                               <p class="font-italic"><i class="fa fa-recycle"></i> <?php echo $this->_tpl_vars['_latest_search']; ?>
</p>
                                               <?php echo $this->_tpl_vars['SearchHistory']; ?>

                                             </span> 
                                        </div>
                                       
                                       </form> -->
                                    <label for="q" class="sr-only">Search</label>
                                    <input type="text" class="form-control" name="q" id="SearchKeyword" value="<?php echo ((is_array($_tmp=@$_GET['q'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['_keywords_sample']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['_keywords_sample'])); ?>
" placeholder="Your last search was: <?php echo ((is_array($_tmp=@$_GET['q'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['_keywords_sample']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['_keywords_sample'])); ?>
" required="">
                                    <button class="btn btn-primary" type="submit" id="BtnSearch" value="<?php echo $this->_tpl_vars['_search']; ?>
"><i class="icon-search"></i></button>
                                 </div>
                                 <!-- End .header-search-wrapper -->
                              </form>
                           </div>
                           <!-- End .header-search -->
                        </div>
                        <!-- End .col-xxl-4-5col -->
                        <div class="col-lg-4 col-xxl-5col d-flex justify-content-end align-items-center">
                           <div class="header-dropdown-link">
                              <div class="dropdown compare-dropdown">
                                 <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Compare Products" aria-label="Compare Products">
                                 <i class="icon-random"></i>
                                 <span class="compare-txt">Compare</span>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="compare-products">
                                       <li class="compare-product">
                                          <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                          <h4 class="compare-product-title"><a href="product.html">Blue Night Dress</a></h4>
                                       </li>
                                       <li class="compare-product">
                                          <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                          <h4 class="compare-product-title"><a href="product.html">White Long Skirt</a></h4>
                                       </li>
                                    </ul>
                                    <div class="compare-actions">
                                       <a href="#" class="action-link">Clear All</a>
                                       <a href="#" class="btn btn-outline-primary-2"><span>Compare</span><i class="icon-long-arrow-right"></i></a>
                                    </div>
                                 </div>
                                 <!-- End .dropdown-menu -->
                              </div>
                              <!-- End .compare-dropdown -->
                              <a href="wishlist.html" class="wishlist-link">
                              <i class="icon-heart-o"></i>
                              <span class="wishlist-count">3</span>
                              <span class="wishlist-txt">Wishlist</span>
                              </a>
                              <div class="dropdown cart-dropdown">
                                 <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                 <i class="icon-shopping-cart"></i>
                                 <span class="cart-count">2</span>
                                 <span class="cart-txt">Cart</span>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-cart-products">
                                       <div class="product">
                                          <div class="product-cart-details">
                                             <h4 class="product-title">
                                                <a href="product.html">Beige knitted elastic runner shoes</a>
                                             </h4>
                                             <span class="cart-product-info">
                                             <span class="cart-product-qty">1</span>
                                             x $84.00
                                             </span>
                                          </div>
                                          <!-- End .product-cart-details -->
                                          <figure class="product-image-container">
                                             <a href="product.html" class="product-image">
                                             <img src="assets/images/products/cart/product-1.jpg" alt="product">
                                             </a>
                                          </figure>
                                          <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                       </div>
                                       <!-- End .product -->
                                       <div class="product">
                                          <div class="product-cart-details">
                                             <h4 class="product-title">
                                                <a href="product.html">Blue utility pinafore denim dress</a>
                                             </h4>
                                             <span class="cart-product-info">
                                             <span class="cart-product-qty">1</span>
                                             x $76.00
                                             </span>
                                          </div>
                                          <!-- End .product-cart-details -->
                                          <figure class="product-image-container">
                                             <a href="product.html" class="product-image">
                                             <img src="assets/images/products/cart/product-2.jpg" alt="product">
                                             </a>
                                          </figure>
                                          <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                       </div>
                                       <!-- End .product -->
                                    </div>
                                    <!-- End .cart-product -->
                                    <div class="dropdown-cart-total">
                                       <span>Total</span>
                                       <span class="cart-total-price">$160.00</span>
                                    </div>
                                    <!-- End .dropdown-cart-total -->
                                    <div class="dropdown-cart-action">
                                       <a href="cart.html" class="btn btn-primary">View Cart</a>
                                       <a href="checkout.html" class="btn btn-outline-primary-2"><span>Checkout</span><i class="icon-long-arrow-right"></i></a>
                                    </div>
                                    <!-- End .dropdown-cart-total -->
                                 </div>
                                 <!-- End .dropdown-menu -->
                              </div>
                              <!-- End .cart-dropdown -->
                           </div>
                        </div>
                        <!-- End .col-xxl-5col -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .col-xl-9 col-xxl-10 -->
               </div>
               <!-- End .row -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-middle -->
         <div class="header-bottom sticky-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-auto col-lg-3 col-xl-3 col-xxl-2 header-left">
                     <div class="dropdown category-dropdown show is-on" data-visible="true">
                        <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories">
                        Browse Categories
                        </a>
                        <div class="dropdown-menu show">
                           <nav class="side-nav">
                              <ul class="menu-vertical sf-arrows">
                                 <li class="megamenu-container">
                                    <a class="sf-with-ul" href="#"><i class="icon-laptop"></i>Electronics</a>
                                    <div class="megamenu">
                                       <div class="row no-gutters">
                                          <div class="col-md-8">
                                             <div class="menu-col">
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Laptops & Computers</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Desktop Computers</a></li>
                                                         <li><a href="#">Monitors</a></li>
                                                         <li><a href="#">Laptops</a></li>
                                                         <li><a href="#">iPad & Tablets</a></li>
                                                         <li><a href="#">Hard Drives & Storage</a></li>
                                                         <li><a href="#">Printers & Supplies</a></li>
                                                         <li><a href="#">Computer Accessories</a></li>
                                                      </ul>
                                                      <div class="menu-title">TV & Video</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">TVs</a></li>
                                                         <li><a href="#">Home Audio Speakers</a></li>
                                                         <li><a href="#">Projectors</a></li>
                                                         <li><a href="#">Media Streaming Devices</a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Cell Phones</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Carrier Phones</a></li>
                                                         <li><a href="#">Unlocked Phones</a></li>
                                                         <li><a href="#">Phone & Cellphone Cases</a></li>
                                                         <li><a href="#">Cellphone Chargers </a></li>
                                                      </ul>
                                                      <div class="menu-title">Digital Cameras</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Digital SLR Cameras</a></li>
                                                         <li><a href="#">Sports & Action Cameras</a></li>
                                                         <li><a href="#">Camcorders</a></li>
                                                         <li><a href="#">Camera Lenses</a></li>
                                                         <li><a href="#">Photo Printer</a></li>
                                                         <li><a href="#">Digital Memory Cards</a></li>
                                                         <li><a href="#">Camera Bags, Backpacks & Cases</a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                </div>
                                                <!-- End .row -->
                                             </div>
                                             <!-- End .menu-col -->
                                          </div>
                                          <!-- End .col-md-8 -->
                                          <div class="col-md-4">
                                             <div class="banner banner-overlay">
                                                <a href="category.html" class="banner banner-menu">
                                                <img src="assets/images/demos/demo-13/menu/banner-1.jpg" alt="Banner">
                                                </a>
                                             </div>
                                             <!-- End .banner banner-overlay -->
                                          </div>
                                          <!-- End .col-md-4 -->
                                       </div>
                                       <!-- End .row -->
                                    </div>
                                    <!-- End .megamenu -->
                                 </li>
                                 <li class="megamenu-container">
                                    <a class="sf-with-ul" href="#"><i class="icon-couch"></i>Furniture</a>
                                    <div class="megamenu">
                                       <div class="row no-gutters">
                                          <div class="col-md-8">
                                             <div class="menu-col">
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Bedroom</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Beds, Frames & Bases</a></li>
                                                         <li><a href="#">Dressers</a></li>
                                                         <li><a href="#">Nightstands</a></li>
                                                         <li><a href="#">Kids' Beds & Headboards</a></li>
                                                         <li><a href="#">Armoires</a></li>
                                                      </ul>
                                                      <div class="menu-title">Living Room</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Coffee Tables</a></li>
                                                         <li><a href="#">Chairs</a></li>
                                                         <li><a href="#">Tables</a></li>
                                                         <li><a href="#">Futons & Sofa Beds</a></li>
                                                         <li><a href="#">Cabinets & Chests</a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Office</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Office Chairs</a></li>
                                                         <li><a href="#">Desks</a></li>
                                                         <li><a href="#">Bookcases</a></li>
                                                         <li><a href="#">File Cabinets</a></li>
                                                         <li><a href="#">Breakroom Tables</a></li>
                                                      </ul>
                                                      <div class="menu-title">Kitchen & Dining</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#">Dining Sets</a></li>
                                                         <li><a href="#">Kitchen Storage Cabinets</a></li>
                                                         <li><a href="#">Bakers Racks</a></li>
                                                         <li><a href="#">Dining Chairs</a></li>
                                                         <li><a href="#">Dining Room Tables</a></li>
                                                         <li><a href="#">Bar Stools</a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                </div>
                                                <!-- End .row -->
                                             </div>
                                             <!-- End .menu-col -->
                                          </div>
                                          <!-- End .col-md-8 -->
                                          <div class="col-md-4">
                                             <div class="banner banner-overlay">
                                                <a href="category.html" class="banner banner-menu">
                                                <img src="assets/images/demos/demo-13/menu/banner-2.jpg" alt="Banner">
                                                </a>
                                             </div>
                                             <!-- End .banner banner-overlay -->
                                          </div>
                                          <!-- End .col-md-4 -->
                                       </div>
                                       <!-- End .row -->
                                    </div>
                                    <!-- End .megamenu -->
                                 </li>
                                 <li class="megamenu-container">
                                    <a class="sf-with-ul" href="#"><i class="icon-concierge-bell"></i>Cooking</a>
                                    <div class="megamenu">
                                       <div class="menu-col">
                                          <div class="row">
                                             <div class="col-md-4">
                                                <div class="menu-title">Cookware</div>
                                                <!-- End .menu-title -->
                                                <ul>
                                                   <li><a href="#">Cookware Sets</a></li>
                                                   <li><a href="#">Pans, Griddles & Woks</a></li>
                                                   <li><a href="#">Pots</a></li>
                                                   <li><a href="#">Skillets & Grill Pans</a></li>
                                                   <li><a href="#">Kettles</a></li>
                                                   <li><a href="#">Soup & Stockpots</a></li>
                                                </ul>
                                             </div>
                                             <!-- End .col-md-4 -->
                                             <div class="col-md-4">
                                                <div class="menu-title">Dinnerware & Tabletop</div>
                                                <!-- End .menu-title -->
                                                <ul>
                                                   <li><a href="#">Plates</a></li>
                                                   <li><a href="#">Cups & Mugs</a></li>
                                                   <li><a href="#">Trays & Platters</a></li>
                                                   <li><a href="#">Coffee & Tea Serving</a></li>
                                                   <li><a href="#">Salt & Pepper Shaker</a></li>
                                                </ul>
                                             </div>
                                             <!-- End .col-md-4 -->
                                             <div class="col-md-4">
                                                <div class="menu-title">Cooking Appliances</div>
                                                <!-- End .menu-title -->
                                                <ul>
                                                   <li><a href="#">Microwaves</a></li>
                                                   <li><a href="#">Coffee Makers</a></li>
                                                   <li><a href="#">Mixers & Attachments</a></li>
                                                   <li><a href="#">Slow Cookers</a></li>
                                                   <li><a href="#">Air Fryers</a></li>
                                                   <li><a href="#">Toasters & Ovens</a></li>
                                                </ul>
                                             </div>
                                             <!-- End .col-md-4 -->
                                          </div>
                                          <!-- End .row -->
                                          <div class="row menu-banners">
                                             <div class="col-md-4">
                                                <div class="banner">
                                                   <a href="#">
                                                   <img src="assets/images/demos/demo-13/menu/1.jpg" alt="image">
                                                   </a>
                                                </div>
                                                <!-- End .banner -->
                                             </div>
                                             <!-- End .col-md-4 -->
                                             <div class="col-md-4">
                                                <div class="banner">
                                                   <a href="#">
                                                   <img src="assets/images/demos/demo-13/menu/2.jpg" alt="image">
                                                   </a>
                                                </div>
                                                <!-- End .banner -->
                                             </div>
                                             <!-- End .col-md-4 -->
                                             <div class="col-md-4">
                                                <div class="banner">
                                                   <a href="#">
                                                   <img src="assets/images/demos/demo-13/menu/3.jpg" alt="image">
                                                   </a>
                                                </div>
                                                <!-- End .banner -->
                                             </div>
                                             <!-- End .col-md-4 -->
                                          </div>
                                          <!-- End .row -->
                                       </div>
                                       <!-- End .menu-col -->
                                    </div>
                                    <!-- End .megamenu -->
                                 </li>
                                 <li class="megamenu-container">
                                    <a class="sf-with-ul" href="#"><i class="icon-tshirt"></i>Clothing</a>
                                    <div class="megamenu">
                                       <div class="row no-gutters">
                                          <div class="col-md-8">
                                             <div class="menu-col">
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Women</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#"><strong>New Arrivals</strong></a></li>
                                                         <li><a href="#"><strong>Best Sellers</strong></a></li>
                                                         <li><a href="#"><strong>Trending</strong></a></li>
                                                         <li><a href="#">Clothing</a></li>
                                                         <li><a href="#">Shoes</a></li>
                                                         <li><a href="#">Bags</a></li>
                                                         <li><a href="#">Accessories</a></li>
                                                         <li><a href="#">Jewlery & Watches</a></li>
                                                         <li><a href="#"><strong>Sale</strong></a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                   <div class="col-md-6">
                                                      <div class="menu-title">Men</div>
                                                      <!-- End .menu-title -->
                                                      <ul>
                                                         <li><a href="#"><strong>New Arrivals</strong></a></li>
                                                         <li><a href="#"><strong>Best Sellers</strong></a></li>
                                                         <li><a href="#"><strong>Trending</strong></a></li>
                                                         <li><a href="#">Clothing</a></li>
                                                         <li><a href="#">Shoes</a></li>
                                                         <li><a href="#">Bags</a></li>
                                                         <li><a href="#">Accessories</a></li>
                                                         <li><a href="#">Jewlery & Watches</a></li>
                                                      </ul>
                                                   </div>
                                                   <!-- End .col-md-6 -->
                                                </div>
                                                <!-- End .row -->
                                             </div>
                                             <!-- End .menu-col -->
                                          </div>
                                          <!-- End .col-md-8 -->
                                          <div class="col-md-4">
                                             <div class="banner banner-overlay">
                                                <a href="category.html" class="banner banner-menu">
                                                <img src="assets/images/demos/demo-13/menu/banner-3.jpg" alt="Banner">
                                                </a>
                                             </div>
                                             <!-- End .banner banner-overlay -->
                                          </div>
                                          <!-- End .col-md-4 -->
                                       </div>
                                       <!-- End .row -->
                                       <div class="menu-col menu-brands">
                                          <div class="row">
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/1.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/2.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/3.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/4.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/5.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                             <div class="col-lg-2">
                                                <a href="#" class="brand">
                                                <img src="assets/images/brands/6.png" alt="Brand Name">
                                                </a>
                                             </div>
                                             <!-- End .col-lg-2 -->
                                          </div>
                                          <!-- End .row -->
                                       </div>
                                       <!-- End .menu-brands -->
                                    </div>
                                    <!-- End .megamenu -->
                                 </li>
                                 <li><a href="#"><i class="icon-blender"></i>Home Appliances</a></li>
                                 <li><a href="#"><i class="icon-heartbeat"></i>Healthy & Beauty</a></li>
                                 <li><a href="#"><i class="icon-shoe-prints"></i>Shoes & Boots</a></li>
                                 <li><a href="#"><i class="icon-map-signs"></i>Travel & Outdoor</a></li>
                                 <li><a href="#"><i class="icon-mobile-alt"></i>Smart Phones</a></li>
                                 <li><a href="#"><i class="icon-tv"></i>TV & Audio</a></li>
                                 <li><a href="#"><i class="icon-shopping-bag"></i>Backpack & Bag</a></li>
                                 <li><a href="#"><i class="icon-music"></i>Musical Instruments</a></li>
                                 <li><a href="#"><i class="icon-gift"></i>Gift Ideas</a></li>
                              </ul>
                              <!-- End .menu-vertical -->
                           </nav>
                           <!-- End .side-nav -->
                        </div>
                        <!-- End .dropdown-menu -->
                     </div>
                     <!-- End .category-dropdown -->
                  </div>
                  <!-- End .col-xl-3 col-xxl-2 -->
                  <div class="col col-lg-6 col-xl-6 col-xxl-8 header-center">
                     <nav class="main-nav">
                        <ul class="menu sf-arrows">
                           <?php $_from = $this->_tpl_vars['navs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['navs'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['navs']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['navs']['iteration']++;
?>
                           <li class="megamenu-container"><?php echo $this->_tpl_vars['item']['nav']; ?>
</li>
                           <?php endforeach; endif; unset($_from); ?>
                        </ul>
                        <!-- End .menu -->
                     </nav>
                     <!-- End .main-nav -->
                  </div>
                  <!-- End .col-xl-9 col-xxl-10 -->
                  <div class="col col-lg-3 col-xl-3 col-xxl-2 header-right">
                     <i class="la la-lightbulb-o"></i>
                     <p>Need Help?</span></p>
                  </div>
               </div>
               <!-- End .row -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-bottom -->
      </header>
      <!-- End .header -->
