<?php /* Smarty version 2.6.18, created on 2020-08-05 22:42:56
         compiled from default%5Cnews.list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'default\\news.list.html', 21, false),array('block', 'news', 'default\\news.list.html', 46, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
  <div class="content clearfix">
    <div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
    <div class="blank6"></div>
    <div class="companylistconleft">
        <div class="base_title">
          <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_search_result']; ?>
</span><span class="corner_t_r"></span></h2> 
	    </div>
        <div class="base_list_box box_bord">
           <div class="base_list_head"><strong><?php echo $this->_tpl_vars['_puglish_time']; ?>
</strong><a href="news/list.php?filter=2592000"><?php echo $this->_tpl_vars['_this_month']; ?>
</a><a href="news/list.php?filter=604800"><?php echo $this->_tpl_vars['_this_week']; ?>
</a><a href="news/list.php?filter=86400"><?php echo $this->_tpl_vars['_today_hours']; ?>
</a></div>
           <div class="base_list_content clearfix">	
           <?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['newslist'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['newslist']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['newslist']['iteration']++;
?>
                <div class="base_list clearfix">
                    <?php if ($this->_tpl_vars['item']['picture']): ?><div class="base_list_img"><img src="<?php echo $this->_tpl_vars['item']['image']; ?>
" /></div><?php endif; ?>
                    <div class="base_list_info">
                      <span class="title title_link"><a href="news/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['title']; ?>
</a></span>
                      <p> <?php echo $this->_tpl_vars['item']['digest']; ?>
</p>
                      <div class="update_time"><em><?php echo $this->_tpl_vars['_update_time']; ?>
(<?php echo $this->_tpl_vars['item']['pubdate']; ?>
)</em><?php echo $this->_tpl_vars['_categories']; ?>
[<a href="news/list.php?typeid=<?php echo $this->_tpl_vars['item']['type_id']; ?>
"><?php echo $this->_tpl_vars['Newstypes'][$this->_tpl_vars['item']['type_id']]; ?>
</a>]</div>
                      <div class="txt_source"><span class="fr link-underline"><a href="news/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['_visit_full_content']; ?>
...</a></span><?php echo $this->_tpl_vars['item']['clicked']; ?>
 <?php echo $this->_tpl_vars['_visited']; ?>
&nbsp;<?php echo $this->_tpl_vars['_source']; ?>
<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['source'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['site_name']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['site_name'])); ?>
</div>
                  </div>
                 </div>
                 <hr class="hr_dashed" />
               <?php endforeach; endif; unset($_from); ?>
                <div><?php echo $this->_tpl_vars['ByPages']; ?>
</div>
           </div>
        </div>
    </div>
    <div class="companylistconright">
      <div class="lookcompany">
        <div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" /><?php echo $this->_tpl_vars['_category_nav']; ?>
</div>
        <div class="companylistseecon">
          <ul>
          	<?php $_from = $this->_tpl_vars['SubCats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item1']):
?>
            <li><a href="news/list.php?typeid=<?php echo $this->_tpl_vars['item1']['id']; ?>
"><?php echo $this->_tpl_vars['item1']['name']; ?>
</a></li>
            <?php endforeach; endif; unset($_from); ?>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="lookcompany">
        <div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" /><?php echo $this->_tpl_vars['_hot_info_top']; ?>
</div>
        <div class="companylistseecon">
          <ul>
          	<?php $this->_tag_stack[] = array('news', array('type' => 'hot','row' => 10)); $_block_repeat=true;smarty_block_news($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
            <li><a href="news/detail.php?id=[field:id]">[field:title]</a></li>
            <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_news($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="lookcompany" style="margin-top:10px;">
       <div class="companylistseecon" >
        <form action="news/list.php" method="get" id="NewsSearchFrm"><input type="text" name="q" value="" class="input"/><input type="button" value="<?php echo $this->_tpl_vars['_search']; ?>
" onclick="$('#NewsSearchFrm').submit();" class="submit"/></form></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  </div>
<script>
$("#SearchFrm").attr("action","news/list.php");
$("#topMenuNews").addClass("lcur");
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>