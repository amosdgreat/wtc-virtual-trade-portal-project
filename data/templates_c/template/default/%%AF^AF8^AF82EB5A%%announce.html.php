<?php /* Smarty version 2.6.18, created on 2020-09-15 13:27:13
         compiled from default%5Cannounce.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_announce'])); ?>
<?php $this->assign('nav_id', ($this->_tpl_vars['nav_id'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
  <div class="content">
    <div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
    <div class="blank"></div>
    <div class="body_content">
        <div class="title_bar_s3">
            <span class="title_top_s3"><span></span></span>
            <h2><?php echo $this->_tpl_vars['_announce']; ?>
</h2>
         </div>  
		<ul class="announce_list">
			<?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['announce'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['announce']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['announce']['iteration']++;
?>
			<li><a href="announce.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
" target="_blank"><?php echo $this->_tpl_vars['item']['subject']; ?>
</a>&nbsp;[<?php echo $this->_tpl_vars['item']['pubdate']; ?>
]</li>
			<?php endforeach; endif; unset($_from); ?>
		</ul>
	 
       <div class="blank"></div>
    </div>
    </div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>