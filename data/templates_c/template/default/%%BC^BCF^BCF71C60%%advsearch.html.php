<?php /* Smarty version 2.6.18, created on 2020-07-29 11:40:47
         compiled from default%5Cadvsearch.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_adv_search'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<style type="text/css">
@import url(<?php echo $this->_tpl_vars['theme_img_path']; ?>
effect.css);
</style>
<script src="scripts/jquery.tools.js"></script>
<div class="wrapper">
<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>  
<div class="advance_search">
	 <!-- the tabs -->
	  <ul class="tabs2 title_mouse">
		  <li><a href="javascript:;"><span><?php echo $this->_tpl_vars['_offer']; ?>
</span></a></li>
		  <li><a href="javascript:;"><span><?php echo $this->_tpl_vars['_product']; ?>
</span></a></li>
		  <li><a href="javascript:;"><span><?php echo $this->_tpl_vars['_yellow_page']; ?>
</span></a></li>
		  <li><a href="javascript:;"><span><?php echo $this->_tpl_vars['_market']; ?>
</span></a></li>
	</ul>
	<!-- tab "panes" -->
	<div class="panes box_bord">
		<div class="tabs_search" id="SearchOffer">
		<form name='form_send0' id='form_send0' method='GET' action='offer/list.php'>
			<input type="hidden" name="do" value="search" />
			<table>
				  <tr>
					<th><?php echo $this->_tpl_vars['_offer_title']; ?>
</th>
					<td><input type="text" name="q"  /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_company_name']; ?>
</th>
					<td><input type="text" name="company_name"  /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_valid_time']; ?>
</th>
					<td><select name="pubdate">
						<option value='0'><?php echo $this->_tpl_vars['_no_time_limit']; ?>
</option>
						<option value='l3'><?php echo $this->_tpl_vars['_least_three_days']; ?>
</option>
						<option value='l10'><?php echo $this->_tpl_vars['_least_ten_days']; ?>
</option>
						<option value='l30'><?php echo $this->_tpl_vars['_least_thirty_days']; ?>
</option>
						</select>
					</td>
				  </tr>
				  <tr>
					<th>&nbsp;</th>
					<td>
					<input type="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
"  class="btn_submit" />
					<input name="input2"  type="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" class="btn_submit"/>
				</td>
				  </tr>
			  </table>
		</form>
		</div>
		<!-- display none -->
		<div class="tabs_search" style="display:none" id="SearchProduct">
		<form name='form_send1' id='form_send1' method='GET' action='product/list.php'>
			<input type="hidden" name="do" value="search" />
			<table>
				  <tr>
					<th><?php echo $this->_tpl_vars['_product_title']; ?>
</th>
					<td><input type="text" name="q" id="q9" /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_company_name']; ?>
</th>
					<td><input type="text" name="company_name" /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_post_time']; ?>
</th>
					<td>
					  <select name="pubdate">
						<option value='0'><?php echo $this->_tpl_vars['_no_time_limit']; ?>
</option>
						<option value='l3'><?php echo $this->_tpl_vars['_least_three_days']; ?>
</option>
						<option value='l10'><?php echo $this->_tpl_vars['_least_ten_days']; ?>
</option>
						<option value='l30'><?php echo $this->_tpl_vars['_least_thirty_days']; ?>
</option>
					  </select>						
					</td>
				  </tr>
				  <tr>
					<th>&nbsp;</th>
					<td>
					<input type="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
"  class="btn_submit" />
					<input name="input2"  type="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" class="btn_submit"/>						
					</td>
				  </tr>
			  </table>
			  </form>
		</div>
		<div class="tabs_search" style="display:none" id="SearchCompany">
		<form name='form_send2' id='form_send2' method='get' action='company/list.php'>
			<input type="hidden" name="do" value="search" />
			<table>
				  <tr>
					<th><?php echo $this->_tpl_vars['_company_name']; ?>
</th>
					<td><input type="text" name="q" id="q15" /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_main_prod_n']; ?>
</th>
					<td><input type="text" name="main_prod" /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_belong_industry']; ?>
</th>
					<td>
						<p id="dataIndustry">
							<select name="industry_id1" id="dataTradeIndustryId1" class="level_1" style="width:120px;" ></select>
							<select name="industry_id2" id="dataTradeIndustryId2" class="level_2" style="width:120px;"></select>
							<select name="industry_id3" id="dataTradeIndustryId3" class="level_3" style="width:120px;"></select>
						</p>						
					</td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_area']; ?>
</th>
					<td>
						<p id="dataArea">
							<select name="area_id1" id="dataTradeAreaId1" class="level_1" style="width:120px;" ></select>
							<select name="area_id2" id="dataTradeAreaId2" class="level_2" style="width:120px;"></select>
							<select name="area_id3" id="dataTradeAreaId3" class="level_3" style="width:120px;"></select>
						</p>						
					</td>
				  </tr>
				  <tr>
					<th>&nbsp;</th>
					<td>
					<input type="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
"  class="btn_submit" />
					<input name="input2"  type="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" class="btn_submit"/>
					</td>
				  </tr>
			  </table>
			  </form>
		</div>
		<div class="tabs_search" style="display:none" id="SearchMarket">
		<form name='form_send3' id='form_send3' method='GET' action='market/list.php'>
			<input type="hidden" name="do" value="search" />
			<table>
				  <tr>
					<th><?php echo $this->_tpl_vars['_market_name']; ?>
</th>
					<td><input name="q" type="text" /></td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_belong_industry']; ?>
</th>
					<td>
						<p id="dataIndustry">
							<select name="industry_id1" id="dataTradeIndustryId1" class="level_1" style="width:120px;" ></select>
							<select name="industry_id2" id="dataTradeIndustryId2" class="level_2" style="width:120px;"></select>
							<select name="industry_id3" id="dataTradeIndustryId3" class="level_3" style="width:120px;"></select>
						</p>						
					</td>
				  </tr>
				  <tr>
					<th><?php echo $this->_tpl_vars['_area']; ?>
</th>
					<td>
						<p id="dataArea">
							<select name="area_id1" id="dataTradeAreaId1" class="level_1" style="width:120px;" ></select>
							<select name="area_id2" id="dataTradeAreaId2" class="level_2" style="width:120px;"></select>
							<select name="area_id3" id="dataTradeAreaId3" class="level_3" style="width:120px;"></select>
						</p>						
					</td>
				  </tr>
				  <tr>
					<th>&nbsp;</th>
					<td>
					<input type="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
"  class="btn_submit" />
					<input name="input2"  type="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" class="btn_submit"/>
					</td>
				  </tr>
			  </table>
		</form>
		</div>
	</div>
	</div>
</div>
<script>
var app_language = '<?php echo $this->_tpl_vars['AppLanguage']; ?>
';
$(function() {
	$("ul.tabs2").tabs("div.panes > div");
});
</script>
<script src="scripts/multi_select.js" charset="<?php echo $this->_tpl_vars['Charset']; ?>
"></script>
<script src="scripts/script_area.js"></script>
<script src="scripts/script_industry.js"></script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>