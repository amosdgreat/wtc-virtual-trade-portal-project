<?php /* Smarty version 2.6.18, created on 2020-07-29 13:18:35
         compiled from default%5Cfair.detail.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', '8'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
  <div class="content  clearfix">
	 <div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
	 <div class="blank6"></div>
	 <div class="ex_con">
		 <div class="ex_con_left">
		   <div class="ex_con_left_title"><p><?php echo $this->_tpl_vars['item']['name']; ?>
</p></div>
		   <div class="blank6"></div>
		   <div class="ex_con_left_con">
           <div class="base_title">
            <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_fair_introduction']; ?>
</span><span class="corner_t_r"></span></h2> 
	       </div>
		  <div class="ex_con_left_con_body">
			<p><?php echo $this->_tpl_vars['item']['description']; ?>
</p>
		  </div>
		   </div>
		   <div class="blank6"></div>
		  <div class="ex_con_left_con">
		       <div class="base_title">
                    <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_fair_content']; ?>
</span><span class="corner_t_r"></span></h2> 
	           </div>
		  <div class="ex_con_left_con_body">
			<?php echo $this->_tpl_vars['_fair_time']; ?>
<?php echo $this->_tpl_vars['item']['begin_date']; ?>
<?php echo $this->_tpl_vars['_arrive_to']; ?>
<?php echo $this->_tpl_vars['item']['end_date']; ?>
<br />
			<?php echo $this->_tpl_vars['_address']; ?>
<?php echo $this->_tpl_vars['item']['address']; ?>
<br />
			<?php echo $this->_tpl_vars['_fair_size']; ?>
<?php echo $this->_tpl_vars['item']['scope']; ?>
<br />
			<?php echo $this->_tpl_vars['_organizer']; ?>
<?php echo $this->_tpl_vars['item']['hosts']; ?>
<br />
			<?php echo $this->_tpl_vars['_sponsor']; ?>
<?php echo $this->_tpl_vars['item']['organisers']; ?>
<br />
			<?php echo $this->_tpl_vars['_support']; ?>
<?php echo $this->_tpl_vars['item']['sponsors']; ?>
<br />
			<?php echo $this->_tpl_vars['_co_organizer']; ?>
<?php echo $this->_tpl_vars['item']['co_organisers']; ?>
<br />
			<?php echo $this->_tpl_vars['_contact']; ?>
<?php echo $this->_tpl_vars['item']['contacts']; ?>
<br />
			<?php echo $this->_tpl_vars['_notes']; ?>
<?php echo $this->_tpl_vars['item']['important_notice']; ?>
<br />
            </div>
		  </div>
		 </div>
	 
	 <div class="ex_con_right">
	<div class="ex_con_right_con">
      <div class="title_bar_s4">
          <span class="title_top_s4"><span></span></span>
          <h3><?php echo $this->_tpl_vars['_fair_enter_name']; ?>
</h3>
       </div>
	  <div class="ex_con_right_con_body"><span>
      <form name="join_fair" id="JoinFair" action="office-room/fair.php" method="post" target="_blank">
      <input type="hidden" name="do" value="join" />
      <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
">
      <input type="button" name="button" class="submit_w91" value="<?php echo $this->_tpl_vars['_enter_name_fair']; ?>
" onclick="$('#JoinFair').submit();" />
      </form>
      </span></div>
	</div>
	<div class="blank6"></div>
	<div class="ex_con_right_con">
       <div class="title_bar_s4">
          <span class="title_top_s4"><span></span></span>
          <h3><?php echo $this->_tpl_vars['_fair_business']; ?>
</h3>
       </div>
	  <div class="ex_con_right_con_body">
		<ul class="ex_con_rightlist" >
        <?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['expomember'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['expomember']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['expomember']):
        $this->_foreach['expomember']['iteration']++;
?>
		 <li><a href="space.php?userid=<?php echo $this->_tpl_vars['expomember']['userid']; ?>
" target="_blank"><?php echo $this->_tpl_vars['expomember']['name']; ?>
</a></li>
        <?php endforeach; endif; unset($_from); ?>
		</ul>
		</div>
	</div>
  </div>
</div>
</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>