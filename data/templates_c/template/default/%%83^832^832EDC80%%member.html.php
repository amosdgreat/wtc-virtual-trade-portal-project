<?php /* Smarty version 2.6.18, created on 2020-07-29 13:21:33
         compiled from default%5Cmember.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_member_reg'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<style type="text/css">  
label.error {
  font-weight: bold;
  color: #b80000;
}
</style>
<div class="wrapper">
  <div class="subnav">
    <div class="member_register"><span class="step_01"><?php echo $this->_tpl_vars['_fill_reg_info']; ?>
</span><span class="step_02"><?php echo $this->_tpl_vars['_reg_success']; ?>
</span></div>
  </div>
  <div class="blank6"></div>
  <div class="content">
	 <div class="base_title">
	  <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_choose_reg_type']; ?>
</span><span class="corner_t_r"></span><span class="title_telephone"><?php echo $this->_tpl_vars['_issue_contact']; ?>
<?php echo $this->_tpl_vars['service_tel']; ?>
</span></h2>
	</div>
	  <div class="base_content box_bord">
	     <h3><?php echo $this->_tpl_vars['_choose_reg_type_conditions']; ?>
</h3>
         <div class="blank"></div>
	     <div class="member_black clearfix">
             <div class="black_w680">
                <table class="reg_member_list no_outline">
                  <tr>
                    <th><?php echo $this->_tpl_vars['_personal_member']; ?>
</th>
                    <th><?php echo $this->_tpl_vars['_enterprise_member']; ?>
</th>
                  </tr>
                  <tr>
                    <td><p><?php echo $this->_tpl_vars['_personal_member_intro']; ?>
</p></td>
                    <td><p><?php echo $this->_tpl_vars['_enterprise_member_intro']; ?>
</p></td>
                  </tr>
                  <tr>
                    <td><a href="register.php?type=personal" class="btn_reg"><?php echo $this->_tpl_vars['_register_now']; ?>
<?php echo $this->_tpl_vars['_personal_member']; ?>
</a></td>
                    <td><a href="register.php?type=company" class="btn_reg"><?php echo $this->_tpl_vars['_register_now']; ?>
<?php echo $this->_tpl_vars['_enterprise_member']; ?>
</a></td>
                  </tr>
                </table>
             </div>
             <div class="block_w240"><?php echo $this->_tpl_vars['SiteDescription']; ?>
</div>
         </div>
	 </div>
  </div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>