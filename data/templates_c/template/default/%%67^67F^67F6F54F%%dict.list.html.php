<?php /* Smarty version 2.6.18, created on 2020-08-03 15:28:03
         compiled from default%5Cdict.list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'default\\dict.list.html', 22, false),array('modifier', 'truncate', 'default\\dict.list.html', 22, false),array('block', 'dict', 'default\\dict.list.html', 37, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', '11'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
	<div class="blank6"></div>  
	<div class="content clearfix">
	  <div class="main_left_w720">
	      <div class="base_title">
		     <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_search_result']; ?>
</span><span class="corner_t_r"></span></h2>
		 </div>
	     <div class="lemma_list">
		<table>
		  <tr>
			<th><?php echo $this->_tpl_vars['_word_name']; ?>
</th>
			<th><?php echo $this->_tpl_vars['_digest']; ?>
</th>
		  </tr>
		 
         <?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['Items'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['Items']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['Items']['iteration']++;
?>
		  <tr>
			<td class="title"><a href="dict/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['word']; ?>
</a></td>
			<td><a href="dict/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']['digest'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 100) : smarty_modifier_truncate($_tmp, 100)); ?>
</a></td>
		  </tr>
		 <tr><td colspan="2" class="height_1"> <hr class="hr_dashed" /></td></tr>
          <?php endforeach; endif; unset($_from); ?>
        
		</table>
	   </div>
	  </div>
	  <div class="main_right_w230">
		<div class="title_bar_s4">
	   <span class="title_top_s4"><span></span></span>
	   <h3><?php echo $this->_tpl_vars['_hot_words']; ?>
</h3>
	   </div>
	   <div class="dictionary">
		<ul>
		 <?php $this->_tag_stack[] = array('dict', array('row' => 10,'orderby' => 'hits DESC','typeid' => "")); $_block_repeat=true;smarty_block_dict($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
		  <li>[link:title]</li>
		 <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_dict($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		</ul>
	   </div>
	  </div>
	</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>