<?php /* Smarty version 2.6.18, created on 2020-07-29 13:19:28
         compiled from default%5Cspecial.industry.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'company', 'default\\special.industry.html', 14, false),array('block', 'offer', 'default\\special.industry.html', 29, false),array('block', 'news', 'default\\special.industry.html', 44, false),array('block', 'market', 'default\\special.industry.html', 59, false),array('block', 'product', 'default\\special.industry.html', 76, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
	<div class="blank6"></div>  
	<div class="content clearfix">
	  <div class="main_left_w220">
	   <div class="sub_nominate">
		<div class="title_bar_s1">
		<span class="title_top_s1"><span></span></span>
		<h3><?php echo $this->_tpl_vars['_recommend_company_database']; ?>
</h3>
		</div>
		 <ul class="sub_nominate_txt">
			<?php $this->_tag_stack[] = array('company', array('row' => '10','industryid' => ($this->_tpl_vars['item']['id']))); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
				<li><a href="[link:title]" title="[field:name]">[field:name]</a></li>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		 </ul>
		<div class="corner_bottom_bar">
			 <span class="corner_bottom_s1"><span></span></span>
		</div>
		</div>
		<div class="blank6"></div>
		<div class="sub_nominate">
            <div class="title_bar_s1">
            <span class="title_top_s1"><span></span></span>
            <h3><?php echo $this->_tpl_vars['_recommended_supply_demand']; ?>
</h3>
            </div>
		 	<ul class="sub_nominate_txt">
			<?php $this->_tag_stack[] = array('offer', array('row' => '10','industryid' => ($this->_tpl_vars['item']['id']))); $_block_repeat=true;smarty_block_offer($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
				<li><a href="[link:title]">[field:title]</a></li>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_offer($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		   </ul>
            <div class="corner_bottom_bar">
                 <span class="corner_bottom_s1"><span></span></span>
            </div>
		</div>
		<div class="blank6"></div>
		<div class="sub_nominate">
            <div class="title_bar_s1">
            <span class="title_top_s1"><span></span></span>
            <h3><?php echo $this->_tpl_vars['_similar_news']; ?>
</h3>
            </div>
		 	<ul class="sub_nominate_txt">
			<?php $this->_tag_stack[] = array('news', array('row' => '10','industryid' => ($this->_tpl_vars['item']['id']))); $_block_repeat=true;smarty_block_news($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
				<li><a href="[link:title]">[field:title]</a></li>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_news($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		   </ul>
            <div class="corner_bottom_bar">
                 <span class="corner_bottom_s1"><span></span></span>
            </div>
		</div>
		<div class="blank6"></div>
		<div class="sub_nominate">
            <div class="title_bar_s1">
            <span class="title_top_s1"><span></span></span>
            <h3><?php echo $this->_tpl_vars['_market']; ?>
</h3>
            </div>
		 	<ul class="sub_nominate_txt">
			<?php $this->_tag_stack[] = array('market', array('row' => '10','industryid' => ($this->_tpl_vars['item']['id']))); $_block_repeat=true;smarty_block_market($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
				<li><a href="[link:title]">[field:title]</a></li>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_market($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		   </ul>
            <div class="corner_bottom_bar">
                 <span class="corner_bottom_s1"><span></span></span>
            </div>
		</div>
	  </div>
	  <div class="main_right_w730">
		<div class="substation_industry">
		<h2><?php echo $this->_tpl_vars['_sub_industry']; ?>
&middot;<?php echo $this->_tpl_vars['item']['name']; ?>
</h2>
		</div>
		<div class="blank6"></div>
		<div class="substation_main"><?php echo $this->_tpl_vars['item']['description']; ?>
</div> 
        <?php if ($this->_tpl_vars['HaveProducts']): ?>  
		<ul class="substation_product clearfix">
			<?php $this->_tag_stack[] = array('product', array('type' => 'image','row' => 7,'titlelen' => '12','industryid' => ($this->_tpl_vars['item']['id']))); $_block_repeat=true;smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
				<li><a href="[link:title]"><img src="[img:src]" /></a><p><a href="[link:title]">[field:title]</a></p></li>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		 </ul>
        <?php endif; ?>
	  </div>
	</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>