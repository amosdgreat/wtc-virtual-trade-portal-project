<?php /* Smarty version 2.6.18, created on 2020-09-15 12:44:23
         compiled from default/footer.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'userpage', 'default/footer.html', 3, false),array('function', 'mailto', 'default/footer.html', 6, false),array('function', 'im', 'default/footer.html', 8, false),)), $this); ?>
<div id="footer" class="footer_class">
	<div class="ins"> 
		<?php $this->_tag_stack[] = array('userpage', array('row' => 15)); $_block_repeat=true;smarty_block_userpage($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<a href="[link:title]" title="[field:title]">[field:title]</a><span>|</span>
		<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_userpage($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
		<p><?php echo $this->_tpl_vars['site_name']; ?>
<?php echo $this->_tpl_vars['_copyright']; ?>
&copy;&nbsp;<?php echo $this->_tpl_vars['_service_hotline']; ?>
<?php echo $this->_tpl_vars['service_tel']; ?>
&nbsp;<?php echo $this->_tpl_vars['_service_email']; ?>
:<?php echo smarty_function_mailto(array('text' => ($this->_tpl_vars['service_email']),'address' => ($this->_tpl_vars['service_email']),'encode' => 'javascript'), $this);?>
&nbsp;<a href="javascript:;" onclick="$('html, body').animate({scrollTop: '0px'}, 300);return false;"><?php echo $this->_tpl_vars['_go_top']; ?>
</a>     </p>
		<p><?php echo $this->_tpl_vars['icp_number']; ?>
</p>
		<p class="footer_im"><?php echo smarty_function_im(array('type' => 'qq','id' => ($this->_tpl_vars['service_qq'])), $this);?>
<?php echo smarty_function_im(array('type' => 'msn','id' => ($this->_tpl_vars['service_msn'])), $this);?>
</p>
		<!-- <p>by SCRIPTUX.COM <a href="http://www.scriptux.com" target="_blank">Full Nulled Scripts</a></p> -->
	</div>
</div>
<script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/js/bootstrap.bundle.min.js"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/js/grouploop-1.0.0.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/dist/js/prefixfree-1.0.7.js"></script>

<!-- new style js files -->

<!-- Plugins JS File -->
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.min.js"></script>
    <!-- <begin runtime type= trus" -->
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.hoverIntent.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/superfish.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/bootstrap-input-spinner.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.plugin.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/jquery.countdown.min.js"></script>
    <!-- Main JS File -->
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/main.js"></script>
    <script src="<?php echo $this->_tpl_vars['theme_style_path']; ?>
assets/js/demos/demo-14.js"></script>

<!-- ENd of new style js files -->

  <script>
    
    $('#grouploop-3').grouploop({
      velocity: 1,
      forward: false,
      pauseOnHover: true,
      childNode: ".item",
      childWrapper: ".item-wrap"
    });
  </script>

</body>
</html>