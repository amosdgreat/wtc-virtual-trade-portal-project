<?php /* Smarty version 2.6.18, created on 2020-07-29 13:21:54
         compiled from default%5Cregister.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_member_reg'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<style type="text/css">  
label.error {
  font-weight: bold;
  color: #b80000;
}
</style>
<div class="wrapper">
	<div class="subnav">
        <div class="member_register"><span class="step_11"><?php echo $this->_tpl_vars['_fill_reg_info']; ?>
</span><span class="step_02"><?php echo $this->_tpl_vars['_reg_success']; ?>
</span></div>
     </div>
     <div class="blank6"></div>
     <div class="base_title">
       <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_fill_reg_info']; ?>
</span><span class="corner_t_r"></span></h2> 
	</div>
	<div class="registercon box_bord">
            <form name="regfrm" id="regfrm" method="post" action="" autocomplete="off" onsubmit="$('#Submit').attr('disabled',true);">
            <input type="hidden" name="formhash" value="<?php echo $this->_tpl_vars['formhash']; ?>
" />
            <input type="hidden" name="register" value="<?php echo $_GET['type']; ?>
" />
            <input type="hidden" name="forward" value="<?php echo $_GET['forward']; ?>
" />
			<label class="registerlabel" for="dataMemberUsername">
			<?php echo $this->_tpl_vars['_member_login_name']; ?>
<span>*</span>
			</label>
			<label>
			<input type="text" name="data[member][username]" id="dataMemberUsername" value="" />
			</label><br />
			<label class="lenglabel" id="membernameDiv">
			<?php echo $this->_tpl_vars['_member_login_name_conditions']; ?>

			</label>
            <br clear="all" />
			<label class="registerlabel" for="dataMemberEmail">
			E-mail<?php echo $this->_tpl_vars['_colon']; ?>
<span>*</span>
			</label>
			<label>
			<input name="data[member][email]" id="dataMemberEmail" value="" />
			</label><br />
			<label class="lenglabel" id="memberemailDiv">
			<?php echo $this->_tpl_vars['_email_conditions']; ?>

			</label><br clear="all" />
			<label class="registerlabel" for="memberpass">
			<?php echo $this->_tpl_vars['_password']; ?>
<span>*</span>
			</label>
			<label>
			<input type="password" name="data[member][userpass]" id="memberpass" value="">
			</label><br />
			<label class="lenglabel">
			<?php echo $this->_tpl_vars['_password_conditions']; ?>

			</label>
			<br clear="all" />
			<label class="registerlabel" for="re_memberpass">
			<?php echo $this->_tpl_vars['_re_enter_password']; ?>
<span>*</span>
			</label>
			<label>
			<input name="re_memberpass" type="password" id="re_memberpass" value="">
			</label><br />
			<label class="lenglabel">
			<?php echo $this->_tpl_vars['_re_enter_up_password']; ?>

			</label>
            <br clear="all" />
            <?php if ($this->_tpl_vars['ifcapt']): ?>
			<label class="registerlabel" for="login_auth">
			<?php echo $this->_tpl_vars['_code']; ?>

			</label>
			<span class="registercheck"><img class="registerpic" width="123" height="50" id="imgcaptcha" src="captcha.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" alt="<?php echo $this->_tpl_vars['_unclear_see_numbers']; ?>
" title="<?php echo $this->_tpl_vars['_unclear_see_numbers']; ?>
" />
			<p>
			<object type="application/x-shockwave-flash" data="images/play.swf?audio=captcha.php&amp;do=play&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000" height="19" width="19">
			<param name="movie" value="images/play.swf?audio=captcha.php&amp;do=play&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000" />
			</object>
			<br /><a href="javascript:;"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/gongqiu03.jpg" class="registerpic2" id="exchange_imgcapt" /></a></p><br clear="all" /></span><br clear="all" />
			<label class="checkinput">
			<input name="data[capt_register]" id="login_auth" type="text" value="" size="4" style="width:60px;">&nbsp;&nbsp;<font class="gray"><?php echo $this->_tpl_vars['_input_code']; ?>
</font>
			</label>
            <br clear="all" />
            <?php endif; ?>
			<p class="registerp2"><input name="licence_check" id="LicenseCheck" type="checkbox" onclick="if(this.checked)$('#Submit').removeAttr('disabled'); else $('#Submit').attr('disabled','true');" checked="checked" /><label for="LicenseCheck"><?php echo $this->_tpl_vars['_see_agree']; ?>
</label></p>
            <br clear="all" />
			<p class="registerbutton"><input type="submit" name="Submit" id="Submit" value="<?php echo $this->_tpl_vars['_register']; ?>
" class="submit_w67" /></p>
			</form>
		</div>
 </div>
<script language="javascript" src="scripts/jquery/validate.js"></script>
<script language="javascript" src="scripts/validate.js" charset="<?php echo $this->_tpl_vars['Charset']; ?>
"></script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>