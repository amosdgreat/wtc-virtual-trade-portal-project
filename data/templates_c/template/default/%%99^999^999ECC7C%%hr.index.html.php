<?php /* Smarty version 2.6.18, created on 2020-08-03 15:05:41
         compiled from default%5Chr.index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'default\\hr.index.html', 33, false),array('block', 'job', 'default\\hr.index.html', 91, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_hr'])); ?>
<?php $this->assign('nav_id', '9'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="content">
		<div class="job_type fl">
          <div class="title_bar_s1" style="height:28px;">
           <span class="title_top_s1"><span></span></span>
           <h3><?php echo $this->_tpl_vars['_industry_sort']; ?>
</h3>
          </div>
			
			<div class="job_type_c">
            	<?php $_from = $this->_tpl_vars['IndustryList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['IndustryList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['IndustryList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['industry_item']):
        $this->_foreach['IndustryList']['iteration']++;
?>
			  <h3><em><a href="hr/list.php?industryid=<?php echo $this->_tpl_vars['industry_item']['id']; ?>
" title="<?php echo $this->_tpl_vars['industry_item']['name']; ?>
"><?php echo $this->_tpl_vars['industry_item']['name']; ?>
</a></em></h3>
				<div class="cb">
                	<?php $_from = $this->_tpl_vars['industry_item']['child']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['industry_key2'] => $this->_tpl_vars['industry_item2']):
?>
                    <a href="hr/list.php?industryid=<?php echo $this->_tpl_vars['industry_item2']['id']; ?>
" title="<?php echo $this->_tpl_vars['industry_item2']['name']; ?>
"><?php echo $this->_tpl_vars['industry_item2']['name']; ?>
</a><em>|</em>
                    <?php endforeach; endif; unset($_from); ?>
              </div>
                <?php endforeach; endif; unset($_from); ?>
		  </div>
		</div>
        
		<div class="job_ss fl">
			<form action="hr/list.php" method="get">
            <input type="hidden" name="do" value="search" />
			<h2><?php echo $this->_tpl_vars['_jobs_search']; ?>
</h2>
			<table width="390" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td align="right" width="65"><?php echo $this->_tpl_vars['_salary']; ?>
</td>
				<td width="325" align="left">
                <select name="data[salary_id]">
						<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['Salary']), $this);?>

						</select></td>
			  </tr>
			  <tr>
				<td align="right"><?php echo $this->_tpl_vars['_jobs_area']; ?>
</td>
				<td align="left">
                <div id="dataArea">
							<select name="data[area_id1]" id="dataAreaId1" class="level_1" style="width:90px;" ></select>
							<select name="data[area_id2]" id="dataAreaId2" class="level_2" style="width:90px;"></select>
							<select name="data[area_id3]" id="dataAreaId3" class="level_3" style="width:90px;"></select>
						</div>
                        </td>
			  </tr>
			  <tr>
				<td align="right"><?php echo $this->_tpl_vars['_keyword']; ?>
</td>
				<td align="left">
					<input type="text" name="q" value="" class="job_gjz" />
					</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td align="left"><input type="submit" value="<?php echo $this->_tpl_vars['_search']; ?>
" class="submit" /></td>
			  </tr>
			</table>
			</form>
		</div>
		<div class="job_reg fr">
            <div class="title_bar_s1">
               <span class="title_top_s1"><span></span></span>
               <h5><?php echo $this->_tpl_vars['_after_free_reg']; ?>
</h5>
            </div>

			<div class="job_reg_c">
				<p class="fl"><a><?php echo $this->_tpl_vars['_build_your_site']; ?>
</a></p>
				<p class="fl"><a><?php echo $this->_tpl_vars['_post_offer_trade']; ?>
</a></p>
				<p class="fl"><a><?php echo $this->_tpl_vars['_spread_your_products']; ?>
</a></p>
				<p class="fl"><a><?php echo $this->_tpl_vars['_meet_more_friends']; ?>
</a></p>
				<div class="reg big-color-weight">
                <span><a href="member.php" class="btn_free"><?php echo $this->_tpl_vars['_free_reg']; ?>
</a><span>
                </div>
			</div>
		</div>
		<div class="job_area fl">
			<h2><?php echo $this->_tpl_vars['_hot_area']; ?>
</h2>
			<div>
            <?php $_from = $this->_tpl_vars['Areas'][1]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['area'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['area']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['area_key'] => $this->_tpl_vars['area_item']):
        $this->_foreach['area']['iteration']++;
?>
            <a href="hr/list.php?areaid=<?php echo $this->_tpl_vars['area_key']; ?>
"><?php echo $this->_tpl_vars['area_item']; ?>
</a>
            <?php endforeach; endif; unset($_from); ?>
          </div>
		</div>
		<div class="job_new fl">
			<div class="base_title">
                 <span class="more"><a href="hr/list.php"><?php echo $this->_tpl_vars['_more']; ?>
&raquo;</a></span>
				<h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_new_hr']; ?>
</span><span class="corner_t_r"></span></h2> 
				
			</div>
			<div class="job_new_c fl box_bord">
				<ul>
                <?php $this->_tag_stack[] = array('job', array('row' => 28)); $_block_repeat=true;smarty_block_job($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
					<li><p class="fl"><a href="[link:title]" title="[field:fulltitle]" target="_blank">[field:title]</a></p><em class="fr">[field:companyname]</em></li>
                <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_job($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			  </ul>
			</div>
		</div>
		<div class="blank6"></div>
		</div>
</div>
<script>
var app_language = '<?php echo $this->_tpl_vars['AppLanguage']; ?>
';
var area_id1 = 0;
var area_id2 = 0;
var area_id3 = 0;
</script>
<script src="scripts/multi_select.js" charset="<?php echo $this->_tpl_vars['Charset']; ?>
"></script>
<script src="scripts/script_area.js"></script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>