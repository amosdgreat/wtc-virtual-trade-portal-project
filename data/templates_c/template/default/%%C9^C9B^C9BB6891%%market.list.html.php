<?php /* Smarty version 2.6.18, created on 2020-07-29 13:17:45
         compiled from default%5Cmarket.list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'company', 'default\\market.list.html', 38, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', '7'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
  <div class="blank6"></div>
<div class="content clearfix">
		<div class="market_list fl">
           <div class="title_bar_s4">
            <span class="title_top_s4"><span></span></span>
            <h3><?php echo $this->_tpl_vars['SearchAmount']; ?>
<?php if ($this->_tpl_vars['Amount']): ?>:<?php echo $this->_tpl_vars['Amount']; ?>
<?php endif; ?></h3>
           </div>
			<div class="market_list_c">
				<ul>
                	<?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['market'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['market']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['market']['iteration']++;
?>
					<li>
						<?php if ($this->_tpl_vars['item']['picture']): ?><a href="market/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
"><img src="<?php echo $this->_tpl_vars['item']['image']; ?>
" alt="<?php echo $this->_tpl_vars['item']['name']; ?>
" class="fl" /></a><?php endif; ?>
                        <div class="fl">
						<h2><a href="market/detail.php?id=<?php echo $this->_tpl_vars['item']['id']; ?>
" title=""><?php echo $this->_tpl_vars['item']['name']; ?>
</a><?php if ($this->_tpl_vars['item']['area_id1']): ?><em>(<?php echo $this->_tpl_vars['AreaItems'][1][$this->_tpl_vars['item']['area_id1']]; ?>
<?php echo $this->_tpl_vars['AreaItems'][2][$this->_tpl_vars['item']['area_id2']]; ?>
)</em><?php endif; ?></h2>
						<p><?php echo $this->_tpl_vars['_introduction']; ?>
<?php echo $this->_tpl_vars['item']['digest']; ?>
</p>
                        </div>
                        <hr class="hr_dashed" />
					</li>
                    <?php endforeach; endif; unset($_from); ?>
				</ul>
				<div><?php echo $this->_tpl_vars['ByPages']; ?>
</div>
			</div>
		</div>
        
         <div class="market_list_right">
		<div class="job_good mart_good">
            <div class="title_bar_s4">
            <span class="title_top_s4"><span></span></span>
            <h3><?php echo $this->_tpl_vars['_industry_recommended']; ?>
</h3>
            </div>
			<div class="job_good_c cb">
				<ul>
                	<?php $this->_tag_stack[] = array('company', array('row' => 12,'industryid' => ($this->_tpl_vars['IndustryIds']))); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
					<li><a href="[link:title]" title="[field:fulltitle]" target="_blank">[field:title]</a></li>
                    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
				</ul>
			</div>
		</div>
        <div class="blank6"></div>
		<div class="job_good mart_good">
            <div class="title_bar_s4">
            <span class="title_top_s4"><span></span></span>
             <h3><?php echo $this->_tpl_vars['_area_recommended']; ?>
</h3>
            </div>
			<div class="job_good_c cb">
				<ul>
                	<?php $this->_tag_stack[] = array('company', array('row' => 12,'areaid' => ($this->_tpl_vars['AreaIds']))); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
					<li><a href="[link:title]" title="[field:fulltitle]" target="_blank">[field:title]</a></li>
                    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
				</ul>
			</div>
		</div>
		<div class="market_add"><?php echo $this->_tpl_vars['_not_exist_market']; ?>
<br /><?php echo $this->_tpl_vars['_click_us']; ?>

			<p><a href="market/add.php"><?php echo $this->_tpl_vars['_added_market']; ?>
</a></p>
		</div>

        </div>
        </div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>