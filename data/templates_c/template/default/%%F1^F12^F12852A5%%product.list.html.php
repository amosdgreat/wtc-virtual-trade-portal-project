<?php /* Smarty version 2.6.18, created on 2020-07-29 12:06:35
         compiled from default%5Cproduct.list.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'default\\product.list.html', 22, false),array('modifier', 'truncate', 'default\\product.list.html', 57, false),array('block', 'product', 'default\\product.list.html', 83, false),array('block', 'company', 'default\\product.list.html', 90, false),)), $this); ?>
<?php $this->assign('page_title', ($this->_tpl_vars['page_title'])); ?>
<?php $this->assign('nav_id', '5'); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
	<div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
    <div class="blank6"></div>
	<div class="kinds clearfix">
		<p>
		<span><?php echo $this->_tpl_vars['_industry_screening']; ?>
</span>
		<?php $_from = $this->_tpl_vars['OtherIndustry']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['subindustry'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['subindustry']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key1'] => $this->_tpl_vars['item1']):
        $this->_foreach['subindustry']['iteration']++;
?>
		<a href="product/list.php?industryid=<?php echo $this->_tpl_vars['key1']; ?>
" title="<?php echo $this->_tpl_vars['item1']; ?>
"><?php echo $this->_tpl_vars['item1']; ?>
</a>
		<?php endforeach; else: ?>
		<a><?php echo $this->_tpl_vars['_no_subclass']; ?>
</a>
		<?php endif; unset($_from); ?>
		</p>
		<form name="search_frm" id="ProductSearchFrm" action="" method="get">
		<p class="group2">
		<span><?php echo $this->_tpl_vars['_fast_screening']; ?>
</span>
		<label>
		<select name="industryid" onchange="$('#OfferSearchFrm').submit();">
			<option value="0"><?php echo $this->_tpl_vars['_all_sort']; ?>
</option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['OtherIndustry'],'selected' => $_GET['industryid']), $this);?>

		</select>
		</label>
		<label>
			<select name="areaid" onchange="$('#OfferSearchFrm').submit();">
				<option value="0"><?php echo $this->_tpl_vars['_all_area']; ?>
</option>
				<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['OtherArea'],'selected' => $_GET['areaid']), $this);?>

			</select>
		</label>
		<label><input type="submit"  value="<?php echo $this->_tpl_vars['_screening']; ?>
" class="submit"/>
		</label>
      
		</p>
		</form>
	</div>

<div class="qiugoucontent clearfix">
	<div class="qiugoucontentleft">
    <div class="base_title">
         <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_search_result']; ?>
</span><span class="corner_t_r"></span></h2> 
	</div>
		<div class="qiugouleftcon box_bord">
			<form>
            <table width="100%" border="0">
              <tr>
                <th class="offer_img"><?php echo $this->_tpl_vars['_products_picture']; ?>
</th>
                <th class="offer_info"><?php echo $this->_tpl_vars['_supply_information_company']; ?>
</th>
                <th class="offer_area"><?php echo $this->_tpl_vars['_all_area']; ?>
</th>
                <th class="offer_level"><?php echo $this->_tpl_vars['_reputation_index']; ?>
</th>
                <th class="offer_contact"><?php echo $this->_tpl_vars['_online_contact']; ?>
</th>
              </tr>
              <?php $_from = $this->_tpl_vars['Items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
              <tr>
                <td class="offer_img"><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
"><img src="<?php echo $this->_tpl_vars['item']['image']; ?>
" border=0 alt="<?php echo $this->_tpl_vars['item']['name']; ?>
" width="80" height="80"></a></td>
                <td class="offer_info title_link"><span><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
" title="<?php echo $this->_tpl_vars['item']['name']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></span><?php echo $this->_tpl_vars['item']['pubdate']; ?>
<br />
						<?php echo $this->_tpl_vars['_abstract']; ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['item']['content'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 100) : smarty_modifier_truncate($_tmp, 100)); ?>
<br /><?php echo $this->_tpl_vars['_publisher']; ?>
<?php if ($this->_tpl_vars['item']['companyname']): ?><a href="space.php?userid=<?php echo $this->_tpl_vars['item']['userid']; ?>
"><?php echo $this->_tpl_vars['item']['companyname']; ?>
</a><?php else: ?><?php echo $this->_tpl_vars['item']['username']; ?>
<?php endif; ?></td>
                <td class="offer_area"><?php if ($this->_tpl_vars['item']['area_id1']): ?><?php echo $this->_tpl_vars['Areas'][1][$this->_tpl_vars['item']['area_id1']]; ?>
<br /><?php echo $this->_tpl_vars['Areas'][2][$this->_tpl_vars['item']['area_id2']]; ?>
<?php endif; ?></td>
                <td class="offer_level"><img src="<?php echo $this->_tpl_vars['item']['gradeimg']; ?>
" alt="<?php echo $this->_tpl_vars['item']['gradename']; ?>
" ></td>
                <td  class="offer_contact">
                    <span><?php echo $this->_tpl_vars['item']['im']; ?>
</span><br />
                    <span class="two">
                    <?php if ($this->_tpl_vars['item']['companyname'] != ""): ?>
                    <a href="space.php?do=feedback&userid=<?php echo $this->_tpl_vars['item']['userid']; ?>
"><?php echo $this->_tpl_vars['_station_message']; ?>
</a>
                    <?php else: ?>
                    <a href="office-room/pms.php?do=send&to=<?php echo $this->_tpl_vars['item']['username']; ?>
"><?php echo $this->_tpl_vars['_send_message']; ?>
</a>
                    <?php endif; ?>
                    </span>
               </td>
              </tr>
              	<?php endforeach; endif; unset($_from); ?>
            </table>

			<div>
				<span><?php echo $this->_tpl_vars['ByPages']; ?>
</span>
			</div>
			</form>
		</div>
	</div>
	<div class="qiugoucontentright">
		<div class="recommendcompany">
			<div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" />&nbsp;<?php echo $this->_tpl_vars['_commend']; ?>
<?php echo $this->_tpl_vars['_offer']; ?>
</div>
			<?php $this->_tag_stack[] = array('product', array('row' => 10,'type' => 'commend')); $_block_repeat=true;smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<p><a href="[link:title]">[field:title]</a></p>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_product($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			<div class="clear"></div>
		</div>
		<div class="recommendcompany">
			<div class="recommendcompanytop"><img src="<?php echo $this->_tpl_vars['theme_img_path']; ?>
images/lhighs.gif" />&nbsp;<?php echo $this->_tpl_vars['_recommended_company']; ?>
</div>
			<?php $this->_tag_stack[] = array('company', array('row' => 15)); $_block_repeat=true;smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<p><a href="[link:title]" target="_blank">[field:title]</a></p>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_company($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			<div class="clear"></div>
		</div>
</div>
</div>
<script>
$("#SearchFrm").attr("action","offer/list.php");
$("#topMenuProduct").addClass("lcur");
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>