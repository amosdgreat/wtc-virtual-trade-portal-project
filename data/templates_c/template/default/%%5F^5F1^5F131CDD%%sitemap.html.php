<?php /* Smarty version 2.6.18, created on 2020-07-29 11:42:57
         compiled from default%5Csitemap.html */ ?>
<?php $this->assign('page_title', ($this->_tpl_vars['_site_map'])); ?>
<?php $this->assign('nav_id', ($this->_tpl_vars['nav_id'])); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="wrapper">
  <div class="content">
    <div class="tips"><span><?php echo $this->_tpl_vars['position']; ?>
</span></div>
    <div class="webmap">
      <div class="webmap_left">
      <div class="base_title">
       <h2><span class="corner_t_l"></span><span class="corner_t_m title_mouse"><?php echo $this->_tpl_vars['_basic_info']; ?>
</span><span class="corner_t_r"></span></h2>
	</div>
        <div class="base_content">
          <div class="webmap_left_con1">
            <h2><?php echo $this->_tpl_vars['_offer_info']; ?>
</h2>
            <p><a href="buy/"><?php echo $this->_tpl_vars['_Buy']; ?>
</a> <a href="sell/"><?php echo $this->_tpl_vars['_supply']; ?>
</a> <a href="offer/list.php?typeid=8"><?php echo $this->_tpl_vars['_inventory']; ?>
</a> <a href="offer/list.php?typeid=5"><?php echo $this->_tpl_vars['_merchants']; ?>
</a> 
			<a href="offer/list.php?typeid=6"><?php echo $this->_tpl_vars['_join']; ?>
</a> <a href="offer/list.php?typeid=4"><?php echo $this->_tpl_vars['_cooperation']; ?>
</a> <a href="offer/list.php?typeid=3"><?php echo $this->_tpl_vars['_agent']; ?>
</a>
			<a href="offer/list.php?typeid=7"><?php echo $this->_tpl_vars['_wholesale']; ?>
</a></p>
          </div>
          <div class="webmap_left_con2">
            <h2><?php echo $this->_tpl_vars['_important_channel']; ?>
</h2>
            <p><a href="company/"><?php echo $this->_tpl_vars['_yellow_page']; ?>
</a>&nbsp;<a href="company/list.php?type=commend"><?php echo $this->_tpl_vars['_recommend_company_database']; ?>
</a>&nbsp;<a href="product/"><?php echo $this->_tpl_vars['_nventory']; ?>
</a>&nbsp;<a href="news/"><?php echo $this->_tpl_vars['_information']; ?>
</a>&nbsp;<a href="market/"><?php echo $this->_tpl_vars['_professional_market']; ?>
</a>&nbsp;<a href="fair"><?php echo $this->_tpl_vars['_fair']; ?>
</a>&nbsp;<a href="hr/"><?php echo $this->_tpl_vars['_hr']; ?>
</a></p>
          </div>
        </div>
      </div>
      <div class="webmap_right">
       <div class="title_bar_s4">
              <span class="title_top_s4"><span></span></span>
              <h3><?php echo $this->_tpl_vars['_quick_navigation']; ?>
</h3>
           </div>
      
        <div class="webmap_right_con">
          <ul>
            <li><a href="offer/post.php"><?php echo $this->_tpl_vars['_release_supply_offer']; ?>
</a></li>
            <li><a href="service.php"><?php echo $this->_tpl_vars['_client_terms_Service']; ?>
</a></li>
            <li><a href="agreement.php" target="_blank"><?php echo $this->_tpl_vars['_web_login_pact']; ?>
</a></li>
          </ul>
        </div>
      </div>
    </div><br clear="all"/>
  </div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['ThemeName'])."/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>