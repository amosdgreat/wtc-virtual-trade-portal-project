<?php /* Smarty version 2.6.18, created on 2020-07-29 11:17:51
         compiled from setting.basic.desc.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'editor', 'setting.basic.desc.html', 31, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_site_info']; ?>
</p>
</div>
<div id="rightTop">
	<h3><?php echo $this->_tpl_vars['_site_info']; ?>
</h3>
	<ul class="subnav">
		<li><a href="setting.php?do=basic"><?php echo $this->_tpl_vars['_basic_setting']; ?>
</a></li>
		<li><a href="setting.php?do=basic_contact"><?php echo $this->_tpl_vars['_contact_method']; ?>
</a></li>
		<li><a class="btn1" href="setting.php?do=basic_desc"><span><?php echo $this->_tpl_vars['_site_desc']; ?>
</span></a></li>
	</ul>
</div>
<div class="info">
  <form method="post">
    <table class="infoTable">
      <tr>
        <th class="paddingT15"><label for="price_format"> <?php echo $this->_tpl_vars['_site_desc_n']; ?>
</label></th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[setting1][site_description]" id="SiteDescription" style="width:650px;height:300px;" ><?php echo $this->_tpl_vars['item']['SITE_DESCRIPTION']; ?>
</textarea>
          <span class="gray"><?php echo $this->_tpl_vars['_site_desc_and_show']; ?>
</span>
		</td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20"><input class="formbtn" type="submit" name="savebasic" value="<?php echo $this->_tpl_vars['_submit']; ?>
" />
          <input class="formbtn" type="reset" name="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" />
        </td>
      </tr>
    </table>
  </form>
</div>
<?php echo smarty_function_editor(array('type' => 'ckeditor','element' => 'SiteDescription','toolbar' => 'Full'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>