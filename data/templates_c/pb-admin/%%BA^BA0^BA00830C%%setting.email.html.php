<?php /* Smarty version 2.6.18, created on 2020-08-14 16:29:44
         compiled from setting.email.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'setting.email.html', 25, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script src="../scripts/jquery/facebox.js" type="text/javascript"></script>
<link href="../images/facebox/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; Email settings</p>
</div>
<div id="rightTop"> 
    <h3>Email settings</h3> 
</div>
<div class="info"> 
    <form method="post" action="setting.php" name="edit_frm" id="EditFrm">
    <input type="hidden" name="do" id="Do" value="" />
        <table class="infoTable"> 
            <tr> 
                <th class="paddingT15"> 
              <label for="email_type">Mail sending method:</label></th> 
          <td class="paddingT15 wordSpacing5"> 
                    <label for="mail_sendtype1"><input type="radio" name="data[setting][send_mail]" id="mail_sendtype1" value="1">&nbsp;Use the built-in Mail service of the server(<?php echo $this->_tpl_vars['_commend']; ?>
This way)</label>&nbsp;<label for="mail_sendtype2"><input type="radio" name="data[setting][send_mail]" id="mail_sendtype2" value="2">&nbsp;Use other SMTP services</label>&nbsp;</td> 
            </tr>
            <tbody id="smtp_set" style="<?php if ($this->_tpl_vars['item']['SEND_MAIL'] == 1): ?>display:none;<?php endif; ?>">
            <tr> 
                <th class="paddingT15"> 
                    SMTP server address:</th> 
                <td class="paddingT15 wordSpacing5"> 
                    <input class="infoTableInput" type="text" name="data[setting][smtp_server]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['SMTP_SERVER'])) ? $this->_run_mod_handler('default', true, $_tmp, 'smtp.qq.com') : smarty_modifier_default($_tmp, 'smtp.qq.com')); ?>
" /></td> 
            </tr> 
            <tr> 
                <th class="paddingT15"> 
                    SMTP Server port:</th> 
                <td class="paddingT15 wordSpacing5"> 
                    <input class="infoTableInput" type="text" name="data[setting][smtp_port]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['SMTP_PORT'])) ? $this->_run_mod_handler('default', true, $_tmp, '25') : smarty_modifier_default($_tmp, '25')); ?>
" /></td> 
            </tr> 
            <tr> 
                <th class="paddingT15"> 
                    SMTP Authentication required:</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input type="radio" name="data[setting][smtp_auth]" id="smtp_auth1" value="1" <?php if ($this->_tpl_vars['item']['SMTP_AUTH'] == '1'): ?>checked="checked"<?php endif; ?> />Yes
                <input type="radio" name="data[setting][smtp_auth]" id="smtp_auth2" value="0" <?php if ($this->_tpl_vars['item']['SMTP_AUTH'] == '0'): ?>checked="checked"<?php endif; ?> />No
                </td> 
            </tr> 
            <tr> 
                <th class="paddingT15"> 
                    Sender’s email address:</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input class="infoTableInput" type="text" name="data[setting][mail_from]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['MAIL_FROM'])) ? $this->_run_mod_handler('default', true, $_tmp, 'email@host.com') : smarty_modifier_default($_tmp, 'email@host.com')); ?>
" /></td> 
            </tr>
            <tr> 
                <th class="paddingT15"> 
                    Name of sender:</th> 
                <td class="paddingT15 wordSpacing5"> 
                    <input class="infoTableInput" type="text" name="data[setting][mail_fromwho]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['MAIL_FROMWHO'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['sitetitle']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['sitetitle'])); ?>
" /></td> 
            </tr> 
            <tr> 
                <th class="paddingT15"> 
                    SMTP Authentication<?php echo $this->_tpl_vars['_cp_username_n']; ?>
</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input class="infoTableInput" type="text" name="data[setting][auth_username]" id="auth_username" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['AUTH_USERNAME'])) ? $this->_run_mod_handler('default', true, $_tmp, 'username') : smarty_modifier_default($_tmp, 'username')); ?>
" /></td> 
            </tr>
            <tr> 
                <th class="paddingT15"> 
                    SMTP Authentication method:</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input class="infoTableInput" type="text" name="data[setting][auth_protocol]" id="auth_auth_protocol" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['AUTH_PROTOCOL'])) ? $this->_run_mod_handler('default', true, $_tmp, '') : smarty_modifier_default($_tmp, '')); ?>
" /><span class="gray">Don’t fill in unless you need it. For example, Gmail mailbox needs to be set as ssl</span></td> 
            </tr>
            <tr> 
                <th class="paddingT15"> 
                    SMTP Authentication password:</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input class="infoTableInput" name="data[setting][auth_password]" type="password" id="auth_password" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['AUTH_PASSWORD'])) ? $this->_run_mod_handler('default', true, $_tmp, 'password') : smarty_modifier_default($_tmp, 'password')); ?>
"/></td> 
            </tr>
            </tbody>
            <tr> 
                <th class="paddingT15"> 
                    Test email address:</th> 
                <td class="paddingT15 wordSpacing5"> 
                <input class="infoTableInput" type="text" name="data[setting][testemail]" id="dataTestemail" value="<?php echo $this->_tpl_vars['item']['TESTEMAIL']; ?>
" />&nbsp;&nbsp;<input class="formbtn" type="button" name="test" id="Test" value="test" /></td> 
            </tr> 
            <tr> 
            <th></th> 
            <td class="ptb20"> 
                <input class="formbtn" type="submit" name="save_mail" id="SaveEmail" value="<?php echo $this->_tpl_vars['_save']; ?>
" /> 
                <input class="formbtn" type="reset" name="reset" id="ResetEmail" value="<?php echo $this->_tpl_vars['_reset']; ?>
" /> 
            </td> 
        </tr> 
        </table> 
    </form> 
</div> 
<script language="JavaScript" type="text/JavaScript">
jQuery(document).ready(function($) {
	$("#dataTestemail").focus(function(){
	if($("#dataTestemail").val()=='<?php echo $this->_tpl_vars['service_email']; ?>
'){
		$("#dataTestemail").val("")};
	}).blur(function(){
	if($("#dataTestemail").val()==''){
		$("#dataTestemail").val("<?php echo $this->_tpl_vars['service_email']; ?>
").css("color","#ccc")};
	});
	$("#mail_sendtype<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['SEND_MAIL'])) ? $this->_run_mod_handler('default', true, $_tmp, '1') : smarty_modifier_default($_tmp, '1')); ?>
").attr("checked",'checked');
	$("#mail_sendtype2").click(
		function(){
			$("#smtp_set").show();
		}
	);
	$("#mail_sendtype1").click(
		function(){
			$("#smtp_set").hide();
		}
	);
	$("#Test").click(
		function(){
			$("#Do").val("testemail");
			$.facebox.settings.loadingImage = '<?php echo $this->_tpl_vars['SiteUrl']; ?>
images/facebox/loading.gif'; 
			$.facebox.settings.closeImage = '<?php echo $this->_tpl_vars['SiteUrl']; ?>
images/facebox/closelabel.gif'; 
			jQuery.facebox('<?php echo $this->_tpl_vars['_on_sending']; ?>
...');
			$("#EditFrm").submit();
		}
	);
})
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>