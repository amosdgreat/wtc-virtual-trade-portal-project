<?php /* Smarty version 2.6.18, created on 2020-08-20 11:36:57
         compiled from area.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'area.edit.html', 65, false),array('modifier', 'default', 'area.edit.html', 65, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_area']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_area']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="area.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
        <li><a class="btn1" href="area.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
        <li><a href="areatype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
        <li><a href="area.php?do=clear"><?php echo $this->_tpl_vars['_clearing']; ?>
</a></li>
        <li><a href="area.php?do=refresh"><?php echo $this->_tpl_vars['_update_cache']; ?>
</a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="area.php" id="EditFrm" name="edit_frm">
    <table class="infoTable">
    <?php if ($_GET['id']): ?>
      <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
" />
      <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>
" />
      <tr>
        <th class="paddingT15"> Numbering:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[area][id]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" type="text" />        </td>
      </tr>
      <tr>
        <th class="paddingT15"> District name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[area][name]" value="<?php echo $this->_tpl_vars['item']['name']; ?>
" type="text" />        </td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_digest_n']; ?>
</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[area][description]"><?php echo $this->_tpl_vars['item']['description']; ?>
</textarea></td>
      </tr>
    <?php else: ?>
      <tr>
        <th class="paddingT15"> District name:</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[names]" id="dataNames"></textarea><label class="field_notice">You can add multiple, one line represents one area</label></td>
      </tr>
    <?php endif; ?>
      <tr>
        <th class="paddingT15"> Superior area:</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[area][parent_id]" id="dataParentId">
        <option value="0">no</option>
        <?php echo $this->_tpl_vars['CacheItems']; ?>

        </select>
       </td>
      </tr>
      <tr>
        <th class="paddingT15"> Belongs to the series:</th>
        <td class="paddingT15 wordSpacing5">
         <select name="data[area][areatype_id]" id="dataTypeId">
			<option value="0">no</option>
            <?php $_from = $this->_tpl_vars['Types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key3'] => $this->_tpl_vars['item3']):
?>
            <option value="<?php echo $this->_tpl_vars['key3']; ?>
" <?php if ($this->_tpl_vars['key3'] == $this->_tpl_vars['item']['areatype_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['item3']; ?>
</option>
            <?php endforeach; endif; unset($_from); ?>
        </select>   
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> link address:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[area][url]" value="<?php echo $this->_tpl_vars['item']['url']; ?>
" type="text" /><label class="field_notice">Is empty, the system default address</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Whether to display:</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[area][available]",'options' => $this->_tpl_vars['AskAction'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['available'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)),'separator' => ""), $this);?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_display_order']; ?>
</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[area][display_order]" type="text" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['display_order'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
"></td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" />		</td>
      </tr>
    </table>
  </form>
</div>
<script>
jQuery(document).ready(function($) {
	$('#dataParentId').val("<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['parent_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
");
	$('#dataTypeId').val("<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['areatype_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
");
})
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>