<?php /* Smarty version 2.6.18, created on 2020-08-14 15:21:18
         compiled from member.email.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'member.email.html', 32, false),array('function', 'editor', 'member.email.html', 63, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_member_center']; ?>
 &raquo; <?php echo $this->_tpl_vars['_user_center']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_member']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="member.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
        <li><a href="member.php?do=edit"><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</a></li>
        <li><a href="membertype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
        <li><a class="btn1" href="memberemail.php"><span>Email alert</span></a></li>        
    </ul>
</div>
<div class="info">
  <form method="post" action="" id="EditFrm" name="edit_frm">
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> Sending conditions:</th>
        <td class="paddingT15 wordSpacing5">
		<input type="checkbox" name="data[all]" value="1" id="data_all" onclick='$("#condition").toggle();'>&nbsp;<label for="data_all">All(Ignore all the following conditions)</label><br />
		<div id="condition">
		<input type="text" name="data[day]" style="width:18px;">&nbsp;Users who have not logged in in the day<br />
		user ID(ID) in<input type="text" name="data[id][from]" style="width:18px;">versus<input type="text" name="data[id][to]" style="width:18px;">between
		</div>
		</td>
      </tr>   
      <tr>
        <th class="paddingT15"> User Type:</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[membertype_id]" id="dataMemberTypeId">
		<option value="0">Not specify</option>
        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['Membertypes'],'selected' => $this->_tpl_vars['item']['membertype_id']), $this);?>

        </select>
        </td>
      </tr>
      <tr>
        <th class="paddingT15">Member Group:</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[membergroup_id]" id="dataMemberGroupId">
		<option value="0">Not specify</option>
        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['MembergroupOptions'],'selected' => $this->_tpl_vars['item']['membergroup_id']), $this);?>

        </select>
        <?php if ($this->_tpl_vars['item']['groupimage']): ?><img src="<?php echo $this->_tpl_vars['item']['groupimage']; ?>
" /><?php endif; ?>
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_title_n']; ?>
</th>
        <td class="paddingT15 wordSpacing5">          
		<input name="data[subject]" value="" required="" /></td>
      </tr>      
      <tr>
        <th class="paddingT15">Content of Email:</th>
        <td class="paddingT15 wordSpacing5"><textarea style="width:550px;height:100px;" name="data[content]" required="" id="NewsContent"><?php echo $this->_tpl_vars['item']['content']; ?>
</textarea></td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="send" value="send immediately" />		</td>
      </tr>
    </table>
  </form>
</div>
<?php echo smarty_function_editor(array('type' => 'ckeditor','element' => 'NewsContent','toolbar' => 'Full'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>