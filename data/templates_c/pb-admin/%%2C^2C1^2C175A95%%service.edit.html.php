<?php /* Smarty version 2.6.18, created on 2020-08-14 16:23:47
         compiled from service.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'service.edit.html', 26, false),array('function', 'html_radios', 'service.edit.html', 38, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_service_center']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_service_center']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="service.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
		<li><a class="btn1"><span>Reply</span></a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="service.php" id="EditFrm" name="edit_frm">
  <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" />
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_title_n']; ?>
</th>
        <td class="paddingT15 wordSpacing5"><input type="text" name="data[service][title]" value="<?php echo $this->_tpl_vars['item']['title']; ?>
" /></td>
      </tr> 
      <tr>
        <th class="paddingT15"> Feedback category:</th>
        <td class="paddingT15 wordSpacing5"><?php echo $this->_tpl_vars['ServiceTypes'][$this->_tpl_vars['item']['type_id']]; ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> Submitter:</th>
        <td class="paddingT15 wordSpacing5"><?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['nick_name'])) ? $this->_run_mod_handler('default', true, $_tmp, 'anonymous') : smarty_modifier_default($_tmp, 'anonymous')); ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> Contact Email:</th>
        <td class="paddingT15 wordSpacing5"><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> Feedback content:</th>
        <td class="paddingT15 wordSpacing5"><?php echo $this->_tpl_vars['item']['content']; ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15">Whether to open:</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[service][status]",'options' => $this->_tpl_vars['Status'],'checked' => $this->_tpl_vars['item']['status'],'separator' => ' '), $this);?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> Reply content:</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[service][revert_content]" rows="5" cols="50"><?php echo $this->_tpl_vars['item']['revert_content']; ?>
</textarea></td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="Reply" />		</td>
      </tr>
    </table>
  </form>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>