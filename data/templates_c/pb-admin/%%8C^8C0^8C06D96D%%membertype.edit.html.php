<?php /* Smarty version 2.6.18, created on 2020-08-14 13:44:08
         compiled from membertype.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'membertype.edit.html', 32, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_member_center']; ?>
 &raquo; <?php echo $this->_tpl_vars['_user_center']; ?>
 &raquo; 分类</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_member']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="member.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
        <li><a href="membertype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
        <li><a class="btn1" href="membertype.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="membertype.php" id="EditFrm" name="edit_frm">
  <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" />
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> 名称：</th>
        <td class="paddingT15 wordSpacing5">          
		<input class="infoTableInput2" name="data[membertype][name]" value="<?php echo $this->_tpl_vars['item']['name']; ?>
" /></td>
      </tr>   
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_digest_n']; ?>
</th>
        <td class="paddingT15 wordSpacing5">
		<textarea style="width:550px;height:50px;" name="data[membertype][description]"><?php echo $this->_tpl_vars['item']['description']; ?>
</textarea>
		</td>
      </tr>
      <tr>
        <th class="paddingT15">默认会员组：</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[membertype][default_membergroup_id]" id="dataMembertypeGroupId">
        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['Membergroups'],'selected' => $this->_tpl_vars['item']['default_membergroup_id']), $this);?>

        </select>        </td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" />		</td>
      </tr>
    </table>
  </form>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>