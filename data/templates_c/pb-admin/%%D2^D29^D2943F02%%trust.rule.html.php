<?php /* Smarty version 2.6.18, created on 2020-08-25 10:40:20
         compiled from trust.rule.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_extension']; ?>
 &raquo; <?php echo $this->_tpl_vars['_trusts']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_extension']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="trust.php">认证类型</a></li>
		<li><a class="btn1" href="trust.php?do=rule"><span>诚信指数</span></a></li>
    </ul>
</div>
<div class="info">
  <form action="trust.php" method="post" id="EditFrm" name="edit_frm">
  <input type="hidden" name="do" value="setting">
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> 信用指数标准：</th>
        <td class="paddingT15 wordSpacing5">
		<label for="CreditType0"><input type="radio" name="data[setting1][credit_type]" id="CreditType0" value="0" onclick="$('#tbCreditCode').hide();" <?php if ($this->_tpl_vars['item']['CREDIT_TYPE'] == 0): ?>checked="checked"<?php endif; ?>>系统内置</label>
		<br />
		<label for="CreditType1"><input type="radio" name="data[setting1][credit_type]" id="CreditType1" value="1" onclick="$('#tbCreditCode').show();" <?php if ($this->_tpl_vars['item']['CREDIT_TYPE'] == 1): ?>checked="checked"<?php endif; ?>>第三方机构及代码</label>
		</td>
      </tr>
	  <tbody id="tbCreditCode" style="display:none;">
      <tr>
        <th class="paddingT15"></th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[setting1][credit_code]" style="width:650px;height:100px;"><?php echo $this->_tpl_vars['item']['CREDIT_CODE']; ?>
</textarea><span class="gray"><br>可用参数：%companyname%表示公司名称；%username%表示<?php echo $this->_tpl_vars['_cp_username']; ?>
；%userid%表示用户编号</span></td>
      </tr>
	  </tbody>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save_trust_rule" value="<?php echo $this->_tpl_vars['_save']; ?>
" />		</td>
      </tr>
    </table>
  </form>
</div>
<?php if ($this->_tpl_vars['item']['CREDIT_TYPE'] == 1): ?>
<script>
$("#tbCreditCode").show();
</script>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>