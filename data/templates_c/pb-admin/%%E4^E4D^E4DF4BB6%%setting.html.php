<?php /* Smarty version 2.6.18, created on 2020-08-14 15:22:55
         compiled from C:%5Cwamp64%5Cwww%5Cwtc_b2b_new%5Capi%5Cpayments%5Calipay%5Ctemplate%5Csetting.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'C:\\wamp64\\www\\wtc_b2b_new\\api\\payments\\alipay\\template\\setting.html', 32, false),array('function', 'html_radios', 'C:\\wamp64\\www\\wtc_b2b_new\\api\\payments\\alipay\\template\\setting.html', 42, false),array('function', 'editor', 'C:\\wamp64\\www\\wtc_b2b_new\\api\\payments\\alipay\\template\\setting.html', 76, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<style type="text/css">
<!--
body {background: #fcfdff}
-->
</style>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 系统工具 &raquo; 支付方式</p>
</div>
<div id="rightTop"> 
    <h3>支付方式</h3> 
    <ul class="subnav">
		<li><a href="payment.php">管理</a></li>
        <li><a class="btn1"><span>编辑</span></a></li>
    </ul>
</div>
<div class="info">
  <form action="payment.php" method="post" id="EditFrm" name="edit_frm">
  <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
">
  <input type="hidden" name="data[payment][name]" id="paymentName" value="<?php echo $_GET['entry']; ?>
">
    <table class="infoTable">
      <tr>
        <th colspan="2" class="paddingT15 wordSpacing5">基本信息</th>
      </tr>
      <tr>
        <th class="paddingT15"> 名称：</th>
        <td class="paddingT15 wordSpacing5"><?php echo $this->_tpl_vars['item']['name']; ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> 显示标题：</th>
        <td class="paddingT15 wordSpacing5">          
		<input class="infoTableInput2" name="data[payment][title]" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['title'])) ? $this->_run_mod_handler('default', true, $_tmp, @$this->_tpl_vars['item']['name']) : smarty_modifier_default($_tmp, @$this->_tpl_vars['item']['name'])); ?>
" /><label class="field_notice">显示的标题</label></td>
      </tr>
      
      <tr>
        <th class="paddingT15"> 简单描述：</th>
        <td class="paddingT15 wordSpacing5">
        <textarea style="width:550px;height:50px;" name="data[payment][description]"><?php echo $this->_tpl_vars['item']['description']; ?>
</textarea>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> 是否启用：</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[payment][available]",'options' => $this->_tpl_vars['AskAction'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['available'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)),'separator' => ""), $this);?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> 支持在线支付：</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[payment][if_online_support]",'options' => $this->_tpl_vars['AskAction'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['if_online_support'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)),'separator' => ""), $this);?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> 网关URL地址：</th>
        <td class="paddingT15 wordSpacing5">          
		<input name="data[config][gateway]" value="<?php echo $this->_tpl_vars['item']['gateway']; ?>
" /></td>
      </tr>
	    <tr>
        <th class="paddingT15"> 支付宝收款账号：</th>
        <td class="paddingT15 wordSpacing5">          
		<input name="data[config][account]" value="<?php echo $this->_tpl_vars['item']['account']; ?>
" /></td>
      </tr>
	    <tr>
        <th class="paddingT15"> 交易安全校验码：</th>
        <td class="paddingT15 wordSpacing5">          
		<input name="data[config][key]" value="<?php echo $this->_tpl_vars['item']['key']; ?>
" /></td>
      </tr>
	    <tr>
        <th class="paddingT15"> 合作者身份：</th>
        <td class="paddingT15 wordSpacing5">          
		<input name="data[config][partner_id]" value="<?php echo $this->_tpl_vars['item']['partner_id']; ?>
" /></td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="保存" />		</td>
      </tr>
    </table>
  </form>
</div>
<?php echo smarty_function_editor(array('type' => 'tiny_mce'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>