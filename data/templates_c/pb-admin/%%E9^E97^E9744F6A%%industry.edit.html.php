<?php /* Smarty version 2.6.18, created on 2020-08-17 10:05:08
         compiled from industry.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'industry.edit.html', 73, false),array('modifier', 'default', 'industry.edit.html', 73, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<link href="../images/jquery/colorpicker.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../scripts/jquery/colorpicker.js"></script>
<script>
jQuery(document).ready(function($) {
	$.fn.colorPicker.defaultColors = ['00FFFF', '000000', '999999', 'FF0000', 'FFFF00', '0000FF', 'FFFFFF', '00FF7F', '00FF00'];
	$('#color1').colorPicker();
})
</script>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_industry']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_industry']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="industry.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
        <li><a class="btn1" href="industry.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
        <li><a href="industrytype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
        <li><a href="industry.php?do=clear"><?php echo $this->_tpl_vars['_clearing']; ?>
</a></li>
        <li><a href="industry.php?do=refresh"><?php echo $this->_tpl_vars['_update_cache']; ?>
</a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="industry.php" id="EditFrm" name="edit_frm">
    <table class="infoTable">
    <?php if ($_GET['id']): ?>
      <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
" />
      <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>
" />
      <tr>
        <th class="paddingT15"> 编号：</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[industry][id]" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" type="text" /><label class="field_notice">一般不需要改动</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> 分类名称：</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[industry][name]" value="<?php echo $this->_tpl_vars['item']['name']; ?>
" type="text" /><label class="field_notice">一级分类名称不要有符号出现</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_digest_n']; ?>
</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[industry][description]"><?php echo $this->_tpl_vars['item']['description']; ?>
</textarea></td>
      </tr>
    <?php else: ?>
      <tr>
        <th class="paddingT15"> 分类名称：</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[names]" id="dataNames"></textarea><label class="field_notice">可以添加多个，一行代表一个分类,一级分类名称不要有符号</label></td>
      </tr>
    <?php endif; ?>
      <tr>
        <th class="paddingT15"> 上级分类：</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[industry][parent_id]" id="dataParentId">
        <option value="0">无</option>
        <?php echo $this->_tpl_vars['CacheItems']; ?>

        </select>
       </td>
      </tr>
      <tr>
        <th class="paddingT15"> 所属系列：</th>
        <td class="paddingT15 wordSpacing5">
         <select name="data[industry][industrytype_id]" id="dataTypeId">
			<option value="0">无</option>
            <?php $_from = $this->_tpl_vars['Types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key3'] => $this->_tpl_vars['item3']):
?>
            <option value="<?php echo $this->_tpl_vars['key3']; ?>
"><?php echo $this->_tpl_vars['item3']; ?>
</option>
            <?php endforeach; endif; unset($_from); ?>
        </select>   
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> 链接地址：</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[industry][url]" value="<?php echo $this->_tpl_vars['item']['url']; ?>
" type="text" /><label class="field_notice">为空，则系统默认地址</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> 是否显示：</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[industry][available]",'options' => $this->_tpl_vars['AskAction'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['available'])) ? $this->_run_mod_handler('default', true, $_tmp, 1) : smarty_modifier_default($_tmp, 1)),'separator' => ""), $this);?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> 样式：</th>
        <td class="paddingT15 wordSpacing5"><input name="data[style_color]" id="color1" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['style_color'])) ? $this->_run_mod_handler('default', true, $_tmp, "#000") : smarty_modifier_default($_tmp, "#000")); ?>
" /></td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_display_order']; ?>
</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[industry][display_order]" type="text" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['display_order'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
"></td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" />		</td>
      </tr>
    </table>
  </form>
</div>
<script>
jQuery(document).ready(function($) {
	$('#dataParentId').val("<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['parent_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
");
	$('#dataTypeId').val("<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['industrytype_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
");
})
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>