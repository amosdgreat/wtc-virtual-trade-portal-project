<?php /* Smarty version 2.6.18, created on 2020-08-25 10:38:26
         compiled from brand.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'brand.edit.html', 82, false),array('function', 'editor', 'brand.edit.html', 107, false),array('modifier', 'default', 'brand.edit.html', 82, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script src="../scripts/jquery/facebox.js" type="text/javascript"></script>
<link href="../images/facebox/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script>
jQuery(document).ready(function($) {
	$.facebox.settings.loadingImage = '<?php echo $this->_tpl_vars['SiteUrl']; ?>
images/facebox/loading.gif'; 
	$.facebox.settings.closeImage = '<?php echo $this->_tpl_vars['SiteUrl']; ?>
images/facebox/closelabel.gif'; 
	$('a[rel*=facebox]').facebox() 
})
</script>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_trade_management']; ?>
 &raquo; <?php echo $this->_tpl_vars['_product_center']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_product_center']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="product.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
		<li><a href="productsort.php">Types of</a></a></li>
        <li><a href="productcategory.php">Product Category</a></li>
		<li><a href="brand.php">Brand</a></li>
        <li><a class="btn1" href="brand.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
        <li><a href="brandtype.php">Brand<?php echo $this->_tpl_vars['_types']; ?>
</a></li>
    </ul>
</div>
<div class="info">
  <form action="brand.php" method="post" id="EditFrm" name="edit_frm" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" />
  <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>
" />
   <input type="hidden" name="company_name" value="<?php echo $this->_tpl_vars['item']['companyname']; ?>
" />
  <input type="hidden" name="username" value="<?php echo $this->_tpl_vars['item']['username']; ?>
" />
    <table class="infoTable">
      <tr>
        <th class="paddingT15">brand name</th>
        <td class="paddingT15 wordSpacing5">          
		<input class="infoTableInput2" name="data[name]" value="<?php echo $this->_tpl_vars['item']['name']; ?>
" />        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Alias:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[alias_name]" value="<?php echo $this->_tpl_vars['item']['alias_name']; ?>
" /><label class="field_notice">Optimization Name</label></td>
      </tr>
	  <?php if ($this->_tpl_vars['item']['id']): ?>
	  <tr>
        <th class="paddingT15"> Member name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="brand[[username]" value="<?php echo $this->_tpl_vars['item']['username']; ?>
"  readonly/></td>
      </tr>
	   <tr>
        <th class="paddingT15"> Company Name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="brand[company_name]" value="<?php echo $this->_tpl_vars['item']['companyname']; ?>
" readonly/></td>
      </tr>
	  <?php else: ?>
	  	  <tr>
        <th class="paddingT15"> Member name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="brand[[username]" value="<?php echo $this->_tpl_vars['item']['username']; ?>
"  /></td>
      </tr>
	   <tr>
        <th class="paddingT15"> Company Name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="brand[company_name]" value="<?php echo $this->_tpl_vars['item']['companyname']; ?>
" /></td>
      </tr>
	  <?php endif; ?>
      <tr>
        <th class="paddingT15"> Classification:</th>
        <td class="paddingT15 wordSpacing5">
		<select name="data[type_id]" id="Brandtypes">
		<option value="0">Top Classification</option>
        <?php echo $this->_tpl_vars['BrandtypeOptions']; ?>

		</select>        
		</td>
      </tr>
	  	<tr>
        <th class="paddingT15"> Views:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[hits]" value="<?php echo $this->_tpl_vars['item']['hits']; ?>
" /></td>
      </tr>
      <tr>
        <th class="paddingT15"> Description:</th>
        <td class="paddingT15 wordSpacing5">
        <textarea style="width:550px;height:50px;" name="data[description]"><?php echo $this->_tpl_vars['item']['description']; ?>
</textarea>
        </td>
      </tr>
      <tr>
        <th class="paddingT15">Whether<?php echo $this->_tpl_vars['_commend']; ?>
：</th>
        <td class="paddingT15 wordSpacing5">          
		<?php echo smarty_function_html_radios(array('name' => "data[if_commend]",'options' => $this->_tpl_vars['AskAction'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['if_commend'])) ? $this->_run_mod_handler('default', true, $_tmp, '0') : smarty_modifier_default($_tmp, '0')),'separator' => ' '), $this);?>
</td>
      </tr>
      <tr>
	   <tr>
        <th class="paddingT15"><?php echo $this->_tpl_vars['_picture']; ?>
</th>
        <td class="paddingT15 wordSpacing5">
        <input class="infoTableFile2" type="file" name="pic" id="pic" />
          <label class="field_notice">(Support format GIF,JPG,JPEG,PNG）</label>
          <?php if ($this->_tpl_vars['item']['picture']): ?>
          <br /><span><img src="<?php echo $this->_tpl_vars['item']['image']; ?>
" alt="<?php echo $this->_tpl_vars['item']['name']; ?>
"/></span>
          <?php endif; ?>
        </td>
      </tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" />
		</td>
      </tr>
    </table>
  </form>
</div>
<script>
jQuery(document).ready(function($) {
$("#Brandtypes").val("<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['type_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
");})
</script>
<?php echo smarty_function_editor(array('type' => 'tiny_mce'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>