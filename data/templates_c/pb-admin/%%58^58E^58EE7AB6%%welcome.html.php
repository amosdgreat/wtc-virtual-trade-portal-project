<?php /* Smarty version 2.6.18, created on 2020-08-14 13:35:54
         compiled from welcome.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'welcome.html', 40, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_tpl_vars['Charset']; ?>
" />
<title><?php echo $this->_tpl_vars['_welcome_to_cp']; ?>
</title>
<base target="_blank" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scripts/jquery.js"></script>
<script src="../scripts/jquery/facebox.js" type="text/javascript"></script>
<link href="../images/facebox/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script>
function setUpdateAlert(d)
{
$.ajax({
    url: 'home.php?do=set_update_alert&type='+d,
    type: 'GET',
    dataType: 'html',
    timeout: 1000,
    error: function(){
        alert('Error loading XML document');
    },
    success: function(data){
        $.facebox.close();
    }
});
}
</script>
<?php if ($this->_tpl_vars['hasNewVersion']): ?>
<script>
jQuery(document).ready(function($) {
	$.facebox.settings.loadingImage = '<?php echo $this->_tpl_vars['SiteUrl']; ?>
images/facebox/loading.gif'; 
	$.facebox('<?php echo $this->_tpl_vars['VersionInfo']; ?>
'+"<div align='center'><input type='button' id='NoUpdateAlert' value='<?php echo $this->_tpl_vars['_alert_no']; ?>
' onclick='setUpdateAlert(1);'>&nbsp;<input type='button' id='AlertAfterWeek' value='<?php echo $this->_tpl_vars['_alert_after_week']; ?>
' onclick='setUpdateAlert(7);'>&nbsp;<input type='button' id='AlertAfterWeek' value='<?php echo $this->_tpl_vars['_close']; ?>
' onclick='$.facebox.close();'></div>");
})
</script>
<?php endif; ?>
</head>
<body>
<div id="rightWelcome">
	<p>
		<strong>Hello, <?php echo $this->_tpl_vars['current_adminer']; ?>
,Welcome to <?php echo $this->_tpl_vars['site_name']; ?>
<?php echo $this->_tpl_vars['_control_pannel']; ?>
</strong>Today is:<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>
,Last Login Time:<?php echo $this->_tpl_vars['item']['last_login']; ?>
, Last login ip:<?php echo $this->_tpl_vars['item']['last_ip']; ?>

	</p>
</div>
<dl id="rightCon">
<dt>safety warning:</dt>
<dd>
    <ul>
       <li>Please keep it safe<span class="red">Website key</span>, Please go to "<a href="setting.php?do=auth" target="_self"><span class="bold"><?php echo $this->_tpl_vars['_setting_global']; ?>
 - safety verification</span></a>" Re-covered.</li>
       <li>If you want to continue to enable<span class="red">Update reminder</span>Function, please re "<a href="javascript:void(0);" onclick="setUpdateAlert(0);" target="_self"><span class="bold">Set reminder</span></a>" And refresh the page.</li>
       <li>
			<span><a href="member.php?do=search&member[status]=0" target="_self">Pending member:</a><em>(<?php echo $this->_tpl_vars['TotalAmounts']['member']; ?>
)</em></span>
			<span><a href="company.php?do=search&status=0" target="_self">Pending company library:</a><em>(<?php echo $this->_tpl_vars['TotalAmounts']['company']; ?>
)</em></span>
			<span><a href="offer.php?do=search&status=0" target="_self">Pending supply and demand information:</a><em>(<?php echo $this->_tpl_vars['TotalAmounts']['trade']; ?>
)</em></span>
			<span><a href="product.php?do=search&product[status]=0" target="_self">Pending product:</a><em>(<?php echo $this->_tpl_vars['TotalAmounts']['product']; ?>
)</em></span>
	   </li>
    </ul>
</dd>
<dt><?php echo $this->_tpl_vars['_administrator']; ?>
 leave a message</dt>
<dd>
    <ul>
      <form method="post" action="adminnote.php" id="EditFrm" name="edit_frm" target="_self">
        <li>Message content:<input type="text" name="data[adminnote][title]" value="" />&nbsp;<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" /></li>
      </form>
   </ul>
</dd>
<dt>system message</dt>
<dd>
    <table>
        <tr>
            <th>Server operating system:</th>
            <td><?php echo $this->_tpl_vars['item']['operatingsystem']; ?>
</td>
            <th>WEB Server:</th>
            <td><?php echo $this->_tpl_vars['item']['software']; ?>
</td>
        </tr>
        <tr>
            <th>PHP Version:</th>
            <td><?php echo $this->_tpl_vars['item']['PhpVersion']; ?>
</td>
            <th>MYSQL Version:</th>
            <td><?php echo $this->_tpl_vars['item']['MysqlVersion']; ?>
</td>
        </tr>
        <tr>
            <th>Program version and code:</th>
            <td><a href="<?php echo $this->_tpl_vars['SupportUrl']; ?>
" title="Check for updates" target="_blank">
                <!-- <span class="red"><?php echo $this->_tpl_vars['item']['PBVersion']; ?>
</span> -->
            </a></td>
            <th>Installation date:</th>
            <td><?php echo $this->_tpl_vars['item']['InstallDate']; ?>
</td>
        </tr>
        <tr>
            <th>used<?php echo $this->_tpl_vars['_database']; ?>
:</th>
            <td><?php echo $this->_tpl_vars['item']['DatabaseSize']; ?>
</td>
            <th>GD library support:</th>
            <td><?php echo $this->_tpl_vars['item']['GDSupports']; ?>
</td>
        </tr>
    </table>
</dd>
<dt>on <?php echo $this->_tpl_vars['_soft_name']; ?>
</dt>
<dd>
    <table>
        <tr>
            <th>Help forum:</th>
            <td><a href="https://www.wtcdigitalservices.com">http://www.wtcdigitalservices.com/</a></td>
        </tr>
    </table>
</dd>
</dl>
</body>
</html>