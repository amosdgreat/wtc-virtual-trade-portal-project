<?php /* Smarty version 2.6.18, created on 2020-08-20 14:18:22
         compiled from productcategory.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'productcategory.edit.html', 57, false),array('modifier', 'default', 'productcategory.edit.html', 73, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
 <p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_trade_management']; ?>
 &raquo; <?php echo $this->_tpl_vars['_product_center']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_product_center']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="product.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
		<li><a href="productsort.php">Types of</a></a></li>
        <li><a href="productcategory.php">Product Category</a></li>
        <li><a class="btn1" href="productcategory.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
		<li><a href="brand.php">Brand</a></a></li>
		<li><a href="brandtype.php">Brand category</a></a></li>
		<li><a href="price.php">Offer</a></a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="productcategory.php" id="EditFrm" name="edit_frm">
  <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['item']['id']; ?>
" />
  <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>
" />
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> Add method</th>
        <td class="paddingT15 wordSpacing5">
		<select name="data[method]" id="DataMethod">
			<option value="1">Add one by one</option>
			<option value="2">Copy from industry classification</option>
        </select></td>
      </tr>
	  <tbody id="DataMethod1">
	    <?php if ($_GET['id']): ?>
      <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
" />
      <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>
" />
      <tr>
        <th class="paddingT15"> Category Name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[productcategory][name]" value="<?php echo $this->_tpl_vars['item']['name']; ?>
" type="text" /><label class="field_notice">Do not have symbols in the name of the first level classification</label>        </td>
      </tr>
    <?php else: ?>
	      <tr>
        <th class="paddingT15"> Category Name:</th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[productcategory][name]" id="dataNames"></textarea></td>
      </tr>
    <?php endif; ?>      
      <tr>
        <th class="paddingT15"> Sub-headings:</th>
        <td class="paddingT15 wordSpacing5">
        <select name="data[productcategory][parent_id]" id="ProductcategorytypeParentId">
        <option value="0">Top classification</option>
        <?php echo $this->_tpl_vars['ProductcategoryOptions']; ?>

        </select>
        </td>
      </tr>
	  </tbody>
	  <tbody id="DataMethod2" style="display:none;">
      <tr>
        <th class="paddingT15"> Whether to cover:</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[coverage]",'options' => $this->_tpl_vars['AskAction'],'checked' => 1,'separator' => ""), $this);?>
<label class="field_notice">If you choose not to cover, it may cause classification disorder</label></td>
      </tr>      
      <tr>
        <th class="paddingT15"> Whether to empty:</th>
        <td class="paddingT15 wordSpacing5"><?php echo smarty_function_html_radios(array('name' => "data[truncate]",'options' => $this->_tpl_vars['AskAction'],'checked' => 0,'separator' => ""), $this);?>
</td>
      </tr>      
	  </tbody>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save_and_pub']; ?>
" />		</td>
      </tr>
    </table>
  </form>
</div>
<script>
var parent_id = "<?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['parent_id'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
";
jQuery(document).ready(function($) {
	$("#ProductcategorytypeParentId option[value='"+parent_id+"']").attr("selected","selected")
	$("#DataMethod").change( function() { 
		$("#DataMethod1, #DataMethod2").toggle();
	} 
	);
})
</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>