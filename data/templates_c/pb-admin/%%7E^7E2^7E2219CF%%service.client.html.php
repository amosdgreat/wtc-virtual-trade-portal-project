<?php /* Smarty version 2.6.18, created on 2020-08-20 11:41:45
         compiled from service.client.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'editor', 'service.client.html', 29, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_service_center']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_service_center']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="service.php">Customer submission</a></li>
		<li><a class="btn1" href="service.php?do=client"><span>Terms of Service: </span></a></li>
    </ul>
</div>
<div class="info">
  <form method="post" action="service.php">
    <table class="infoTable">
      <tr>
        <th class="paddingT15"><label for="price_format"> Terms of Service: </label></th>
        <td class="paddingT15 wordSpacing5"><textarea name="data[setting1][service_client]" id="SERVICE_CLIENT" style="width:650px;height:300px;" ><?php echo $this->_tpl_vars['item']['SERVICE_CLIENT']; ?>
</textarea>
		</td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20"><input class="formbtn" type="submit" name="save_client" value="<?php echo $this->_tpl_vars['_submit']; ?>
" />
          <input class="formbtn" type="reset" name="reset" value="<?php echo $this->_tpl_vars['_reset']; ?>
" />
        </td>
      </tr>
    </table>
  </form>
</div>
<?php echo smarty_function_editor(array('type' => 'ckeditor','element' => 'SERVICE_CLIENT','toolbar' => 'Full'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>