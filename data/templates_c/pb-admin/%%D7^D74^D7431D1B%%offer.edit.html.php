<?php /* Smarty version 2.6.18, created on 2020-08-20 11:50:03
         compiled from offer.edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'fetch', 'offer.edit.html', 2, false),array('function', 'html_radios', 'offer.edit.html', 56, false),array('function', 'editor', 'offer.edit.html', 130, false),array('modifier', 'default', 'offer.edit.html', 56, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo smarty_function_fetch(array('file' => "../scripts/date.js"), $this);?>

<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_trade_management']; ?>
 &raquo; <?php echo $this->_tpl_vars['_offer']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_offer']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="offer.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
        <li><a class="btn1" href="offer.php?do=edit"><span><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</span></a></li>
        <li><a href="offer.php?do=setting"><?php echo $this->_tpl_vars['_setting']; ?>
</a></li>
        <li><a href="offertype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
    </ul>
</div>
<div class="info">
  <form method="post" enctype="multipart/form-data" id="EditFrm" action="offer.php" name="edit_frm">
  	<input type="hidden" name="page" value="<?php echo $_GET['page']; ?>
" />
  	<?php if ($_GET['id']): ?>
  	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>
" />
  	<input type="hidden" name="company_name" value="<?php echo $this->_tpl_vars['item']['companyname']; ?>
" />
  	<input type="hidden" name="username" value="<?php echo $this->_tpl_vars['item']['username']; ?>
" />
  	<input type="hidden" name="oldtag" value="<?php echo $this->_tpl_vars['item']['tag']; ?>
" />
    <input type="hidden" name="oldimg" value="<?php echo $this->_tpl_vars['item']['picture']; ?>
" />
  	<?php endif; ?>
    <table class="infoTable">
      <tr>
        <th class="paddingT15"> Member name:</th>
        <td class="paddingT15 wordSpacing5">          
		<input class="infoTableInput2" name="data[username]" value="<?php echo $this->_tpl_vars['item']['username']; ?>
" /><label class="field_notice">Member name</label>
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Company Name:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[company_name]" id="dataCompanyName" value="<?php echo $this->_tpl_vars['item']['companyname']; ?>
"/></td>
      </tr>
      <tr>
        <th class="paddingT15"> Information title:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[trade][title]" value="<?php echo $this->_tpl_vars['item']['title']; ?>
" /><label class="field_notice">Supply and demand content title</label></td>
      </tr>
      <tr>
        <th class="paddingT15"> Label:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[tag]" type="text" id="tag" value="<?php echo $this->_tpl_vars['item']['tag']; ?>
" /><label class="field_notice"><?php echo $this->_tpl_vars['_multi_seperate_by_quot_or_space']; ?>
</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Release Time:</th>
        <td class="paddingT15 wordSpacing5"><?php echo $this->_tpl_vars['item']['pubdate']; ?>
</td>
      </tr>
      <tr>
        <th class="paddingT15"> End Time:</th>
        <td class="paddingT15 wordSpacing5">
        	<input type="text" name="expiretime" id="date1" value="<?php echo $this->_tpl_vars['item']['expdate']; ?>
" /><span class="btn_calendar" id="calendar-date1"></span>
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Supply and demand type:</th>
        <td class="paddingT15 wordSpacing5"><p><?php echo smarty_function_html_radios(array('name' => "data[trade][type_id]",'options' => $this->_tpl_vars['TradeTypes'],'checked' => ((is_array($_tmp=@$this->_tpl_vars['item']['type_id'])) ? $this->_run_mod_handler('default', true, $_tmp, '1') : smarty_modifier_default($_tmp, '1')),'separator' => ' '), $this);?>
</p></td>
      </tr>
      <tr>
        <th class="paddingT15"> Check the required points:</th>
        <td class="paddingT15 wordSpacing5"><input class="infoTableInput2" name="data[trade][require_point]" type="text" id="require_point" value="<?php echo $this->_tpl_vars['item']['require_point']; ?>
" /><label class="field_notice">(The default is 0, no points are required)</label>        </td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_in_industry']; ?>
</th>
        <td class="paddingT15 wordSpacing5">
        <span id="dataIndustry">
					<select name="data[trade][industry_id1]" id="dataTradeIndustryId1" class="level_1" style="width:120px;" ></select>
					<select name="data[trade][industry_id2]" id="dataTradeIndustryId2" class="level_2" style="width:120px;"></select>
					<select name="data[trade][industry_id3]" id="dataTradeIndustryId3" class="level_3" style="width:120px;"></select>
		</span>
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> <?php echo $this->_tpl_vars['_in_area']; ?>
</th>
        <td class="paddingT15 wordSpacing5">
			<span id="dataArea">
						<select name="data[trade][area_id1]" id="dataTradeAreaId1" class="level_1" style="width:120px;" ></select>
						<select name="data[trade][area_id2]" id="dataTradeAreaId2" class="level_2" style="width:120px;"></select>
						<select name="data[trade][area_id3]" id="dataTradeAreaId3" class="level_3" style="width:120px;"></select>
			</span>        
        </td>
      </tr>
      <tr>
        <th class="paddingT15"> Detailed description:</th>
        <td class="paddingT15 wordSpacing5"><textarea style="width:650px;height:300px;" name="data[trade][content]" id="dataTradeContent"><?php echo $this->_tpl_vars['item']['content']; ?>
</textarea>
		</td>

      </tr>
      <tr>
        <th class="paddingT15">Image:</th>
        <td class="paddingT15 wordSpacing5">
        <input class="infoTableFile2" type="file" name="pic" id="pic" />
          <label class="field_notice">(Support format GIF,JPG,JPEG,PNG）</label>
          <?php if ($this->_tpl_vars['item']['picture']): ?>
          <br /><span><img src="<?php echo $this->_tpl_vars['item']['image']; ?>
" alt="<?php echo $this->_tpl_vars['item']['name']; ?>
"/></span>
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <th></th>
        <td class="ptb20">
			<input class="formbtn" type="submit" name="save" value="<?php echo $this->_tpl_vars['_save']; ?>
" />
			<input class="formbtn" type="submit" name="pass" value="examination passed" />
			<input class="formbtn" type="submit" name="forbid" value="Invalid review" />
			<input class="formbtn" type="submit" name="del" value="<?php echo $this->_tpl_vars['_delete']; ?>
" />
		</td>

      </tr>
    </table>
  </form>
</div>
<script>
var cache_path = "../";
var app_language = '<?php echo $this->_tpl_vars['AppLanguage']; ?>
';
var area_id1 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['area_id1'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
var area_id2 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['area_id2'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
var area_id3 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['area_id3'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
var industry_id1 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['industry_id1'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
var industry_id2 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['industry_id2'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
var industry_id3 = <?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['industry_id3'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
 ;
Calendar.setup({
	trigger    : "calendar-date1",
	animation  : false,
	inputField : "date1",
	onSelect   : function() { this.hide() }
});
</script>
<script src="../scripts/multi_select.js" charset="<?php echo $this->_tpl_vars['Charset']; ?>
"></script>
<script src="../scripts/script_area.js"></script>
<script src="../scripts/script_industry.js"></script>
<?php echo smarty_function_editor(array('type' => 'tiny_mce'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>