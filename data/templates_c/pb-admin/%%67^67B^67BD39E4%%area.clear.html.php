<?php /* Smarty version 2.6.18, created on 2020-08-14 16:09:54
         compiled from area.clear.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="currentPosition">
	<p><?php echo $this->_tpl_vars['_your_current_position']; ?>
 <?php echo $this->_tpl_vars['_setting_global']; ?>
 &raquo; <?php echo $this->_tpl_vars['_area']; ?>
</p>
</div>
<div id="rightTop"> 
    <h3><?php echo $this->_tpl_vars['_area']; ?>
</h3> 
    <ul class="subnav">
		<li><a href="area.php"><?php echo $this->_tpl_vars['_management']; ?>
</a></li>
		<li><a href="area.php?do=edit"><?php echo $this->_tpl_vars['_add_or_edit']; ?>
</a></li>
        <li><a href="areatype.php"><?php echo $this->_tpl_vars['_sorts']; ?>
</a></li>
        <li><a class="btn1" href="area.php?do=clear"><span><?php echo $this->_tpl_vars['_clearing']; ?>
</span></a></li>
        <li><a href="area.php?do=refresh"><?php echo $this->_tpl_vars['_update_cache']; ?>
</a></li>
    </ul>
</div>
<div class="mrightTop"> 
    <div class="fontl"> 
        <form name="edit_frm" action="area.php" id="EditFrm" method="post">
        <input type="hidden" name="do" value="clear" />
             <div class="left"> 
               <label for="Level1"><input type="checkbox" name="data[level][]" id="Level1" value="1"/>First-level area</label>
               <label for="Level2"><input type="checkbox" name="data[level][]" id="Level2" value="2" checked="checked"  />Secondary area</label>
               <label for="Level3"><input type="checkbox" name="data[level][]" id="Level3" value="3" checked="checked" />Tertiary area</label>
                <input type="submit" name="save" id="Save" class="formbtn" value="Clean up now" /> 
            </div> 
        </form> 
    </div> 
    <div class="fontr"></div> 
</div> 
<div class="tdare"></div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>