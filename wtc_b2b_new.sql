-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Oct 05, 2020 at 08:06 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wtc_b2b_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `pb_adminfields`
--

DROP TABLE IF EXISTS `pb_adminfields`;
CREATE TABLE IF NOT EXISTS `pb_adminfields` (
  `member_id` int(10) NOT NULL,
  `depart_id` tinyint(1) NOT NULL DEFAULT 0,
  `first_name` varchar(25) NOT NULL DEFAULT '',
  `last_name` varchar(25) NOT NULL DEFAULT '',
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `last_login` int(10) NOT NULL DEFAULT 0,
  `last_ip` varchar(25) NOT NULL DEFAULT '',
  `expired` int(10) NOT NULL DEFAULT 0,
  `permissions` text NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_adminfields`
--

INSERT INTO `pb_adminfields` (`member_id`, `depart_id`, `first_name`, `last_name`, `level`, `last_login`, `last_ip`, `expired`, `permissions`, `created`, `modified`) VALUES
(1, 0, '', 'administrator', 0, 1600269753, 'unknown', 0, '', 1596019810, 1596019810);

-- --------------------------------------------------------

--
-- Table structure for table `pb_adminmodules`
--

DROP TABLE IF EXISTS `pb_adminmodules`;
CREATE TABLE IF NOT EXISTS `pb_adminmodules` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_adminnotes`
--

DROP TABLE IF EXISTS `pb_adminnotes`;
CREATE TABLE IF NOT EXISTS `pb_adminnotes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_adminnotes`
--

INSERT INTO `pb_adminnotes` (`id`, `member_id`, `title`, `content`, `created`, `modified`) VALUES
(1, 1, 'testing message', NULL, 1597418524, 1597418524);

-- --------------------------------------------------------

--
-- Table structure for table `pb_adminprivileges`
--

DROP TABLE IF EXISTS `pb_adminprivileges`;
CREATE TABLE IF NOT EXISTS `pb_adminprivileges` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `adminmodule_id` int(5) NOT NULL DEFAULT 0,
  `name` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_adminroles`
--

DROP TABLE IF EXISTS `pb_adminroles`;
CREATE TABLE IF NOT EXISTS `pb_adminroles` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_adses`
--

DROP TABLE IF EXISTS `pb_adses`;
CREATE TABLE IF NOT EXISTS `pb_adses` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `adzone_id` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `is_image` tinyint(1) NOT NULL DEFAULT 1,
  `source_name` varchar(100) NOT NULL DEFAULT '',
  `source_type` varchar(100) NOT NULL DEFAULT '',
  `source_url` varchar(255) NOT NULL DEFAULT '',
  `target_url` varchar(255) NOT NULL DEFAULT '',
  `width` smallint(6) NOT NULL DEFAULT 0,
  `height` smallint(6) NOT NULL DEFAULT 0,
  `alt_words` varchar(25) NOT NULL DEFAULT '',
  `start_date` int(10) NOT NULL DEFAULT 0,
  `end_date` int(10) NOT NULL DEFAULT 0,
  `priority` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` smallint(6) NOT NULL DEFAULT 1,
  `target` enum('_parent','_self','_blank') NOT NULL DEFAULT '_blank',
  `seq` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `picture_replace` varchar(255) NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_adses`
--

INSERT INTO `pb_adses` (`id`, `adzone_id`, `title`, `description`, `is_image`, `source_name`, `source_type`, `source_url`, `target_url`, `width`, `height`, `alt_words`, `start_date`, `end_date`, `priority`, `clicked`, `target`, `seq`, `state`, `status`, `picture_replace`, `created`, `modified`) VALUES
(1, 1, 'GIF', 'Sample Ad1', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(2, 1, 'JPG', 'Sample Ad2', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(3, 1, 'PNG', 'Sample Ad3', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(4, 1, 'Sample Ad4', '', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(5, 1, 'Sample Ad5', 'PNG', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(6, 1, 'Sample Ad6', '', 1, '', 'image/gif', 'attachment/sample/default/blue.gif', '', 152, 52, '', 0, 0, 0, 1, '_blank', 0, 1, 1, '', 1596019821, 0),
(7, 2, 'E-commerce platform for next-generation industry', '', 1, '', 'image/pjpeg', 'attachment/sample/example_958.jpg', 'http://www.phpb2b.com', 958, 62, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'attachment/sample/example_958.jpg', 1596019821, 0),
(8, 3, 'Advertising Opportunities', '', 1, '', 'image/pjpeg', 'attachment/sample/breathe-offer1.jpg', '', 477, 170, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0),
(9, 3, 'Welcome Register', 'Registration', 1, '', 'image/pjpeg', 'attachment/sample/breathe-offer2.jpg', '', 477, 170, '', 1596019821, 1596106221, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0),
(15, 4, 'Home Big Picture', '', 1, '', 'image/pjpeg', 'attachment/sample/breathe-product1.jpg', '', 500, 200, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0),
(16, 5, 'Home Big Picture', '', 1, '', 'image/pjpeg', 'attachment/sample/breathe-index1.jpg', '', 500, 200, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0),
(18, 4, 'Sign up business opportunities', '', 1, '', 'image/pjpeg', 'attachment/sample/breathe-product2.jpg', '', 570, 170, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0),
(17, 5, 'Member Advertising', 'asdf', 1, '', 'image/pjpeg', 'attachment/sample/breathe-index2.jpg', '', 500, 200, '', 1596019821, 0, 0, 1, '_blank', 0, 1, 1, 'images/nopicture_small.gif', 1596019821, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_adzones`
--

DROP TABLE IF EXISTS `pb_adzones`;
CREATE TABLE IF NOT EXISTS `pb_adzones` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `membergroup_ids` varchar(50) NOT NULL DEFAULT '',
  `what` varchar(10) NOT NULL DEFAULT '',
  `style` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `additional_adwords` text DEFAULT NULL,
  `price` float(9,2) NOT NULL DEFAULT 0.00,
  `file_name` varchar(100) NOT NULL DEFAULT '',
  `width` smallint(6) NOT NULL DEFAULT 0,
  `height` smallint(6) NOT NULL DEFAULT 0,
  `wrap` smallint(6) NOT NULL DEFAULT 0,
  `max_ad` smallint(6) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_adzones`
--

INSERT INTO `pb_adzones` (`id`, `membergroup_ids`, `what`, `style`, `name`, `description`, `additional_adwords`, `price`, `file_name`, `width`, `height`, `wrap`, `max_ad`, `created`, `modified`) VALUES
(1, '8,9', '1', 0, 'Home at the top of a small image ads', '6 picture line, Home Show', '', 1.00, 'index.php', 760, 52, 6, 12, 0, 1261133418),
(2, '0', '2', 0, 'Home banner ads', 'Free open-source, supports multiple languages, PHPB2B a new generation of e-commerce application platform', '<script type=\"text/javascript\"><!--\r\ngoogle_ad_client = \"pub-3644891845756714\";\r\n/* This is a ad sample, change it in admin panel */\r\ngoogle_ad_slot = \"2369135211\";\r\ngoogle_ad_width = 728;\r\ngoogle_ad_height = 90;\r\n//--></script>\r\n<script type=\"text/javascript\"src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\"></script>\r\n', 3000.00, 'index.php', 958, 62, 0, 0, 1265678633, 1273158424),
(3, '', '1', 1, 'Home Advertising Opportunities', 'Look for business opportunities home', '', 1000.00, '', 380, 270, 0, 0, 1271760815, 1273157527),
(4, '', '1', 1, 'Product Home Advertising', '', '', 0.00, '', 570, 170, 0, 0, 1272349339, 1273157343),
(5, '', '1', 1, 'Home Big Picture Advertising', 'Home Publicity', '', 0.00, '', 473, 170, 0, 0, 1273156673, 1273157006);

-- --------------------------------------------------------

--
-- Table structure for table `pb_albums`
--

DROP TABLE IF EXISTS `pb_albums`;
CREATE TABLE IF NOT EXISTS `pb_albums` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT 0,
  `attachment_id` int(10) NOT NULL DEFAULT 0,
  `type_id` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_albumtypes`
--

DROP TABLE IF EXISTS `pb_albumtypes`;
CREATE TABLE IF NOT EXISTS `pb_albumtypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_albumtypes`
--

INSERT INTO `pb_albumtypes` (`id`, `name`, `display_order`) VALUES
(1, 'Company album', 0),
(2, 'Product Image', 0),
(3, 'Advertising', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_announcements`
--

DROP TABLE IF EXISTS `pb_announcements`;
CREATE TABLE IF NOT EXISTS `pb_announcements` (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `announcetype_id` smallint(3) NOT NULL DEFAULT 0,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text DEFAULT NULL,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `display_expiration` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_announcements`
--

INSERT INTO `pb_announcements` (`id`, `announcetype_id`, `subject`, `message`, `display_order`, `display_expiration`, `created`, `modified`) VALUES
(13, 0, 'How to position your product closer to the top', NULL, 0, 0, 1596019822, 0),
(14, 0, 'Web Member Services', 'By Gold member, you can not just their own business, product and other commercial information published on the Internet, can also take the initiative to establish contact with many buyers.', 0, 0, 1596019822, 0),
(11, 0, '2009, 500 Chinese enterprises released', NULL, 0, 0, 1596019822, 0),
(12, 0, 'Open gold medal Wang Pu', NULL, 0, 0, 1596019822, 0),
(4, 1, 'Wonderful Expo, concerned about environmental protection ', NULL, 0, 0, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_announcementtypes`
--

DROP TABLE IF EXISTS `pb_announcementtypes`;
CREATE TABLE IF NOT EXISTS `pb_announcementtypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_announcementtypes`
--

INSERT INTO `pb_announcementtypes` (`id`, `name`) VALUES
(1, 'Site Notice'),
(2, 'Advertising time');

-- --------------------------------------------------------

--
-- Table structure for table `pb_areas`
--

DROP TABLE IF EXISTS `pb_areas`;
CREATE TABLE IF NOT EXISTS `pb_areas` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `attachment_id` int(10) NOT NULL DEFAULT 0,
  `areatype_id` smallint(3) NOT NULL DEFAULT 0,
  `child_ids` text DEFAULT NULL,
  `top_parentid` smallint(6) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `alias_name` varchar(255) NOT NULL DEFAULT '',
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_areas`
--

INSERT INTO `pb_areas` (`id`, `attachment_id`, `areatype_id`, `child_ids`, `top_parentid`, `level`, `name`, `url`, `alias_name`, `highlight`, `parent_id`, `display_order`, `description`, `available`, `created`, `modified`) VALUES
(1, 0, 0, NULL, 0, 1, 'Australia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(2, 0, 0, NULL, 0, 1, 'Indonesia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(3, 0, 0, NULL, 0, 1, 'New Zealand', '', '', 0, 0, 0, NULL, 1, 0, 0),
(4, 0, 0, NULL, 0, 1, 'South Korea', '', '', 0, 0, 0, NULL, 1, 0, 0),
(5, 0, 0, NULL, 0, 1, 'China', '', '', 0, 0, 0, NULL, 1, 0, 0),
(6, 0, 0, NULL, 0, 1, 'Iran', '', '', 0, 0, 0, NULL, 1, 0, 0),
(7, 0, 0, NULL, 0, 1, 'Pakistan', '', '', 0, 0, 0, NULL, 1, 0, 0),
(8, 0, 0, NULL, 0, 1, 'Taiwan', '', '', 0, 0, 0, NULL, 1, 0, 0),
(9, 0, 0, NULL, 0, 1, 'Hong Kong', '', '', 0, 0, 0, NULL, 1, 0, 0),
(10, 0, 0, NULL, 0, 1, 'Japan', '', '', 0, 0, 0, NULL, 1, 0, 0),
(11, 0, 0, NULL, 0, 1, 'Philippines', '', '', 0, 0, 0, NULL, 1, 0, 0),
(12, 0, 0, NULL, 0, 1, 'Thailand', '', '', 0, 0, 0, NULL, 1, 0, 0),
(13, 0, 0, NULL, 0, 1, 'India', '', '', 0, 0, 0, NULL, 1, 0, 0),
(14, 0, 0, NULL, 0, 1, 'Malaysia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(15, 0, 0, NULL, 0, 1, 'Singapore', '', '', 0, 0, 0, NULL, 1, 0, 0),
(16, 0, 0, NULL, 0, 1, 'Vietnam', '', '', 0, 0, 0, NULL, 1, 0, 0),
(17, 0, 0, NULL, 0, 1, 'Argentina', '', '', 0, 0, 0, NULL, 1, 0, 0),
(18, 0, 0, NULL, 0, 1, 'Canada', '', '', 0, 0, 0, NULL, 1, 0, 0),
(19, 0, 0, NULL, 0, 1, 'Colombia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(20, 0, 0, NULL, 0, 1, 'Peru', '', '', 0, 0, 0, NULL, 1, 0, 0),
(21, 0, 0, NULL, 0, 1, 'Brazil', '', '', 0, 0, 0, NULL, 1, 0, 0),
(22, 0, 0, NULL, 0, 1, 'Chile', '', '', 0, 0, 0, NULL, 1, 0, 0),
(23, 0, 0, NULL, 0, 1, 'Mexico', '', '', 0, 0, 0, NULL, 1, 0, 0),
(24, 0, 0, NULL, 0, 1, 'United States', '', '', 0, 0, 0, NULL, 1, 0, 0),
(25, 0, 0, NULL, 0, 1, 'Belgium', '', '', 0, 0, 0, NULL, 1, 0, 0),
(26, 0, 0, NULL, 0, 1, 'Germany', '', '', 0, 0, 0, NULL, 1, 0, 0),
(27, 0, 0, NULL, 0, 1, 'Poland', '', '', 0, 0, 0, NULL, 1, 0, 0),
(28, 0, 0, NULL, 0, 1, 'Sweden', '', '', 0, 0, 0, NULL, 1, 0, 0),
(29, 0, 0, NULL, 0, 1, 'Bulgaria', '', '', 0, 0, 0, NULL, 1, 0, 0),
(30, 0, 0, NULL, 0, 1, 'Greece', '', '', 0, 0, 0, NULL, 1, 0, 0),
(31, 0, 0, NULL, 0, 1, 'Portugal', '', '', 0, 0, 0, NULL, 1, 0, 0),
(32, 0, 0, NULL, 0, 1, 'Switzerland', '', '', 0, 0, 0, NULL, 1, 0, 0),
(33, 0, 0, NULL, 0, 1, 'Czech Republic', '', '', 0, 0, 0, NULL, 1, 0, 0),
(34, 0, 0, NULL, 0, 1, 'Iceland', '', '', 0, 0, 0, NULL, 1, 0, 0),
(35, 0, 0, NULL, 0, 1, 'Romania', '', '', 0, 0, 0, NULL, 1, 0, 0),
(36, 0, 0, NULL, 0, 1, 'Turkey', '', '', 0, 0, 0, NULL, 1, 0, 0),
(37, 0, 0, NULL, 0, 1, 'Denmark', '', '', 0, 0, 0, NULL, 1, 0, 0),
(38, 0, 0, NULL, 0, 1, 'Italy', '', '', 0, 0, 0, NULL, 1, 0, 0),
(39, 0, 0, NULL, 0, 1, 'Russia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(40, 0, 0, NULL, 0, 1, 'Ukraine', '', '', 0, 0, 0, NULL, 1, 0, 0),
(41, 0, 0, NULL, 0, 1, 'France', '', '', 0, 0, 0, NULL, 1, 0, 0),
(42, 0, 0, NULL, 0, 1, 'Netherlands', '', '', 0, 0, 0, NULL, 1, 0, 0),
(43, 0, 0, NULL, 0, 1, 'Spain', '', '', 0, 0, 0, NULL, 1, 0, 0),
(44, 0, 0, NULL, 0, 1, 'United Kingdom', '', '', 0, 0, 0, NULL, 1, 0, 0),
(45, 0, 0, NULL, 0, 1, 'Israel', '', '', 0, 0, 0, NULL, 1, 0, 0),
(46, 0, 0, NULL, 0, 1, 'Syria', '', '', 0, 0, 0, NULL, 1, 0, 0),
(47, 0, 0, NULL, 0, 1, 'United Arab ', '', '', 0, 0, 0, NULL, 1, 0, 0),
(48, 0, 0, NULL, 0, 1, 'Emirates', '', '', 0, 0, 0, NULL, 1, 0, 0),
(49, 0, 0, NULL, 0, 1, 'Saudi Arabia', '', '', 0, 0, 0, NULL, 1, 0, 0),
(50, 0, 0, NULL, 0, 1, 'Egypt', '', '', 0, 0, 0, NULL, 1, 0, 0),
(51, 0, 0, NULL, 0, 1, 'South Africa', '', '', 0, 0, 0, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_areatypes`
--

DROP TABLE IF EXISTS `pb_areatypes`;
CREATE TABLE IF NOT EXISTS `pb_areatypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_areatypes`
--

INSERT INTO `pb_areatypes` (`id`, `name`) VALUES
(1, 'North China'),
(2, 'Northeast'),
(3, 'East China'),
(4, 'Central China'),
(5, 'Southwest'),
(6, 'Northwest'),
(7, 'South China'),
(8, 'SAR');

-- --------------------------------------------------------

--
-- Table structure for table `pb_attachments`
--

DROP TABLE IF EXISTS `pb_attachments`;
CREATE TABLE IF NOT EXISTS `pb_attachments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `attachmenttype_id` smallint(3) NOT NULL DEFAULT 0,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `file_name` char(100) NOT NULL DEFAULT '',
  `attachment` char(255) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `file_type` char(50) NOT NULL DEFAULT '0',
  `file_size` mediumint(8) NOT NULL DEFAULT 0,
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `remote` varchar(100) NOT NULL DEFAULT '',
  `is_image` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_attachmenttypes`
--

DROP TABLE IF EXISTS `pb_attachmenttypes`;
CREATE TABLE IF NOT EXISTS `pb_attachmenttypes` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_banned`
--

DROP TABLE IF EXISTS `pb_banned`;
CREATE TABLE IF NOT EXISTS `pb_banned` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ip1` char(3) NOT NULL DEFAULT '',
  `ip2` char(3) NOT NULL DEFAULT '',
  `ip3` char(3) NOT NULL DEFAULT '',
  `ip4` char(3) NOT NULL DEFAULT '',
  `expiration` int(10) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip1` (`ip1`,`ip2`,`ip3`,`ip4`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_brands`
--

DROP TABLE IF EXISTS `pb_brands`;
CREATE TABLE IF NOT EXISTS `pb_brands` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `company_id` int(10) NOT NULL DEFAULT -1,
  `type_id` smallint(3) NOT NULL DEFAULT 0,
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias_name` varchar(100) NOT NULL DEFAULT '',
  `picture` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `hits` smallint(6) NOT NULL DEFAULT 0,
  `ranks` smallint(3) NOT NULL DEFAULT 0,
  `letter` varchar(2) NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_brands`
--

INSERT INTO `pb_brands` (`id`, `member_id`, `company_id`, `type_id`, `if_commend`, `name`, `alias_name`, `picture`, `description`, `hits`, `ranks`, `letter`, `created`, `modified`) VALUES
(1, -1, 0, 1, 1, 'palm', 'palm', 'sample/brand/1.jpg', '', 1, 0, 'p', 1596019822, 0),
(2, -1, 0, 1, 1, 'LG', 'LG', 'sample/brand/2.jpg', '', 1, 0, 'l', 1596019822, 0),
(3, -1, 0, 1, 0, 'Motorola', 'motolola', 'sample/brand/3.jpg', '', 1, 0, 'm', 1596019822, 0),
(4, -1, 1, 1, 1, 'Nokia', 'nokia', 'sample/brand/4.jpg', '', 1, 0, 'n', 1596019822, 0),
(5, 1, 1, 4, 1, 'Philips', 'philips', 'sample/brand/5.jpg', '', 0, 0, 'f', 1596019822, 0),
(26, -1, 1, 4, 0, 'Samsung', 'samsung', 'sample/brand/6.jpg', '', 0, 0, 's', 1596019822, 0),
(27, -1, -1, 4, 0, 'Matsushita', 'panasonic', 'sample/brand/7.jpg', '', 0, 0, 's', 1596019822, 0),
(28, -1, 0, 4, 0, 'Sony Ericsson', 'sony', 'sample/brand/8.jpg', '', 0, 0, 's', 1596019822, 0),
(29, -1, -1, 4, 0, 'Siemens', 'simens', 'sample/brand/9.jpg', '', 0, 0, 'x', 1596019822, 0),
(32, 1, 1, 275, 0, 'Alcatel', 'alcatel', 'sample/brand/10.jpg', 'asdf', 0, 0, 'a', 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_brandtypes`
--

DROP TABLE IF EXISTS `pb_brandtypes`;
CREATE TABLE IF NOT EXISTS `pb_brandtypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(100) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_brandtypes`
--

INSERT INTO `pb_brandtypes` (`id`, `parent_id`, `level`, `name`, `display_order`) VALUES
(1, 0, 1, 'Decorative building materials', 0),
(2, 0, 1, 'Clothing and shoes', 0),
(3, 0, 1, 'Household Furniture', 0),
(4, 0, 1, 'Food', 0),
(5, 0, 1, 'Digital home appliances', 0),
(6, 0, 1, 'Auto Real Estate', 0),
(7, 0, 1, 'Dining Entertainment', 0),
(8, 0, 1, 'Mechanical Chemical', 0),
(9, 5, 2, 'Washing machine', 0),
(10, 5, 2, 'Drinking', 0),
(11, 5, 2, 'Computer', 0),
(12, 5, 2, 'Mobile', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_companies`
--

DROP TABLE IF EXISTS `pb_companies`;
CREATE TABLE IF NOT EXISTS `pb_companies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT 0,
  `cache_spacename` varchar(255) NOT NULL DEFAULT '',
  `cache_membergroupid` smallint(3) NOT NULL DEFAULT 0,
  `cache_credits` smallint(6) NOT NULL DEFAULT 0,
  `topleveldomain` varchar(255) NOT NULL DEFAULT '',
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` char(6) NOT NULL DEFAULT '0',
  `area_id2` char(6) NOT NULL DEFAULT '0',
  `area_id3` char(6) NOT NULL DEFAULT '0',
  `type_id` tinyint(2) NOT NULL DEFAULT 0,
  `name` char(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `english_name` char(100) NOT NULL DEFAULT '',
  `keywords` varchar(50) NOT NULL DEFAULT '',
  `boss_name` varchar(25) NOT NULL DEFAULT '',
  `manage_type` varchar(25) NOT NULL DEFAULT '',
  `year_annual` tinyint(2) NOT NULL DEFAULT 0,
  `property` tinyint(1) NOT NULL DEFAULT 0,
  `configs` text DEFAULT NULL,
  `bank_from` varchar(50) NOT NULL DEFAULT '',
  `bank_account` varchar(50) NOT NULL DEFAULT '',
  `main_prod` varchar(100) NOT NULL DEFAULT '',
  `employee_amount` varchar(25) NOT NULL DEFAULT '',
  `found_date` char(10) NOT NULL DEFAULT '0',
  `reg_fund` tinyint(2) NOT NULL DEFAULT 0,
  `reg_address` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `zipcode` varchar(15) NOT NULL DEFAULT '',
  `main_brand` varchar(100) NOT NULL DEFAULT '',
  `main_market` varchar(200) NOT NULL DEFAULT '',
  `main_biz_place` varchar(50) NOT NULL DEFAULT '',
  `main_customer` varchar(200) NOT NULL DEFAULT '',
  `link_man` varchar(25) NOT NULL DEFAULT '',
  `link_man_gender` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(1) NOT NULL DEFAULT 0,
  `tel` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(25) NOT NULL DEFAULT '',
  `mobile` varchar(25) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `site_url` varchar(100) NOT NULL DEFAULT '',
  `picture` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `first_letter` char(2) NOT NULL DEFAULT '',
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` int(5) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_companies`
--

INSERT INTO `pb_companies` (`id`, `member_id`, `cache_spacename`, `cache_membergroupid`, `cache_credits`, `topleveldomain`, `industry_id1`, `industry_id2`, `industry_id3`, `area_id1`, `area_id2`, `area_id3`, `type_id`, `name`, `description`, `english_name`, `keywords`, `boss_name`, `manage_type`, `year_annual`, `property`, `configs`, `bank_from`, `bank_account`, `main_prod`, `employee_amount`, `found_date`, `reg_fund`, `reg_address`, `address`, `zipcode`, `main_brand`, `main_market`, `main_biz_place`, `main_customer`, `link_man`, `link_man_gender`, `position`, `tel`, `fax`, `mobile`, `email`, `site_url`, `picture`, `status`, `first_letter`, `if_commend`, `clicked`, `created`, `modified`) VALUES
(1, 1, 'admin', 10, 0, '', 1, 11, 24, '1', '2', '3', 2, 'Ualink E-Commerce', 'PHPB2B', 'UALINK', '', 'Steven chow', '4', 3, 1, 'a:1:{s:12:\"templet_name\";b:0;}', 'Beijing Bank', '12342143', 'Ualink', '4', '975542400', 5, 'Beijing', 'Beijing City', '100010', 'Ualink', '', 'Beijing City', '', 'Stevenchow', 1, 4, '(086)10-84128912', '(086)10-84128912', '13012345678', 'service@phpb2b.com', 'http://www.phpb2b.com/', 'sample/company/1.jpg', 1, 'Z', 1, 1, 1596019821, 1596019821),
(13, 1, 'admin', 9, 0, '', 0, 0, 0, '1', '0', '0', 2, 'Beijing Ualink E-Commerce Inc.', 'Beijing Ualink E-Commerce Inc.', 'Ualink', '', '', '0', 0, 1, NULL, '', '', 'Ualink', '', '', 5, 'Beijing', 'Dongcheng District, Beijing', '100010', 'Ualink', '', 'Beijing', '', '', 1, 4, '(000)10-84128912', '(000)10-84128912', '', 'service@phpb2b.com', 'http://www.phpb2b.com/', 'sample/company/1.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(3, 2, 'athena1', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Dongguan Chong Yi Plastics Co', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(4, 2, 'athena2', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Copper Co., Ltd. Ningbo Jiangdong Xing Feng', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(5, 2, 'athena3', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Shenzhen Electronic Technology Co., Ltd. letter Noda', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(6, 2, 'athena4', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Friction Material Co., Ltd. Hangzhou Jonah', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(7, 2, 'athena5', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Shenzhen Hualian Trade Co., Ltd.', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(8, 2, 'athena6', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Building Decoration Co., Ltd. Beijing Xuan St.', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(9, 2, 'athena7', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'On Haixibosi Flow Control Co., Ltd.', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(10, 2, 'athena8', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Sichuan Jin Ming Building Material Co., Ltd.', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0),
(11, 2, 'athena9', 9, 0, '', 0, 0, 0, '1', '2', '3', 0, 'Master Automation Suzhou Environmental Protection Equipment Co., Ltd. 100', 'This is the presentation of data, do not guarantee the authenticity of the data', '', '', '', '0', 0, 1, NULL, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 1, 7, '', '', '', '', '', 'sample/company/no.jpg', 1, 'Z', 1, 1, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_companyfields`
--

DROP TABLE IF EXISTS `pb_companyfields`;
CREATE TABLE IF NOT EXISTS `pb_companyfields` (
  `company_id` int(10) NOT NULL DEFAULT 0,
  `map_longitude` varchar(25) NOT NULL DEFAULT '',
  `map_latitude` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_companynewses`
--

DROP TABLE IF EXISTS `pb_companynewses`;
CREATE TABLE IF NOT EXISTS `pb_companynewses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `company_id` int(10) NOT NULL DEFAULT -1,
  `type_id` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `picture` varchar(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` int(5) NOT NULL DEFAULT 1,
  `created` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_companynewstypes`
--

DROP TABLE IF EXISTS `pb_companynewstypes`;
CREATE TABLE IF NOT EXISTS `pb_companynewstypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_companytypes`
--

DROP TABLE IF EXISTS `pb_companytypes`;
CREATE TABLE IF NOT EXISTS `pb_companytypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `display_order` tinyint(1) NOT NULL,
  `url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_dicts`
--

DROP TABLE IF EXISTS `pb_dicts`;
CREATE TABLE IF NOT EXISTS `pb_dicts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dicttype_id` smallint(6) NOT NULL DEFAULT 0,
  `extend_dicttypeid` varchar(25) NOT NULL DEFAULT '',
  `word` varchar(255) NOT NULL DEFAULT '',
  `word_name` varchar(255) NOT NULL DEFAULT '',
  `digest` varchar(255) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `picture` varchar(255) NOT NULL DEFAULT '',
  `refer` tinytext DEFAULT NULL,
  `hits` int(10) NOT NULL DEFAULT 1,
  `closed` tinyint(1) NOT NULL DEFAULT 0,
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_dicts`
--

INSERT INTO `pb_dicts` (`id`, `dicttype_id`, `extend_dicttypeid`, `word`, `word_name`, `digest`, `content`, `picture`, `refer`, `hits`, `closed`, `if_commend`, `created`, `modified`) VALUES
(1, 7, '', 'Cost and freight', 'Cost and Freight', 'Cost and freight（Cost and Freight）(named port of shipment)，Named port of destination。</p>', '<p>Cost and freight（Cost and Freight）(named port of shipment)，Named port of destination。It means the seller must pay the goods to the named port of destination, cost and freight, but goods to the ship deck, the cargo risk, loss or damage and accidents resulting from the additional expenditure in the goods pass the designated port ship, the burden on the buyer by the seller．Also requires the seller of goods customs clearance procedures. The term for sea or inland waterway transport.', '', '', 2, 0, 0, 1596019822, 1596019822),
(2, 0, '', 'Multimodal transport', '', 'Multimodal transport, is the shipment to destination from the transport process contains two or more modes of transport, sea, land, air, river and so on.', '', '', '', 1, 0, 0, 1596019822, 1596019822);

-- --------------------------------------------------------

--
-- Table structure for table `pb_dicttypes`
--

DROP TABLE IF EXISTS `pb_dicttypes`;
CREATE TABLE IF NOT EXISTS `pb_dicttypes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_dicttypes`
--

INSERT INTO `pb_dicttypes` (`id`, `name`, `parent_id`, `display_order`) VALUES
(1, 'Logistics knowledge', 0, 0),
(2, 'Logistics Management', 0, 0),
(3, 'Logistics', 0, 0),
(4, 'Laws and regulations', 0, 0),
(5, 'E-commerce', 0, 0),
(6, 'Knowledge', 0, 0),
(7, 'Common Terms', 1, 0),
(8, 'Transport knowledge', 1, 0),
(9, 'Storage of knowledge', 1, 0),
(10, 'Logistics Equipment', 1, 0),
(11, 'Logistics Insurance', 1, 0),
(12, 'Third Party Logistics', 1, 0),
(13, 'Supply Chain', 1, 0),
(14, 'Other', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_expoes`
--

DROP TABLE IF EXISTS `pb_expoes`;
CREATE TABLE IF NOT EXISTS `pb_expoes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expotype_id` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `begin_time` int(10) NOT NULL DEFAULT 0,
  `end_time` int(10) NOT NULL DEFAULT 0,
  `industry_ids` varchar(100) NOT NULL DEFAULT '0',
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `address` varchar(100) NOT NULL DEFAULT '',
  `stadium_name` varchar(100) NOT NULL DEFAULT '',
  `refresh_method` varchar(100) NOT NULL DEFAULT '',
  `scope` varchar(100) NOT NULL DEFAULT '',
  `hosts` varchar(255) NOT NULL DEFAULT '',
  `organisers` varchar(255) NOT NULL DEFAULT '',
  `co_organisers` varchar(255) NOT NULL DEFAULT '',
  `sponsors` varchar(255) NOT NULL DEFAULT '',
  `contacts` text DEFAULT NULL,
  `important_notice` text DEFAULT NULL,
  `picture` varchar(100) NOT NULL DEFAULT '',
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `hits` smallint(6) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_expoes`
--

INSERT INTO `pb_expoes` (`id`, `expotype_id`, `name`, `description`, `begin_time`, `end_time`, `industry_ids`, `industry_id1`, `industry_id2`, `industry_id3`, `area_id1`, `area_id2`, `area_id3`, `address`, `stadium_name`, `refresh_method`, `scope`, `hosts`, `organisers`, `co_organisers`, `sponsors`, `contacts`, `important_notice`, `picture`, `if_commend`, `status`, `hits`, `created`, `modified`) VALUES
(1, 1, 'The 14th China Beauty Expo opening', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(2, 1, 'The 29th Guangzhou International Beauty Fair Aspect wonderful show', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(3, 1, 'Nutrition and Health of the Tenth China International Industry Fair', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(4, 1, 'Shanghai, China, Textiles and Nonwovens Exhibition', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(5, 2, 'Tokyo International Exhibition of footwear and leather products', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(6, 2, 'Th China International Glass Industrial Technical Exhibition', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(7, 1, 'Fifth Inner Mongolia, China Food Processing and Packaging Machinery Exhibition', '', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, 1, 1596019822, 0),
(8, 2, 'Seventh China International Food Processing and Packaging Equipment Exhibition', NULL, 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', NULL, NULL, '', 0, 1, 1, 1596019822, 0),
(9, 1, 'Tenth China Beijing International Green Food and Organic Food Fair', NULL, 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', NULL, NULL, '', 0, 1, 1, 1596019822, 0),
(10, 2, '15th China International Building Decoration Materials Exhibition', NULL, 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', NULL, NULL, '', 0, 1, 1, 1596019822, 0),
(11, 1, 'Third Western China Building Technology Exhibition', NULL, 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', NULL, NULL, 'sample/other/fair-2.jpg', 1, 1, 1, 1596019822, 0),
(12, 1, 'World Expo 2010 Shanghai China', 'Shanghai World Expo is our opportunity', 1596019822, 1598611822, '0', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', NULL, NULL, 'sample/other/fair-1.jpg', 1, 1, 1, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_expomembers`
--

DROP TABLE IF EXISTS `pb_expomembers`;
CREATE TABLE IF NOT EXISTS `pb_expomembers` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `expo_id` smallint(6) NOT NULL DEFAULT 0,
  `member_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `expo_id` (`expo_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_expostadiums`
--

DROP TABLE IF EXISTS `pb_expostadiums`;
CREATE TABLE IF NOT EXISTS `pb_expostadiums` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `sa` varchar(100) DEFAULT '',
  `country_id` smallint(6) DEFAULT 0,
  `province_id` smallint(6) DEFAULT 0,
  `city_id` smallint(6) DEFAULT 0,
  `sb` varchar(200) DEFAULT '',
  `sc` varchar(150) DEFAULT '',
  `sd` varchar(150) DEFAULT '',
  `se` varchar(150) DEFAULT '',
  `sf` varchar(150) DEFAULT '',
  `sg` text DEFAULT NULL,
  `sh` smallint(6) DEFAULT 0,
  `created` int(10) DEFAULT NULL,
  `modified` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_expotypes`
--

DROP TABLE IF EXISTS `pb_expotypes`;
CREATE TABLE IF NOT EXISTS `pb_expotypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_expotypes`
--

INSERT INTO `pb_expotypes` (`id`, `name`, `created`, `modified`) VALUES
(1, 'China Exhibition', 0, 0),
(2, 'International Exhibition', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_favorites`
--

DROP TABLE IF EXISTS `pb_favorites`;
CREATE TABLE IF NOT EXISTS `pb_favorites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `target_id` int(10) NOT NULL,
  `type_id` tinyint(1) NOT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`,`target_id`,`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_feeds`
--

DROP TABLE IF EXISTS `pb_feeds`;
CREATE TABLE IF NOT EXISTS `pb_feeds` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(1) NOT NULL DEFAULT 0,
  `type` varchar(100) NOT NULL DEFAULT '',
  `member_id` int(10) NOT NULL DEFAULT 0,
  `username` varchar(100) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_formattributes`
--

DROP TABLE IF EXISTS `pb_formattributes`;
CREATE TABLE IF NOT EXISTS `pb_formattributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(1) NOT NULL,
  `form_id` smallint(3) NOT NULL DEFAULT 0,
  `formitem_id` smallint(3) NOT NULL DEFAULT 0,
  `primary_id` int(10) NOT NULL,
  `attribute` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_formitems`
--

DROP TABLE IF EXISTS `pb_formitems`;
CREATE TABLE IF NOT EXISTS `pb_formitems` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `form_id` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `identifier` varchar(50) NOT NULL DEFAULT '',
  `type` enum('checkbox','select','radio','calendar','url','image','textarea','email','number','text') NOT NULL DEFAULT 'text',
  `rules` text DEFAULT NULL,
  `display_order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_formitems`
--

INSERT INTO `pb_formitems` (`id`, `form_id`, `title`, `description`, `identifier`, `type`, `rules`, `display_order`) VALUES
(1, 0, 'Numbers', '', 'product_quantity', 'text', '', 0),
(2, 0, 'Packaging', '', 'packing', 'text', '', 0),
(3, 0, 'Price Description', '', 'product_price', 'text', '', 0),
(4, 0, 'Specifications', '', 'product_specification', 'text', '', 0),
(5, 0, 'ID Code', '', 'serial_number', 'text', '', 0),
(6, 0, 'Place', '', 'production_place', 'text', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_forms`
--

DROP TABLE IF EXISTS `pb_forms`;
CREATE TABLE IF NOT EXISTS `pb_forms` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(1) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `items` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_forms`
--

INSERT INTO `pb_forms` (`id`, `type_id`, `name`, `items`) VALUES
(1, 1, 'Supply and demand of custom fields', '1,2,3,4,5,6'),
(2, 2, 'Product Custom Fields', '1,2,3,4,5,6');

-- --------------------------------------------------------

--
-- Table structure for table `pb_friendlinks`
--

DROP TABLE IF EXISTS `pb_friendlinks`;
CREATE TABLE IF NOT EXISTS `pb_friendlinks` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `friendlinktype_id` tinyint(1) NOT NULL DEFAULT 0,
  `industry_id` smallint(6) NOT NULL DEFAULT 0,
  `area_id` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(50) NOT NULL DEFAULT '',
  `logo` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(50) NOT NULL DEFAULT '',
  `priority` smallint(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `description` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_friendlinks`
--

INSERT INTO `pb_friendlinks` (`id`, `friendlinktype_id`, `industry_id`, `area_id`, `title`, `logo`, `url`, `priority`, `status`, `description`, `created`, `modified`) VALUES
(1, 1, 0, 0, 'Ualink', '', 'http://www.phpb2b.com/', 0, 1, '', 1596019821, 0),
(2, 2, 0, 0, 'Ualink E-commerce Web Demo', '', 'http://demo.phpb2b.com/', 0, 1, '', 1596019821, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_friendlinktypes`
--

DROP TABLE IF EXISTS `pb_friendlinktypes`;
CREATE TABLE IF NOT EXISTS `pb_friendlinktypes` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_friendlinktypes`
--

INSERT INTO `pb_friendlinktypes` (`id`, `name`) VALUES
(1, 'Links'),
(2, 'Partners');

-- --------------------------------------------------------

--
-- Table structure for table `pb_goods`
--

DROP TABLE IF EXISTS `pb_goods`;
CREATE TABLE IF NOT EXISTS `pb_goods` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `price` float(9,2) NOT NULL DEFAULT 0.00,
  `closed` tinyint(1) NOT NULL DEFAULT 1,
  `picture` varchar(100) NOT NULL DEFAULT '',
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_goods`
--

INSERT INTO `pb_goods` (`id`, `name`, `description`, `price`, `closed`, `picture`, `if_commend`, `created`, `modified`) VALUES
(2, 'Senior Member Upgrade', '', 1500.00, 1, '', 0, 1596019822, 1596019822),
(1, 'General Membership Upgrade', '', 1000.00, 1, '', 0, 1596019822, 1596019822);

-- --------------------------------------------------------

--
-- Table structure for table `pb_helps`
--

DROP TABLE IF EXISTS `pb_helps`;
CREATE TABLE IF NOT EXISTS `pb_helps` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `helptype_id` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_helptypes`
--

DROP TABLE IF EXISTS `pb_helptypes`;
CREATE TABLE IF NOT EXISTS `pb_helptypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_htmlcaches`
--

DROP TABLE IF EXISTS `pb_htmlcaches`;
CREATE TABLE IF NOT EXISTS `pb_htmlcaches` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) NOT NULL DEFAULT '',
  `last_cached_time` int(10) NOT NULL DEFAULT 0,
  `cache_cycle_time` int(10) NOT NULL DEFAULT 86400,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_industries`
--

DROP TABLE IF EXISTS `pb_industries`;
CREATE TABLE IF NOT EXISTS `pb_industries` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `attachment_id` int(9) NOT NULL DEFAULT 0,
  `industrytype_id` smallint(3) NOT NULL DEFAULT 0,
  `child_ids` text DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `alias_name` varchar(255) NOT NULL DEFAULT '',
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `top_parentid` smallint(6) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_industries`
--

INSERT INTO `pb_industries` (`id`, `attachment_id`, `industrytype_id`, `child_ids`, `name`, `url`, `alias_name`, `highlight`, `parent_id`, `top_parentid`, `level`, `display_order`, `description`, `available`, `created`, `modified`) VALUES
(1, 0, 0, NULL, 'Agriculture', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(2, 0, 0, NULL, 'Apparel&Fashion', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(3, 0, 0, NULL, 'Automobile', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(4, 0, 0, NULL, 'BusinessServices', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(5, 0, 0, NULL, 'Chemicals', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(6, 0, 0, NULL, 'ComputerHardwareSoftware', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(7, 0, 0, NULL, 'Construction&RealEstate', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(8, 0, 0, NULL, 'Electronics&Electrical', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(9, 0, 0, NULL, 'Energy', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(10, 0, 0, NULL, 'Environment', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(11, 0, 0, NULL, 'ExcessInventory', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(12, 0, 0, NULL, 'Food&Beverage', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(13, 0, 0, NULL, 'Gifts&Crafts', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(14, 0, 0, NULL, 'Health&Beauty', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(15, 0, 0, NULL, 'HomeAppliances', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(16, 0, 0, NULL, 'HomeSupplies', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(17, 0, 0, NULL, 'IndustrialSupplies', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(18, 0, 0, NULL, 'Minerals,Metals&Materials', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(19, 0, 0, NULL, 'OfficeSupplies', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(20, 0, 0, NULL, 'Packaging&Paper', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(21, 0, 0, NULL, 'Printing&Publishing', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(22, 0, 0, NULL, 'Security&Protection', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(23, 0, 0, NULL, 'Sports&Entertainment', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(24, 0, 0, NULL, 'Telecommunications', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(25, 0, 0, NULL, 'Textiles&LeatherProducts', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(26, 0, 0, NULL, 'Toys', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(27, 0, 0, NULL, 'Transportation', '', '', 0, 0, 0, 1, 0, NULL, 1, 0, 0),
(28, 0, 0, NULL, 'Agriculture&By-productAgent', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(29, 0, 0, NULL, 'AgricultureProductStocks', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(30, 0, 0, NULL, 'Agrochemicals&Pesticides', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(31, 0, 0, NULL, 'AnimalExtract', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(32, 0, 0, NULL, 'AnimalFodders', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(33, 0, 0, NULL, 'AnimalHusbandry', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(34, 0, 0, NULL, 'Bamboo&RattanProducts', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(35, 0, 0, NULL, 'Beans', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(36, 0, 0, NULL, 'Cigarette&Tobacco', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(37, 0, 0, NULL, 'Eggs', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(38, 0, 0, NULL, 'FarmMachines&Tools', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(39, 0, 0, NULL, 'FisheryMachinery', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(40, 0, 0, NULL, 'Flowers&Plant', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(41, 0, 0, NULL, 'ForestMachinery', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(42, 0, 0, NULL, 'Fruit', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(43, 0, 0, NULL, 'Grain', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(44, 0, 0, NULL, 'Mushroom&Truffle', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(45, 0, 0, NULL, 'Nuts&Kernels', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(46, 0, 0, NULL, 'Others', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(47, 0, 0, NULL, 'Plant&AnimalOil', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(48, 0, 0, NULL, 'PlantExtract', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(49, 0, 0, NULL, 'PlantSeed', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(50, 0, 0, NULL, 'Poultry&Livestock', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(51, 0, 0, NULL, 'Vegetable', '', '', 0, 1, 1, 2, 0, NULL, 1, 0, 0),
(52, 0, 0, NULL, 'Apparel & Fashion Agents', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(53, 0, 0, NULL, 'Apparel Stocks', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(54, 0, 0, NULL, 'Athletic Wear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(55, 0, 0, NULL, 'Bathrobe', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(56, 0, 0, NULL, 'Children Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(57, 0, 0, NULL, 'Costume & Ceremony', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(58, 0, 0, NULL, 'Down Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(59, 0, 0, NULL, 'Ethnic Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(60, 0, 0, NULL, 'Fashion Accessories', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(61, 0, 0, NULL, 'Belts & Accessories Hats & Caps moreFootwear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(62, 0, 0, NULL, 'Sports Shoes Boots moreFur Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(63, 0, 0, NULL, 'Garment Accessories', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(64, 0, 0, NULL, 'Buttons & Buckles Others moreGloves & Mittens', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(65, 0, 0, NULL, 'Infant Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(66, 0, 0, NULL, 'Jacket', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(67, 0, 0, NULL, 'Jeans', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(68, 0, 0, NULL, 'Leather Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(69, 0, 0, NULL, 'Leisure Wear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(70, 0, 0, NULL, 'Others', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(71, 0, 0, NULL, 'Outerwear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(72, 0, 0, NULL, 'Pants & Trousers', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(73, 0, 0, NULL, 'Related Machine', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(74, 0, 0, NULL, 'Sewing Machinery', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(75, 0, 0, NULL, 'Shirts & Blouses', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(76, 0, 0, NULL, 'Silk Garment', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(77, 0, 0, NULL, 'Skirts & Dresses', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(78, 0, 0, NULL, 'Socks & Stockings', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(79, 0, 0, NULL, 'Speciality', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(80, 0, 0, NULL, 'Suits & Tuxedo', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(81, 0, 0, NULL, 'Sweaters', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(82, 0, 0, NULL, 'Swimwear & Beachwear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(83, 0, 0, NULL, 'T-Shirts', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(84, 0, 0, NULL, 'Underwear & Nightwear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(85, 0, 0, NULL, 'Bras & Lingerie Underwear Set moreUniforms & Workwear', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0),
(86, 0, 0, NULL, 'Used Clothes', '', '', 0, 16, 16, 2, 0, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_industryfields`
--

DROP TABLE IF EXISTS `pb_industryfields`;
CREATE TABLE IF NOT EXISTS `pb_industryfields` (
  `industry_id` smallint(6) NOT NULL,
  `type_id` enum('offer','company','product','fair','market','news','hr') NOT NULL DEFAULT 'offer',
  `keyword_ids` text DEFAULT NULL,
  PRIMARY KEY (`industry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_industrymodels`
--

DROP TABLE IF EXISTS `pb_industrymodels`;
CREATE TABLE IF NOT EXISTS `pb_industrymodels` (
  `industry_id` smallint(6) NOT NULL,
  `model_id` enum('offers','products','companies') NOT NULL DEFAULT 'offers',
  PRIMARY KEY (`industry_id`,`model_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_industrytypes`
--

DROP TABLE IF EXISTS `pb_industrytypes`;
CREATE TABLE IF NOT EXISTS `pb_industrytypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_industrytypes`
--

INSERT INTO `pb_industrytypes` (`id`, `name`) VALUES
(1, 'Industrial'),
(2, 'Consumer goods'),
(3, 'Raw materials'),
(4, 'Business Services'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `pb_inqueries`
--

DROP TABLE IF EXISTS `pb_inqueries`;
CREATE TABLE IF NOT EXISTS `pb_inqueries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `to_member_id` int(10) DEFAULT NULL,
  `to_company_id` int(10) DEFAULT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `send_achive` tinyint(1) DEFAULT NULL,
  `know_more` varchar(50) NOT NULL DEFAULT '',
  `exp_quantity` varchar(15) NOT NULL DEFAULT '',
  `exp_price` float(9,2) NOT NULL DEFAULT 0.00,
  `contacts` text DEFAULT NULL,
  `user_ip` varchar(11) DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_jobs`
--

DROP TABLE IF EXISTS `pb_jobs`;
CREATE TABLE IF NOT EXISTS `pb_jobs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `company_id` int(10) NOT NULL DEFAULT -1,
  `cache_spacename` varchar(25) NOT NULL DEFAULT '',
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(150) NOT NULL DEFAULT '',
  `work_station` varchar(50) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `require_gender_id` tinyint(1) NOT NULL DEFAULT 0,
  `peoples` varchar(5) NOT NULL DEFAULT '',
  `require_education_id` tinyint(1) NOT NULL DEFAULT 0,
  `require_age` varchar(10) NOT NULL DEFAULT '',
  `salary_id` tinyint(1) NOT NULL DEFAULT 0,
  `worktype_id` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` int(5) NOT NULL DEFAULT 1,
  `expire_time` int(10) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_jobtypes`
--

DROP TABLE IF EXISTS `pb_jobtypes`;
CREATE TABLE IF NOT EXISTS `pb_jobtypes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_jobtypes`
--

INSERT INTO `pb_jobtypes` (`id`, `parent_id`, `level`, `name`, `display_order`) VALUES
(1, 0, 1, 'software category', 0),
(2, 0, 1, 'site network management', 0),
(3, 0, 1, 'Database type', 0),
(4, 0, 1, 'IT Management', 0),
(5, 0, 1, 'IT Quality Assurance and technical support', 0),
(6, 0, 1, 'communication category', 0),
(7, 0, 1, 'design class', 0),
(8, 0, 1, 'customer service class', 0),
(9, 0, 1, 'Sales Consultant Class', 0),
(10, 0, 1, 'market type', 0),
(11, 0, 1, 'other', 0),
(12, 1, 2, 'Software Engineer', 0),
(13, 1, 2, 'Senior Software Engineer', 0),
(14, 1, 2, 'systems architect', 0),
(15, 1, 2, 'systems analyst', 0),
(16, 1, 2, 'system integration engineers', 0),
(17, 1, 2, 'software testing', 0),
(18, 1, 2, 'other', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_keywords`
--

DROP TABLE IF EXISTS `pb_keywords`;
CREATE TABLE IF NOT EXISTS `pb_keywords` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL DEFAULT '',
  `target_id` int(10) NOT NULL DEFAULT 0,
  `target_position` tinyint(1) NOT NULL DEFAULT 0,
  `type_name` enum('trades','companies','newses','products') NOT NULL DEFAULT 'trades',
  `hits` smallint(6) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_logs`
--

DROP TABLE IF EXISTS `pb_logs`;
CREATE TABLE IF NOT EXISTS `pb_logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `handle_type` enum('error','info','warning') NOT NULL DEFAULT 'info',
  `source_module` varchar(50) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `ip_address` int(10) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_markets`
--

DROP TABLE IF EXISTS `pb_markets`;
CREATE TABLE IF NOT EXISTS `pb_markets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `picture` varchar(50) NOT NULL DEFAULT '',
  `ip_address` int(10) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` smallint(6) NOT NULL DEFAULT 1,
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_markets`
--

INSERT INTO `pb_markets` (`id`, `name`, `content`, `area_id1`, `area_id2`, `area_id3`, `industry_id1`, `industry_id2`, `industry_id3`, `picture`, `ip_address`, `status`, `clicked`, `if_commend`, `created`, `modified`) VALUES
(1, 'Hardware products market', '', 0, 0, 0, 2, 0, 0, 'sample/market/01.jpg', 0, 1, 1, 1, 1596019822, 0),
(2, 'Fiber professional market', '', 0, 0, 0, 2, 0, 0, 'sample/market/02.jpg', 0, 1, 1, 1, 1596019822, 0),
(3, 'Haining China Leather Town', '', 0, 0, 0, 0, 0, 0, 'sample/market/03.jpg', 0, 1, 1, 1, 1596019822, 0),
(4, 'Fur World, Zhejiang Chongfu', '', 0, 0, 0, 0, 0, 0, 'sample/market/04.jpg', 0, 1, 1, 1, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_markettypes`
--

DROP TABLE IF EXISTS `pb_markettypes`;
CREATE TABLE IF NOT EXISTS `pb_markettypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_membercaches`
--

DROP TABLE IF EXISTS `pb_membercaches`;
CREATE TABLE IF NOT EXISTS `pb_membercaches` (
  `member_id` int(10) NOT NULL DEFAULT -1,
  `data1` text NOT NULL,
  `data2` text NOT NULL,
  `expiration` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_memberfields`
--

DROP TABLE IF EXISTS `pb_memberfields`;
CREATE TABLE IF NOT EXISTS `pb_memberfields` (
  `member_id` int(10) NOT NULL DEFAULT 0,
  `today_logins` smallint(6) NOT NULL DEFAULT 0,
  `total_logins` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `first_name` varchar(25) NOT NULL DEFAULT '',
  `last_name` varchar(25) NOT NULL DEFAULT '',
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `tel` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(25) NOT NULL DEFAULT '',
  `mobile` varchar(25) NOT NULL DEFAULT '',
  `qq` varchar(12) NOT NULL DEFAULT '',
  `msn` varchar(50) NOT NULL DEFAULT '',
  `icq` varchar(12) NOT NULL DEFAULT '',
  `yahoo` varchar(50) NOT NULL DEFAULT '',
  `skype` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `zipcode` varchar(16) NOT NULL DEFAULT '',
  `site_url` varchar(100) NOT NULL DEFAULT '',
  `question` varchar(50) NOT NULL DEFAULT '',
  `answer` varchar(50) NOT NULL DEFAULT '',
  `reg_ip` varchar(25) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_memberfields`
--

INSERT INTO `pb_memberfields` (`member_id`, `today_logins`, `total_logins`, `area_id1`, `area_id2`, `area_id3`, `first_name`, `last_name`, `gender`, `tel`, `fax`, `mobile`, `qq`, `msn`, `icq`, `yahoo`, `skype`, `address`, `zipcode`, `site_url`, `question`, `answer`, `reg_ip`) VALUES
(1, 0, 0, 6, 7, 9, 'steven', 'chow', 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '127.0.0.1'),
(2, 0, 0, 0, 0, 0, 'lucky', 'liu', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `pb_membergroups`
--

DROP TABLE IF EXISTS `pb_membergroups`;
CREATE TABLE IF NOT EXISTS `pb_membergroups` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `membertype_id` tinyint(1) NOT NULL DEFAULT -1,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `type` enum('define','special','system') NOT NULL DEFAULT 'define',
  `system` enum('private','public') NOT NULL DEFAULT 'private',
  `picture` varchar(50) NOT NULL DEFAULT 'default.gif',
  `point_max` smallint(6) NOT NULL DEFAULT 0,
  `point_min` smallint(6) NOT NULL DEFAULT 0,
  `max_offer` smallint(3) NOT NULL DEFAULT 0,
  `max_product` smallint(3) NOT NULL DEFAULT 0,
  `max_job` smallint(3) NOT NULL DEFAULT 0,
  `max_companynews` smallint(3) NOT NULL DEFAULT 0,
  `max_producttype` smallint(3) NOT NULL DEFAULT 3,
  `max_album` smallint(3) NOT NULL DEFAULT 0,
  `max_attach_size` smallint(6) NOT NULL DEFAULT 0,
  `max_size_perday` smallint(6) NOT NULL DEFAULT 0,
  `max_favorite` smallint(3) NOT NULL DEFAULT 0,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `allow_offer` tinyint(1) NOT NULL DEFAULT 0,
  `allow_market` tinyint(1) NOT NULL DEFAULT 0,
  `allow_company` tinyint(1) NOT NULL DEFAULT 0,
  `allow_product` tinyint(1) NOT NULL DEFAULT 0,
  `allow_job` tinyint(1) NOT NULL DEFAULT 0,
  `allow_companynews` tinyint(1) NOT NULL DEFAULT 1,
  `allow_album` tinyint(1) NOT NULL DEFAULT 0,
  `allow_space` tinyint(1) NOT NULL DEFAULT 1,
  `default_live_time` tinyint(1) NOT NULL DEFAULT 1,
  `after_live_time` tinyint(1) NOT NULL DEFAULT 1,
  `exempt` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_membergroups`
--

INSERT INTO `pb_membergroups` (`id`, `membertype_id`, `name`, `description`, `type`, `system`, `picture`, `point_max`, `point_min`, `max_offer`, `max_product`, `max_job`, `max_companynews`, `max_producttype`, `max_album`, `max_attach_size`, `max_size_perday`, `max_favorite`, `is_default`, `allow_offer`, `allow_market`, `allow_company`, `allow_product`, `allow_job`, `allow_companynews`, `allow_album`, `allow_space`, `default_live_time`, `after_live_time`, `exempt`, `created`, `modified`) VALUES
(1, 1, 'Associate members', '', 'system', 'private', 'informal.gif', 0, -32767, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 1274002638),
(2, 1, 'Member', '', 'system', 'private', 'formal.gif', 32767, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 2, 25, 0, 1274002638),
(3, 1, 'Pending Member', 'Wait for verification', 'special', 'private', 'special_checking.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1274002638),
(4, 1, 'No Access', 'No Access Site', 'special', 'private', 'special_novisit.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1274002638),
(5, 1, 'Prohibits the publication of', 'Prohibition of any information published in the Business Room', 'special', 'private', 'special_noperm.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1274002638),
(6, 1, 'Prohibition of landing', 'Prohibition of landing business room', 'special', 'private', 'special_nologin.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 1274002638),
(7, 1, 'Ordinary Members', 'General Level Member', 'define', 'public', 'copper.gif', 0, 0, 5, 0, 0, 0, 3, 0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 1, 1, 0, 1, 8, 24, 0, 1274002638),
(8, 1, 'Member', 'Individual members', 'define', 'public', 'silver.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 6, 25, 0, 1274002638),
(9, 2, 'Corporate Member', 'This level of corporate members of the general', 'define', 'public', 'gold.gif', 0, 0, 2, 2, 0, 0, 3, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 31, 0, 1274002638),
(10, 2, 'VIP Member', 'Senior Corporate Member', 'define', 'public', 'vip.gif', 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 1, 1, 2, 31, 0, 1274002638);

-- --------------------------------------------------------

--
-- Table structure for table `pb_members`
--

DROP TABLE IF EXISTS `pb_members`;
CREATE TABLE IF NOT EXISTS `pb_members` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `space_name` varchar(255) NOT NULL DEFAULT '',
  `templet_id` smallint(3) NOT NULL DEFAULT 0,
  `username` varchar(25) NOT NULL DEFAULT '',
  `userpass` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `points` smallint(6) NOT NULL DEFAULT 0,
  `credits` smallint(6) NOT NULL DEFAULT 0,
  `balance_amount` float(7,2) NOT NULL DEFAULT 0.00,
  `trusttype_ids` varchar(25) NOT NULL DEFAULT '',
  `status` enum('3','2','1','0') NOT NULL DEFAULT '0',
  `photo` varchar(100) NOT NULL DEFAULT '',
  `membertype_id` smallint(3) NOT NULL DEFAULT 0,
  `membergroup_id` smallint(3) NOT NULL DEFAULT 0,
  `last_login` varchar(11) NOT NULL DEFAULT '0',
  `last_ip` varchar(25) NOT NULL DEFAULT '0',
  `service_start_date` varchar(11) NOT NULL DEFAULT '0',
  `service_end_date` varchar(11) NOT NULL DEFAULT '0',
  `office_redirect` smallint(6) NOT NULL DEFAULT 0,
  `created` varchar(10) NOT NULL DEFAULT '0',
  `modified` varchar(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_members`
--

INSERT INTO `pb_members` (`id`, `space_name`, `templet_id`, `username`, `userpass`, `email`, `points`, `credits`, `balance_amount`, `trusttype_ids`, `status`, `photo`, `membertype_id`, `membergroup_id`, `last_login`, `last_ip`, `service_start_date`, `service_end_date`, `office_redirect`, `created`, `modified`) VALUES
(1, '', 0, 'admin', 'bf548c326c232ee4906c5e84f9dd5808', 'buyitgh@gmail.com', 0, 0, 0.00, '', '1', '', 2, 9, '0', '0', '0', '0', 0, '1596019810', '1596019810'),
(2, 'athena', 1, 'athena', 'bf548c326c232ee4906c5e84f9dd5808', 'phpb2b@163.com', 82, 80, 0.00, '2,1', '1', '', 2, 2, '1600270466', 'unknown', '1600270690', '1600875490', 0, '1596019821', '1600270158');

-- --------------------------------------------------------

--
-- Table structure for table `pb_membertypes`
--

DROP TABLE IF EXISTS `pb_membertypes`;
CREATE TABLE IF NOT EXISTS `pb_membertypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `default_membergroup_id` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_membertypes`
--

INSERT INTO `pb_membertypes` (`id`, `default_membergroup_id`, `name`, `description`) VALUES
(1, 7, 'Individual Member', ''),
(2, 9, 'Corporate Member', '');

-- --------------------------------------------------------

--
-- Table structure for table `pb_messages`
--

DROP TABLE IF EXISTS `pb_messages`;
CREATE TABLE IF NOT EXISTS `pb_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` enum('system','user','inquery') NOT NULL DEFAULT 'user',
  `from_member_id` int(10) NOT NULL DEFAULT -1,
  `cache_from_username` varchar(25) NOT NULL DEFAULT '',
  `to_member_id` int(10) NOT NULL DEFAULT -1,
  `cache_to_username` varchar(25) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_messages`
--

INSERT INTO `pb_messages` (`id`, `type`, `from_member_id`, `cache_from_username`, `to_member_id`, `cache_to_username`, `title`, `content`, `status`, `created`) VALUES
(1, 'user', 2, 'athena', 1, 'admin', 'Test', 'Hello this item thanks i want ', 0, 1600278255);

-- --------------------------------------------------------

--
-- Table structure for table `pb_navs`
--

DROP TABLE IF EXISTS `pb_navs`;
CREATE TABLE IF NOT EXISTS `pb_navs` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `target` enum('_blank','_self') NOT NULL DEFAULT '_self',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `display_order` smallint(3) NOT NULL DEFAULT 0,
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `class_name` varchar(25) NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_navs`
--

INSERT INTO `pb_navs` (`id`, `parent_id`, `name`, `description`, `url`, `target`, `status`, `display_order`, `highlight`, `level`, `class_name`, `created`, `modified`) VALUES
(1, 0, 'Home', '', 'index.php', '_self', 1, 1, 0, 0, '', 1596019821, 0),
(2, 0, 'Buyer', '', 'buy/', '_self', 1, 2, 0, 0, '', 1596019821, 0),
(3, 0, 'Seller', '', 'sell/', '_self', 1, 3, 0, 0, '', 1596019821, 0),
(4, 0, 'Wholesale', '', 'offer/list.php?typeid=7&navid=4', '_self', 1, 4, 0, 0, '', 1596019821, 0),
(5, 0, 'Price', '', 'product/price.php', '_self', 1, 5, 0, 0, '', 1596019821, 0),
(6, 0, 'Quote', '', 'market/quote.php', '_self', 1, 7, 0, 0, '', 1596019821, 0),
(7, 0, 'Market', '', 'market/index.php', '_self', 1, 8, 0, 0, '', 1596019821, 0),
(8, 0, 'Fair', '', 'fair/index.php', '_self', 1, 9, 0, 0, '', 1596019821, 0),
(9, 0, 'Jobs', '', 'hr/index.php', '_self', 1, 10, 0, 0, '', 1596019821, 0),
(10, 0, 'Brands', '', 'brand.php', '_self', 1, 6, 0, 0, '', 1596019821, 0),
(11, 0, 'Dictionay', '', 'dict/', '_self', 1, 11, 0, 0, '', 1596019821, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_newscomments`
--

DROP TABLE IF EXISTS `pb_newscomments`;
CREATE TABLE IF NOT EXISTS `pb_newscomments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) NOT NULL DEFAULT 0,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `cache_username` varchar(25) NOT NULL DEFAULT '',
  `message` text DEFAULT NULL,
  `ip_address` char(15) NOT NULL DEFAULT '',
  `invisible` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `date_line` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_newses`
--

DROP TABLE IF EXISTS `pb_newses`;
CREATE TABLE IF NOT EXISTS `pb_newses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` smallint(3) NOT NULL DEFAULT 0,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `industry_id` smallint(3) NOT NULL DEFAULT 0,
  `area_id` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `source` varchar(25) NOT NULL DEFAULT '',
  `picture` varchar(50) NOT NULL DEFAULT '',
  `if_focus` tinyint(1) NOT NULL DEFAULT 0,
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `clicked` int(10) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `flag` tinyint(1) NOT NULL DEFAULT 0,
  `require_membertype` varchar(15) NOT NULL DEFAULT '0',
  `tag_ids` varchar(255) DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_newses`
--

INSERT INTO `pb_newses` (`id`, `type_id`, `type`, `industry_id`, `area_id`, `title`, `content`, `source`, `picture`, `if_focus`, `if_commend`, `highlight`, `clicked`, `status`, `flag`, `require_membertype`, `tag_ids`, `created`, `create_time`, `modified`) VALUES
(1, 1, 0, 0, 0, 'Oral and cultural development of domestic opportunities motivate', '', '', '', 0, 0, 0, 1, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(2, 1, 0, 0, 0, 'Q CEP certification issues related to EU', '', '', '', 0, 0, 0, 1, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(3, 1, 0, 0, 0, 'Health Products counterfeit drug sales in the six ways', '', '', '', 0, 0, 0, 4, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(4, 1, 0, 0, 0, 'Gift ideas to promote business in China', '', '', 'sample/news/1.jpg', 0, 0, 0, 1, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(5, 2, 0, 0, 0, 'Iron ore project works against flotation Tender Notice', '', '', '', 0, 0, 0, 2, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(6, 2, 0, 0, 0, 'Public shoe brand marketing war started', '', '', '', 0, 0, 0, 3, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(7, 2, 0, 0, 0, 'Stupid rat autumn and winter will be a successful battle order', '', '', '', 0, 0, 0, 1, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(8, 2, 0, 0, 0, 'Cocoa Magic Paradise duck song sounded happy childhood', '', '', 'sample/news/1.jpg', 0, 0, 0, 2, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(9, 2, 0, 0, 0, 'Interview with Blue Star Glass Company Sales Director Han Lijun', '', '', 'sample/news/2.jpg', 0, 0, 0, 4, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(10, 2, 0, 0, 0, 'Zhejiang businessmen settled two million items of glass diamonds Wuning', '', '', 'sample/news/3.jpg', 0, 0, 0, 4, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(11, 2, 0, 0, 0, 'Blue Show to enhance its brand image', '', '', 'sample/news/4.jpg', 0, 0, 0, 3, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(12, 2, 0, 0, 0, '2010 Nanjing Stone Market Status and Future Analysis', '', '', 'sample/news/5.jpg', 0, 0, 0, 6, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0),
(13, 2, 0, 0, 0, 'China, the revision of professional standards of pens', '', '', 'sample/news/6.jpg', 0, 0, 0, 13, 1, 0, '0', '', 1596019822, '2020-07-29 10:50:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_newstypes`
--

DROP TABLE IF EXISTS `pb_newstypes`;
CREATE TABLE IF NOT EXISTS `pb_newstypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `level_id` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_newstypes`
--

INSERT INTO `pb_newstypes` (`id`, `name`, `level_id`, `status`, `parent_id`, `created`) VALUES
(1, 'Industry Focus', 1, 1, 0, 1596019822),
(2, 'Headline News', 1, 1, 0, 1596019822),
(3, 'Site News', 1, 1, 0, 1596019822),
(4, 'Company reports', 1, 1, 0, 1596019822),
(5, 'Media Highlights', 1, 1, 0, 1596019822),
(6, 'Hot topics', 1, 1, 0, 1596019822),
(7, 'High-end interview', 1, 1, 0, 1596019822),
(8, 'News Express', 1, 1, 0, 1596019822);

-- --------------------------------------------------------

--
-- Table structure for table `pb_ordergoods`
--

DROP TABLE IF EXISTS `pb_ordergoods`;
CREATE TABLE IF NOT EXISTS `pb_ordergoods` (
  `goods_id` smallint(6) NOT NULL DEFAULT 0,
  `order_id` smallint(6) UNSIGNED ZEROFILL NOT NULL DEFAULT 000000,
  `amount` smallint(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`goods_id`,`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_orders`
--

DROP TABLE IF EXISTS `pb_orders`;
CREATE TABLE IF NOT EXISTS `pb_orders` (
  `id` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `anonymous` tinyint(1) NOT NULL DEFAULT 0,
  `cache_username` varchar(25) NOT NULL DEFAULT '',
  `total_price` float(9,2) NOT NULL DEFAULT 0.00,
  `content` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_passports`
--

DROP TABLE IF EXISTS `pb_passports`;
CREATE TABLE IF NOT EXISTS `pb_passports` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `title` varchar(25) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `url` varchar(25) NOT NULL DEFAULT '',
  `config` text DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_payments`
--

DROP TABLE IF EXISTS `pb_payments`;
CREATE TABLE IF NOT EXISTS `pb_payments` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `title` varchar(25) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `config` text DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `if_online_support` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_personals`
--

DROP TABLE IF EXISTS `pb_personals`;
CREATE TABLE IF NOT EXISTS `pb_personals` (
  `member_id` int(10) NOT NULL,
  `resume_status` tinyint(1) NOT NULL DEFAULT 0,
  `max_education` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_plugins`
--

DROP TABLE IF EXISTS `pb_plugins`;
CREATE TABLE IF NOT EXISTS `pb_plugins` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `title` varchar(25) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `copyright` varchar(25) NOT NULL DEFAULT '',
  `version` varchar(15) NOT NULL DEFAULT '',
  `pluginvar` text DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_plugins`
--

INSERT INTO `pb_plugins` (`id`, `name`, `title`, `description`, `copyright`, `version`, `pluginvar`, `available`, `created`, `modified`) VALUES
(1, 'hello', 'Demo plug-ins', 'This is a demo version', 'PB TEAM', '1.0', 'a:5:{s:5:\"movie\";s:24:\"plugins/hello/bcastr.swf\";s:5:\"width\";s:3:\"473\";s:6:\"height\";s:3:\"170\";s:7:\"bgcolor\";s:7:\"#ff0000\";s:9:\"flashvars\";s:27:\"xml=plugins/hello/hello.xml\";}', 1, 1596019821, 0),
(2, 'googlesitemap', 'Googlesitemap', 'Use Google sitemap Can improve the site/Pages SERP The ranking of', 'PB TEAM', '1.0', 'a:1:{s:7:\"lastmod\";s:19:\"2009-12-08 12:12:12\";}', 1, 1260263957, 1260263957),
(3, 'vcastr', 'Enterprise Video YellowPa', 'This plug-in can call the corporate video, use the method see the plug-in called:<{plugin name=\"vcastr\"}>', 'PB TEAM', '1.0', 'a:5:{s:5:\"movie\";s:27:\"plugins/vcastr/vcastr22.swf\";s:9:\"flashvars\";s:24:\"plugins/vcastr/video.xml\";s:5:\"width\";s:3:\"410\";s:6:\"height\";s:3:\"190\";s:10:\"flashtitle\";s:6:\"PHPB2B\";}', 1, 1596019821, 0),
(4, 'qqservice', 'QQ online Service', 'This plug-in now support online QQ, MSN, and mail services online', 'PB TEAM', '1.0', 'a:4:{s:3:\"url\";s:22:\"http://www.phpb2b.com/\";s:5:\"email\";s:17:\"steven@phpb2b.com\";s:2:\"qq\";s:8:\"47731473\";s:3:\"msn\";s:21:\"stevenchow811@163.com\";}', 1, 1596019821, 0),
(5, 'baidusitemap', 'Baidu sitemap', 'Baidu Internet forum using open protocols can increase your site traffic', 'PB TEAM', '1.0', '', 1, 1596019821, 0),
(6, 'okqq', 'QQ online Service', 'QQ online Service', 'PB TEAM', '1.0', NULL, 1, 1596019821, 0),
(7, 'card', 'Business card', 'By flash Reveal Business card，Call the method:<{plugin name=\"card\"}>', 'PB_TEAM', '1.0.0', NULL, 1, 1596019821, 0),
(8, 'translate', 'Google Translation Plugin', 'Use Google translation plugin, the establishment of timely translation website', 'PB TEAM', '1.0', NULL, 1, 1596019821, 0),
(9, 'googlemap', 'Googlemap Map', 'Use Google map Company Map', 'PB TEAM', '1.0', 'a:6:{s:3:\"key\";s:86:\"ABQIAAAAnfs7bKE82qgb3Zc2YyS-oBT2yXp_ZAY8_ufC3CFXhHIE1NvwkxSySz_REpPq-4WZA27OwgbtyR3VcA\";s:9:\"zoomlevel\";s:2:\"15\";s:5:\"width\";s:3:\"500\";s:6:\"height\";s:3:\"500\";s:3:\"lat\";s:18:\"29.264911735066963\";s:3:\"lng\";s:18:\"120.24544715881347\";}', 1, 1596019821, 0),
(10, 'video', 'Video player plug-in', 'Video player plug-in，Use JW Player.', 'PB_TEAM', '1.0.0', 'a:4:{s:5:\"movie\";s:24:\"plugins/video/player.swf\";s:5:\"width\";s:3:\"473\";s:6:\"height\";s:3:\"170\";s:7:\"bgcolor\";s:7:\"#ff0000\";}', 1, 1596019821, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_pointlogs`
--

DROP TABLE IF EXISTS `pb_pointlogs`;
CREATE TABLE IF NOT EXISTS `pb_pointlogs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `action_name` varchar(25) NOT NULL DEFAULT '',
  `points` smallint(3) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_pointlogs`
--

INSERT INTO `pb_pointlogs` (`id`, `member_id`, `action_name`, `points`, `description`, `ip_address`, `created`, `modified`) VALUES
(1, 2, 'logging', 1, '', 'unknown', 1600270440, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_productcategories`
--

DROP TABLE IF EXISTS `pb_productcategories`;
CREATE TABLE IF NOT EXISTS `pb_productcategories` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_productcategories`
--

INSERT INTO `pb_productcategories` (`id`, `parent_id`, `level`, `name`, `display_order`) VALUES
(1, 0, 1, 'ELECTRONIC ELEMENTS', 0),
(2, 0, 1, 'beauty treatment', 0),
(3, 0, 1, 'medical care', 0),
(4, 0, 1, 'instrument', 0),
(5, 0, 1, 'household goods', 0),
(6, 0, 1, 'gifts and toys', 0),
(7, 0, 1, 'Automobiles and distribution', 0),
(8, 0, 1, 'Metallurgy of steel', 0),
(9, 0, 1, 'package', 0),
(10, 0, 1, 'computer software', 0),
(11, 0, 1, 'building materials', 0),
(12, 0, 1, 'print', 0),
(13, 0, 1, 'appliances', 0),
(14, 0, 1, 'transport', 0),
(15, 0, 1, 'communication', 0),
(16, 0, 1, 'light industry', 0),
(17, 0, 1, 'Electric Electric', 0),
(18, 0, 1, 'petrochemicals', 0),
(19, 0, 1, 'Painting and the table', 0),
(20, 0, 1, 'rubber, plastics', 0),
(21, 0, 1, 'security', 0),
(22, 0, 1, 'Office Supplies', 0),
(23, 0, 1, 'media', 0),
(24, 0, 1, 'environmental protection and water treatment', 0),
(25, 0, 1, 'machinery', 0),
(26, 0, 1, 'Hardware', 0),
(27, 0, 1, 'leisure', 0),
(28, 0, 1, 'paper', 0),
(29, 0, 1, 'textile and leather', 0),
(30, 0, 1, 'clothing', 0),
(31, 0, 1, 'agriculture', 0),
(32, 0, 1, 'food and drink', 0),
(33, 0, 1, 'clothing', 0),
(34, 0, 1, 'business services', 0),
(35, 0, 1, 'machinery industries', 0),
(36, 1, 2, 'the insurance component', 0),
(37, 1, 2, 'semiconductors', 0),
(38, 1, 2, 'other electronic materials', 0),
(39, 1, 2, 'capacitor', 0),
(40, 1, 2, 'other electronic components', 0),
(41, 1, 2, 'transistor', 0),
(42, 1, 2, 'diodes', 0),
(43, 1, 2, 'frequency components', 0),
(44, 1, 2, 'inverter', 0),
(45, 1, 2, 'inductors', 0),
(46, 1, 2, 'electro-acoustic and components', 0),
(47, 1, 2, 'electro-acoustic devices', 0),
(48, 1, 2, 'electronic vacuum devices', 0),
(49, 1, 2, 'connect generating units', 0),
(50, 1, 2, 'relay', 0),
(51, 1, 2, 'piezoelectric crystal materials', 0),
(52, 1, 2, 'electronic paste', 0),
(53, 1, 2, 'electronic plastic products', 0),
(54, 1, 2, 'FET', 0),
(55, 1, 2, 'electronic processing', 0),
(56, 1, 2, 'magnetic and components', 0),
(57, 1, 2, 'PCB', 0),
(58, 1, 2, 'switching element', 0),
(59, 1, 2, 'Optoelectronics and Display Devices', 0),
(60, 1, 2, 'potentiometer', 0),
(61, 1, 2, 'display device', 0),
(62, 1, 2, 'sensors', 0),
(63, 1, 2, 'electrical measuring instrument', 0),
(64, 1, 2, 'electric ceramic materials', 0),
(65, 1, 2, 'IC', 0),
(66, 1, 2, 'resistor', 0),
(67, 1, 2, 'flap', 0),
(68, 1, 2, 'shielding', 0),
(69, 1, 2, 'Infrared Technology and Application', 0),
(70, 1, 2, 'e-project cooperation', 0),
(71, 1, 2, 'Transformers', 0),
(72, 1, 2, 'laser devices', 0),
(73, 1, 2, 'electronic hardware', 0),
(74, 1, 2, 'industrial encoder', 0),
(75, 1, 2, 'copper clad laminate materials', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_productprices`
--

DROP TABLE IF EXISTS `pb_productprices`;
CREATE TABLE IF NOT EXISTS `pb_productprices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(1) NOT NULL DEFAULT 1,
  `product_id` int(10) NOT NULL DEFAULT -1,
  `brand_id` smallint(6) NOT NULL DEFAULT -1,
  `member_id` int(10) NOT NULL DEFAULT -1,
  `company_id` int(10) NOT NULL DEFAULT -1,
  `area_id` smallint(6) NOT NULL DEFAULT 0,
  `price_trends` tinyint(1) NOT NULL DEFAULT 0,
  `category_id` smallint(6) NOT NULL DEFAULT 0,
  `source` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `units` tinyint(1) NOT NULL DEFAULT 1,
  `currency` tinyint(1) NOT NULL DEFAULT 1,
  `price` float(9,2) NOT NULL DEFAULT 0.00,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_products`
--

DROP TABLE IF EXISTS `pb_products`;
CREATE TABLE IF NOT EXISTS `pb_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL DEFAULT 0,
  `cache_companyname` varchar(100) NOT NULL DEFAULT '',
  `sort_id` tinyint(1) NOT NULL DEFAULT 1,
  `brand_id` smallint(6) NOT NULL DEFAULT 0,
  `category_id` smallint(6) NOT NULL DEFAULT 0,
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `price` float(9,2) NOT NULL DEFAULT 0.00,
  `sn` varchar(20) NOT NULL DEFAULT '',
  `spec` varchar(20) NOT NULL DEFAULT '',
  `produce_area` varchar(50) NOT NULL DEFAULT '',
  `packing_content` varchar(100) NOT NULL DEFAULT '',
  `picture` varchar(50) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `producttype_id` smallint(6) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `ifnew` tinyint(1) NOT NULL DEFAULT 0,
  `ifcommend` tinyint(1) NOT NULL DEFAULT 0,
  `priority` tinyint(1) NOT NULL DEFAULT 0,
  `tag_ids` varchar(255) DEFAULT '',
  `clicked` smallint(6) NOT NULL DEFAULT 1,
  `formattribute_ids` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_products`
--

INSERT INTO `pb_products` (`id`, `member_id`, `company_id`, `cache_companyname`, `sort_id`, `brand_id`, `category_id`, `industry_id1`, `industry_id2`, `industry_id3`, `area_id1`, `area_id2`, `area_id3`, `name`, `price`, `sn`, `spec`, `produce_area`, `packing_content`, `picture`, `content`, `producttype_id`, `status`, `state`, `ifnew`, `ifcommend`, `priority`, `tag_ids`, `clicked`, `formattribute_ids`, `created`, `modified`) VALUES
(1, 1, 13, 'Ualink E-Commerce Inc.', 0, 0, 0, 5, 196, 0, 1, 0, 0, '32-inch HD LCD TV', 0.00, '', '', '', '', 'sample/product/1.jpg', NULL, 0, 1, 1, 0, 1, 0, '', 2, NULL, 1596019822, 1261978847),
(2, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 1, 0, 0, 'dv_sx_c10', 0.00, '', '', '', '', 'sample/product/2.jpg', NULL, 0, 1, 1, 0, 1, 0, NULL, 1, NULL, 1596019822, 1261992494),
(3, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 1, 0, 0, 'Electronic Dictionary', 0.00, '', '', '', '', 'sample/product/3.jpg', '', 0, 1, 1, 0, 1, 0, NULL, 3, NULL, 1596019822, 1261992725),
(4, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 1, 0, 0, 'e navigation', 0.00, '', '', '', '', 'sample/product/4.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 1, NULL, 1596019822, 1261992649),
(5, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 0, 0, 0, 'Value independence were Notebook', 0.00, '', '', '', '', 'sample/product/5.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 1, NULL, 1596019822, 0),
(6, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 0, 0, 0, 'MP4 video player', 0.00, '', '', '', '', 'sample/product/6.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 2, NULL, 1596019822, 0),
(7, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 0, 0, 0, 'Poor nobility HD 1080', 0.00, '', '', '', '', 'sample/product/7.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 1, NULL, 1596019822, 0),
(8, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 0, 0, 0, 'Can be back-type bag', 0.00, '', '', '', '', 'sample/product/8.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 3, NULL, 1596019822, 0),
(9, 1, 13, 'Ualink E-Commerce Inc.', 1, 0, 0, 1, 0, 0, 1, 24, 0, '10 million pixel digital camera', 0.00, '', '', '', '', 'sample/product/9.jpg', NULL, 0, 1, 1, 0, 0, 0, NULL, 1, NULL, 1596019822, 0),
(10, 1, 13, '友邻电子商务网', 1, 0, 0, 1, 0, 0, 1, 28, 0, 'Fashion DV', 0.00, '', '', '', '', 'sample/product/10.jpg', NULL, 0, 1, 1, 0, 1, 0, NULL, 12, NULL, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_productsorts`
--

DROP TABLE IF EXISTS `pb_productsorts`;
CREATE TABLE IF NOT EXISTS `pb_productsorts` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_productsorts`
--

INSERT INTO `pb_productsorts` (`id`, `name`, `display_order`) VALUES
(1, 'New Products', 0),
(2, 'Stock', 0),
(3, 'General Products', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_producttypes`
--

DROP TABLE IF EXISTS `pb_producttypes`;
CREATE TABLE IF NOT EXISTS `pb_producttypes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `name` varchar(25) NOT NULL DEFAULT '',
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_quotes`
--

DROP TABLE IF EXISTS `pb_quotes`;
CREATE TABLE IF NOT EXISTS `pb_quotes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL DEFAULT -1,
  `market_id` smallint(6) NOT NULL DEFAULT -1,
  `type_id` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `area_id` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `max_price` float(9,2) NOT NULL DEFAULT 0.00,
  `min_price` float(9,2) NOT NULL DEFAULT 0.00,
  `units` tinyint(1) NOT NULL DEFAULT 1,
  `currency` tinyint(1) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_quotetypes`
--

DROP TABLE IF EXISTS `pb_quotetypes`;
CREATE TABLE IF NOT EXISTS `pb_quotetypes` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_roleadminers`
--

DROP TABLE IF EXISTS `pb_roleadminers`;
CREATE TABLE IF NOT EXISTS `pb_roleadminers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `adminrole_id` int(2) DEFAULT NULL,
  `adminer_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_roleprivileges`
--

DROP TABLE IF EXISTS `pb_roleprivileges`;
CREATE TABLE IF NOT EXISTS `pb_roleprivileges` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `adminrole_id` int(2) DEFAULT NULL,
  `adminprivilege_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_segmentcaches`
--

DROP TABLE IF EXISTS `pb_segmentcaches`;
CREATE TABLE IF NOT EXISTS `pb_segmentcaches` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `data` text DEFAULT NULL,
  `display_order` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_segmentdicts`
--

DROP TABLE IF EXISTS `pb_segmentdicts`;
CREATE TABLE IF NOT EXISTS `pb_segmentdicts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `word` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_services`
--

DROP TABLE IF EXISTS `pb_services`;
CREATE TABLE IF NOT EXISTS `pb_services` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `title` varchar(25) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `nick_name` varchar(25) DEFAULT '',
  `email` varchar(25) NOT NULL DEFAULT '',
  `user_ip` varchar(11) NOT NULL DEFAULT '',
  `type_id` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  `revert_content` text DEFAULT NULL,
  `revert_date` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_services`
--

INSERT INTO `pb_services` (`id`, `member_id`, `title`, `content`, `nick_name`, `email`, `user_ip`, `type_id`, `status`, `created`, `modified`, `revert_content`, `revert_date`) VALUES
(1, 0, 'views and recommendations', 'hjh', '', 'buyitgh@gmail.com', 'unknown', 0, 0, 1597359454, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_sessions`
--

DROP TABLE IF EXISTS `pb_sessions`;
CREATE TABLE IF NOT EXISTS `pb_sessions` (
  `sesskey` char(32) NOT NULL DEFAULT '',
  `expiry` int(10) NOT NULL DEFAULT 0,
  `expireref` char(64) NOT NULL DEFAULT '',
  `data` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  KEY `sess2_expiry` (`expiry`),
  KEY `sess2_expireref` (`expireref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_settings`
--

DROP TABLE IF EXISTS `pb_settings`;
CREATE TABLE IF NOT EXISTS `pb_settings` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(1) NOT NULL DEFAULT 0,
  `variable` varchar(150) NOT NULL DEFAULT '',
  `valued` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `variable` (`variable`)
) ENGINE=MyISAM AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_settings`
--

INSERT INTO `pb_settings` (`id`, `type_id`, `variable`, `valued`) VALUES
(58, 0, 'site_name', 'WTC Virtual Trade Portal'),
(59, 0, 'site_title', 'WTC Virtual Trade Portal - Powered By PHPB2B'),
(3, 0, 'site_banner_word', 'The most professional e-commerce website industry'),
(73, 0, 'company_name', 'Web site copyright'),
(61, 0, 'site_url', 'http://localhost/wtc_b2b_new/'),
(6, 0, 'icp_number', 'ICP Number'),
(74, 0, 'service_tel', '0302631437'),
(75, 0, 'sale_tel', '0302631437'),
(76, 0, 'service_qq', '0302631437'),
(77, 0, 'service_msn', 'info@wtcaccra.com'),
(78, 0, 'service_email', 'info@wtcaccra.com'),
(12, 1, 'site_description', 'Site detailed description of the'),
(13, 0, 'cp_picture', '0'),
(14, 0, 'register_picture', '0'),
(15, 0, 'login_picture', '0'),
(16, 0, 'vispost_auth', '1'),
(17, 0, 'watermark', '1'),
(62, 0, 'watertext', 'http://localhost/wtc_b2b_new/'),
(19, 0, 'watercolor', '#990000'),
(20, 0, 'add_market_check', '1'),
(21, 0, 'regcheck', '0'),
(22, 0, 'vis_post', '1'),
(23, 0, 'vis_post_check', '1'),
(24, 0, 'sell_logincheck', '1'),
(25, 0, 'buy_logincheck', '0'),
(57, 0, 'install_dateline', '1596019810'),
(27, 0, 'last_backup', '1596019821'),
(119, 0, 'smtp_server', 'smtp.office365.com'),
(120, 0, 'smtp_port', '587'),
(121, 0, 'smtp_auth', '1'),
(122, 0, 'mail_from', 'amensah@wtcaccra.com'),
(123, 0, 'mail_fromwho', 'Amos Mensah'),
(124, 0, 'auth_username', 'amensah@wtcaccra.com'),
(126, 0, 'auth_password', 'M$3sah2#'),
(118, 0, 'send_mail', '2'),
(36, 0, 'sendmail_silent', '1'),
(37, 0, 'mail_delimiter', '0'),
(38, 0, 'reg_filename', 'register.php'),
(131, 0, 'new_userauth', '0'),
(40, 0, 'post_filename', 'post.php'),
(41, 0, 'forbid_ip', ''),
(132, 0, 'ip_reg_sep', '0'),
(60, 0, 'backup_dir', 'cAJTfy'),
(44, 0, 'capt_logging', '0'),
(45, 0, 'capt_register', '1'),
(46, 0, 'capt_post_free', '0'),
(47, 0, 'capt_add_market', '1'),
(48, 0, 'capt_login_admin', '0'),
(49, 0, 'capt_apply_friendlink', '1'),
(50, 0, 'capt_service', '1'),
(51, 0, 'backup_type', '1'),
(130, 0, 'register_type', 'open_common_reg'),
(63, 0, 'auth_key', 't8bdx!3q'),
(54, 0, 'keyword_bidding', '0'),
(55, 0, 'passport_support', '0'),
(56, 0, 'site_logo', 'images/logo.jpg'),
(66, 0, 'languages', 'a:1:{s:2:\"en\";a:2:{s:5:\"title\";s:7:\"English\";s:3:\"img\";s:21:\"languages/en/icon.gif\";}}'),
(133, 0, 'main_cache', '0'),
(134, 0, 'member_cache', '0'),
(135, 0, 'space_cache', '0'),
(136, 0, 'label_cache', '0'),
(137, 0, 'main_cache_lifetime', '3600'),
(138, 0, 'main_cache_check', '0'),
(91, 1, 'update_alert_type', '0'),
(92, 1, 'update_alert_lasttime', '1597418635'),
(128, 1, 'agreement', ''),
(129, 1, 'welcome_msg', '0'),
(125, 0, 'auth_protocol', 'TLS'),
(127, 0, 'testemail', 'buyitgh@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pb_spacecaches`
--

DROP TABLE IF EXISTS `pb_spacecaches`;
CREATE TABLE IF NOT EXISTS `pb_spacecaches` (
  `cache_spacename` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(10) NOT NULL DEFAULT -1,
  `data1` text NOT NULL,
  `data2` text NOT NULL,
  `expiration` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_spacelinks`
--

DROP TABLE IF EXISTS `pb_spacelinks`;
CREATE TABLE IF NOT EXISTS `pb_spacelinks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `display_order` smallint(3) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `is_outlink` tinyint(1) NOT NULL DEFAULT 0,
  `description` varchar(100) NOT NULL DEFAULT '',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `highlight` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_spacelinks`
--

INSERT INTO `pb_spacelinks` (`id`, `member_id`, `company_id`, `display_order`, `title`, `url`, `is_outlink`, `description`, `logo`, `highlight`, `created`) VALUES
(1, 2, 3, 0, 'Dongguan Chong Yi Plastics Co', 'space.php?userid=', 0, '', '', 0, 1600270930),
(2, 2, 3, 0, 'Dongguan Chong Yi Plastics Co', 'space.php?userid=', 0, '', '', 0, 1600271008);

-- --------------------------------------------------------

--
-- Table structure for table `pb_spreads`
--

DROP TABLE IF EXISTS `pb_spreads`;
CREATE TABLE IF NOT EXISTS `pb_spreads` (
  `keyword_id` int(10) NOT NULL,
  `target_id` int(10) NOT NULL,
  `type_name` enum('trades','companies','newses','products') NOT NULL DEFAULT 'trades',
  `expiration` int(10) NOT NULL DEFAULT 0,
  `display_order` tinyint(1) NOT NULL,
  PRIMARY KEY (`keyword_id`),
  KEY `spread` (`keyword_id`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_standards`
--

DROP TABLE IF EXISTS `pb_standards`;
CREATE TABLE IF NOT EXISTS `pb_standards` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `attachment_id` smallint(6) NOT NULL DEFAULT 0,
  `type_id` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(255) NOT NULL,
  `digest` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `publish_time` int(10) NOT NULL DEFAULT 0,
  `force_time` int(10) NOT NULL DEFAULT 0,
  `clicked` smallint(6) NOT NULL DEFAULT 1,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_standardtypes`
--

DROP TABLE IF EXISTS `pb_standardtypes`;
CREATE TABLE IF NOT EXISTS `pb_standardtypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_stats`
--

DROP TABLE IF EXISTS `pb_stats`;
CREATE TABLE IF NOT EXISTS `pb_stats` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `sa` varchar(25) DEFAULT '',
  `sb` varchar(50) DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  `sc` int(10) DEFAULT NULL,
  `sd` int(10) DEFAULT NULL,
  `se` smallint(6) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_tags`
--

DROP TABLE IF EXISTS `pb_tags`;
CREATE TABLE IF NOT EXISTS `pb_tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `numbers` smallint(6) NOT NULL DEFAULT 0,
  `closed` tinyint(1) NOT NULL DEFAULT 0,
  `flag` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `title` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_templets`
--

DROP TABLE IF EXISTS `pb_templets`;
CREATE TABLE IF NOT EXISTS `pb_templets` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL DEFAULT '',
  `title` varchar(25) NOT NULL DEFAULT '',
  `directory` varchar(100) NOT NULL DEFAULT '',
  `type` enum('system','user') NOT NULL DEFAULT 'system',
  `author` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `require_membertype` varchar(100) NOT NULL DEFAULT '0',
  `require_membergroups` varchar(100) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_templets`
--

INSERT INTO `pb_templets` (`id`, `name`, `title`, `directory`, `type`, `author`, `style`, `description`, `is_default`, `require_membertype`, `require_membergroups`, `status`) VALUES
(3, 'default', '默认企业模板', 'skins/default/', 'user', 'PB TEAM', '', 'A PHPB2B Corperate Templet. Enjoy!', 1, '0', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pb_topicnews`
--

DROP TABLE IF EXISTS `pb_topicnews`;
CREATE TABLE IF NOT EXISTS `pb_topicnews` (
  `topic_id` smallint(6) NOT NULL DEFAULT 0,
  `news_id` smallint(6) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_topics`
--

DROP TABLE IF EXISTS `pb_topics`;
CREATE TABLE IF NOT EXISTS `pb_topics` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `picture` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_topics`
--

INSERT INTO `pb_topics` (`id`, `title`, `picture`, `description`, `created`, `modified`) VALUES
(1, 'Explore the mysterious red planet Mars', 'sample/news/topic1.jpg', '', 1596019822, 0),
(2, 'Volcanic eruption in southern Iceland again', 'sample/news/topic2.jpg', '', 1596019822, 0),
(3, 'Explore the whole Earth Hour Live ', 'sample/news/topic3.jpg', '', 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_tradefields`
--

DROP TABLE IF EXISTS `pb_tradefields`;
CREATE TABLE IF NOT EXISTS `pb_tradefields` (
  `trade_id` int(10) NOT NULL DEFAULT 0,
  `member_id` int(10) NOT NULL DEFAULT 0,
  `link_man` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `company_name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `prim_tel` tinyint(1) NOT NULL DEFAULT 0,
  `prim_telnumber` varchar(25) NOT NULL DEFAULT '',
  `prim_im` tinyint(1) NOT NULL DEFAULT 0,
  `prim_imaccount` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`trade_id`),
  UNIQUE KEY `trade_id` (`trade_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_trades`
--

DROP TABLE IF EXISTS `pb_trades`;
CREATE TABLE IF NOT EXISTS `pb_trades` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` enum('8','7','6','5','4','3','2','1') NOT NULL DEFAULT '1',
  `industry_id1` smallint(6) NOT NULL DEFAULT 0,
  `industry_id2` smallint(6) NOT NULL DEFAULT 0,
  `industry_id3` smallint(6) NOT NULL DEFAULT 0,
  `area_id1` smallint(6) NOT NULL DEFAULT 0,
  `area_id2` smallint(6) NOT NULL DEFAULT 0,
  `area_id3` smallint(6) NOT NULL DEFAULT 0,
  `member_id` int(10) NOT NULL DEFAULT 0,
  `company_id` int(5) NOT NULL DEFAULT 0,
  `cache_username` varchar(25) NOT NULL DEFAULT '',
  `cache_companyname` varchar(100) NOT NULL DEFAULT '',
  `cache_contacts` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `price` float(9,2) NOT NULL DEFAULT 0.00,
  `measuring_unit` varchar(15) NOT NULL DEFAULT '0',
  `monetary_unit` varchar(15) NOT NULL DEFAULT '0',
  `packing` varchar(150) NOT NULL DEFAULT '',
  `quantity` varchar(25) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `display_expiration` int(10) NOT NULL DEFAULT 0,
  `spec` varchar(200) NOT NULL DEFAULT '',
  `sn` varchar(25) NOT NULL DEFAULT '',
  `picture` varchar(50) NOT NULL DEFAULT '',
  `picture_remote` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT 0,
  `submit_time` int(10) NOT NULL DEFAULT 0,
  `expire_time` int(10) NOT NULL DEFAULT 0,
  `expire_days` int(3) NOT NULL DEFAULT 0,
  `if_commend` tinyint(1) NOT NULL DEFAULT 0,
  `if_urgent` enum('0','1') NOT NULL DEFAULT '0',
  `if_locked` enum('0','1') NOT NULL DEFAULT '0',
  `require_point` smallint(6) NOT NULL DEFAULT 0,
  `require_membertype` smallint(6) NOT NULL DEFAULT 0,
  `require_freedate` int(10) NOT NULL DEFAULT 0,
  `ip_addr` varchar(15) NOT NULL DEFAULT '',
  `clicked` int(10) NOT NULL DEFAULT 1,
  `tag_ids` varchar(255) DEFAULT '',
  `formattribute_ids` text DEFAULT NULL,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_trades`
--

INSERT INTO `pb_trades` (`id`, `type_id`, `industry_id1`, `industry_id2`, `industry_id3`, `area_id1`, `area_id2`, `area_id3`, `member_id`, `company_id`, `cache_username`, `cache_companyname`, `cache_contacts`, `title`, `content`, `price`, `measuring_unit`, `monetary_unit`, `packing`, `quantity`, `display_order`, `display_expiration`, `spec`, `sn`, `picture`, `picture_remote`, `status`, `submit_time`, `expire_time`, `expire_days`, `if_commend`, `if_urgent`, `if_locked`, `require_point`, `require_membertype`, `require_freedate`, `ip_addr`, `clicked`, `tag_ids`, `formattribute_ids`, `created`, `modified`) VALUES
(1, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Buy double-door stainless steel table ', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/12.jpg', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 3, '', NULL, 1596019822, 0),
(2, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Buy rice, peanuts, red bean corn', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(3, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Estonian businessmen purchase freshwater bass', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(4, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'SLEEPCOCompany Buy', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 2, '', NULL, 1596019822, 0),
(5, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Buy camphor pine recruits and', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(6, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Buy watermelon', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(7, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Asian business Chinese-made metal furniture Buy', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(8, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Nepal Tobacco tender', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(9, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Asian business Chinese-made food packaging equipment Buy', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(10, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Asia Business Buy China fabric', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/8.jpg', '', 1, 1596019822, 1596106222, 0, 1, '1', '0', 0, 0, 0, '', 3, '', NULL, 1596019822, 0),
(11, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Asia Business Buy computer parts made in China', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/9.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(12, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Libyan businessmen interested in buying Chinese-made French fries production equipment', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/11.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(13, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'The supply of Japanese larch seedlings', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/12.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 3, '', NULL, 1596019822, 0),
(14, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Supply all kinds of flowers', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(15, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Aspen Trust Group to seek cooperation with Chinese companies', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(16, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Find buyers honey', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(17, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Croatia yacht company wants to sell the yacht', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(18, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Israeli fruits and vegetables and agricultural companies to seek domestic buyers', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(19, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Section 1 cocoa processing companies to seek buyers in China', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 2, '', NULL, 1596019822, 0),
(20, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Ireland ECOBUILD Exhibition', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(21, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'You kind of young fruit tree supply Taiwan', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', '', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(22, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Supply the entire paragraph 7 yuan T shirt wholesale clothing inventory', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/7.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 5, '', NULL, 1596019822, 0),
(23, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Promotional Wooden Comb ', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/6.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(24, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Sell Aida Arm Chairs ', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/5.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 2, '', NULL, 1596019822, 0),
(25, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Vacuum Dehydrator For Emulsified', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/4.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 3, '', NULL, 1596019822, 0),
(26, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Samsung Cartridge Chips ', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/3.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(27, '2', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Sell Polarized Lens', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/2.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(28, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Buy Cattle Fence From', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/12.jpg', '', 1, 1596019822, 1596106222, 0, 0, '0', '0', 0, 0, 0, '', 1, '', NULL, 1596019822, 0),
(29, '1', 0, 0, 0, 0, 0, 0, 1, 13, '', '', '', 'Bosch Engine', NULL, 0.00, '0', '0', '', '', 0, 0, '', '', 'sample/offer/1.jpg', '', 1, 1597422744, 1597422744, 0, 0, '0', '0', 0, 0, 0, '', 8, '', NULL, 1596019822, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_tradetypes`
--

DROP TABLE IF EXISTS `pb_tradetypes`;
CREATE TABLE IF NOT EXISTS `pb_tradetypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(3) NOT NULL DEFAULT 0,
  `name` varchar(25) NOT NULL DEFAULT '',
  `level` tinyint(1) NOT NULL DEFAULT 1,
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_tradetypes`
--

INSERT INTO `pb_tradetypes` (`id`, `parent_id`, `name`, `level`, `display_order`) VALUES
(1, 0, 'Buy', 1, 0),
(2, 0, 'Sell', 1, 0),
(3, 0, 'Proxy', 1, 0),
(4, 0, 'Cooperation', 1, 0),
(5, 0, 'Merchants', 1, 0),
(6, 0, 'Join', 1, 0),
(7, 0, 'Wholesale', 1, 0),
(8, 0, 'Stocks', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pb_trustlogs`
--

DROP TABLE IF EXISTS `pb_trustlogs`;
CREATE TABLE IF NOT EXISTS `pb_trustlogs` (
  `member_id` int(10) NOT NULL AUTO_INCREMENT,
  `trusttype_id` smallint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_trusttypes`
--

DROP TABLE IF EXISTS `pb_trusttypes`;
CREATE TABLE IF NOT EXISTS `pb_trusttypes` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_trusttypes`
--

INSERT INTO `pb_trusttypes` (`id`, `name`, `description`, `image`, `display_order`, `status`) VALUES
(2, 'Qualification Certification', NULL, 'company.gif', 0, 1),
(1, 'Real-name authentication', NULL, 'truename.gif', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pb_typemodels`
--

DROP TABLE IF EXISTS `pb_typemodels`;
CREATE TABLE IF NOT EXISTS `pb_typemodels` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `type_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_typemodels`
--

INSERT INTO `pb_typemodels` (`id`, `title`, `type_name`) VALUES
(1, 'expiration time', 'offer_expire'),
(2, 'Type', 'manage_type'),
(3, 'major markets', 'main_market'),
(4, 'registered capital', 'reg_fund'),
(5, 'turnover', 'year_annual'),
(6, 'economy type', 'economic_type'),
(7, 'moderation status', 'check_status'),
(8, 'employees', 'employee_amount'),
(9, 'status', 'common_status'),
(10, 'the proposed type', 'service_type'),
(11, 'educational experience', 'education'),
(12, 'wages', 'salary'),
(13, 'the nature', 'work_type'),
(14, 'Job Title', 'position'),
(15, 'gender', 'gender'),
(16, 'Phone Type', 'phone_type'),
(17, 'instant messaging category', 'im_type'),
(18, 'option', 'common_option'),
(19, 'honorific', 'calls'),
(20, 'units', 'measuring'),
(21, 'currency', 'monetary'),
(22, 'quote type', 'price_type'),
(23, 'price trend', 'price_trends');

-- --------------------------------------------------------

--
-- Table structure for table `pb_typeoptions`
--

DROP TABLE IF EXISTS `pb_typeoptions`;
CREATE TABLE IF NOT EXISTS `pb_typeoptions` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `typemodel_id` smallint(3) NOT NULL DEFAULT 0,
  `option_value` varchar(50) NOT NULL DEFAULT '',
  `option_label` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_typeoptions`
--

INSERT INTO `pb_typeoptions` (`id`, `typemodel_id`, `option_value`, `option_label`) VALUES
(1, 1, '10', '10 days'),
(2, 1, '30', 'month'),
(3, 1, '90', 'three'),
(4, 1, '180', 'six'),
(5, 2, '1', 'production'),
(6, 2, '2', 'trade type'),
(7, 2, '3', 'service'),
(8, 2, '4', 'the Government or other agencies'),
(9, 3, '1', 'China'),
(10, 3, '2', 'Hong Kong, Macao and Taiwan'),
(11, 3, '3', 'North America'),
(12, 3, '4', 'South America'),
(13, 3, '5', 'Europe'),
(14, 3, '6', 'Asia'),
(15, 3, '7', 'Africa'),
(16, 3, '8', 'Oceania'),
(17, 3, '9', 'other market'),
(18, 4, '0', 'closed'),
(19, 4, '1', 'one hundred thousand yuan less'),
(20, 4, '2', 'RMB 10-30 million'),
(21, 4, '3', 'RMB 30-50 million'),
(22, 4, '4', 'RMB 50-100 million'),
(23, 4, '5', 'RMB 100-300 million'),
(24, 4, '6', 'RMB 300-500 million'),
(25, 4, '7', 'RMB 500-1,000 million'),
(26, 4, '8', 'million RMB 1000-5000'),
(27, 4, '9', 'more than RMB 50 million'),
(28, 4, '10', 'other'),
(29, 5, '1', 'RMB 10 million or less/year'),
(30, 5, '2', 'RMB 10-30 million/year'),
(31, 5, '3', 'RMB 30-50 million/year'),
(32, 5, '4', 'RMB 50-100 million/year'),
(33, 5, '5', 'RMB 100-300 million/year'),
(34, 5, '6', 'RMB 300-500 million/year'),
(35, 5, '7', 'RMB 500-1,000 million/year'),
(36, 5, '8', 'RMB 1000-5000 million/year'),
(37, 5, '9', 'more than 50 million RMB/year'),
(38, 5, '10', 'other'),
(39, 6, '1', 'state-owned enterprises'),
(40, 6, '2', 'collective enterprises'),
(41, 6, '3', 'Corporations'),
(42, 6, '4', 'joint venture'),
(43, 6, '5', 'limited liability company'),
(44, 6, '6', 'Corporation'),
(45, 6, '7', 'private'),
(46, 6, '8', 'individual enterprise'),
(47, 6, '9', 'non-profit organization'),
(48, 6, '10', 'other'),
(49, 7, '0', 'invalid'),
(50, 7, '1', 'effective'),
(51, 7, '2', 'awaiting approval'),
(52, 7, '3', 'audit is not passed'),
(53, 8, '1', '5 less'),
(54, 8, '2', '5-10 people'),
(55, 8, '3', '11-50 people'),
(56, 8, '4', '51-100 people'),
(57, 8, '5', '101-500 persons'),
(58, 8, '6', '501-1000 person'),
(59, 8, '7', '1000 or more'),
(60, 10, '1', 'consultation'),
(61, 10, '2', 'proposal'),
(62, 10, '3', 'complaints'),
(63, 11, '0', 'other'),
(64, 11, '-1', 'not required'),
(65, 11, '-2', 'open'),
(66, 11, '1', 'Doctor'),
(67, 11, '2', 'Master'),
(68, 11, '3', 'undergraduate'),
(69, 11, '4', 'college'),
(70, 11, '5', 'secondary'),
(71, 11, '6', 'technical school'),
(72, 11, '7', 'high'),
(73, 11, '8', 'middle'),
(74, 11, '9', 'primary'),
(75, 12, '0', 'no choice'),
(76, 12, '-1', 'Interview'),
(77, 12, '1', '1500 less'),
(78, 12, '2', '1500-1999 RMB/month'),
(79, 12, '3', '2000-2999 yuan/month'),
(80, 12, '4', '3000-4999 yuan/month'),
(81, 12, '5', '5000 above'),
(82, 13, '0', 'no choice'),
(83, 13, '1', 'full'),
(84, 13, '2', 'part-time'),
(85, 13, '3', 'provisional'),
(86, 13, '4', 'practice'),
(87, 13, '5', 'other'),
(88, 14, '0', 'no choice'),
(89, 14, '1', 'chairman, president and deputies'),
(90, 14, '2', 'the executive branch managers/executives'),
(91, 14, '3', 'technical manager/technical staff'),
(92, 14, '4', 'production manager/production staff'),
(93, 14, '5', 'marketing manager/marketing staff'),
(94, 14, '6,', 'purchasing department manager/procurement officer'),
(95, 14, '7', 'sales manager/sales'),
(96, 14, '8', 'other'),
(97, 15, '0', 'no choice'),
(98, 15, '1', 'Male'),
(99, 15, '2', 'Female'),
(100, 15, '-1', 'open'),
(101, 16, '1', 'mobile phone'),
(102, 16, '2', 'residential'),
(103, 16, '3', 'business phone'),
(104, 16, '4', 'other'),
(105, 17, '1', 'QQ'),
(106, 17, '2', 'ICQ'),
(107, 17, '3', 'MSN Messenger'),
(108, 17, '4', 'Yahoo Messenger'),
(109, 17, '5', 'Skype'),
(110, 17, '6', 'other'),
(111, 17, '0', 'no choice'),
(112, 16, '0', 'no choice'),
(113, 6, '0', 'no choice'),
(114, 9, '0', 'invalid'),
(115, 9, '1', 'effective'),
(116, 18, '0', 'no'),
(117, 18, '1', 'yes'),
(118, 19, '1', 'Mr.'),
(119, 19, '2', 'Ms.'),
(120, 20, '1', 'single'),
(121, 20, '2', 'pieces'),
(122, 21, '1', 'element'),
(123, 21, '3', 'USD'),
(124, 22, '1', 'buy'),
(125, 22, '2', 'sell'),
(126, 23, '1', 'up'),
(127, 23, '2', 'stable'),
(128, 23, '3', 'down'),
(129, 23, '4', 'uncertain'),
(130, 21, '2', 'million');

-- --------------------------------------------------------

--
-- Table structure for table `pb_userpages`
--

DROP TABLE IF EXISTS `pb_userpages`;
CREATE TABLE IF NOT EXISTS `pb_userpages` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `templet_name` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `digest` varchar(50) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `url` varchar(100) NOT NULL DEFAULT '',
  `display_order` tinyint(1) NOT NULL DEFAULT 0,
  `created` int(10) NOT NULL DEFAULT 0,
  `modified` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pb_userpages`
--

INSERT INTO `pb_userpages` (`id`, `templet_name`, `name`, `title`, `digest`, `content`, `url`, `display_order`, `created`, `modified`) VALUES
(1, '', 'aboutus', 'About Us', '', 'Note on the website', '', 0, 1596019821, 1596019821),
(2, '', 'contactus', 'Contact us', '', 'Contact', '', 0, 1596019821, 1596019821),
(3, '', 'aboutads', 'Advertising', '', 'Description of advertising and price', '', 0, 1596019821, 1596019821),
(4, '', 'sitemap', 'Site Map', '', 'Web Site Map', 'sitemap.php', 0, 1596019821, 1596019821),
(5, '', 'agreement', 'Legal Notices', '', 'Legal Notices', 'agreement.php', 0, 1596019821, 0),
(6, '', 'friendlink', 'Links', '', 'Application Links', 'friendlink.php', 0, 1596019821, 0),
(7, '', 'help', 'Help Center', '', 'Help Center', '', 0, 1596019821, 1596019821),
(8, '', 'service', 'Comments complaints', '', 'Comments and suggestions, complaints', '', 0, 1596019821, 1596019821),
(9, '', 'special', 'Sub-station project', '', 'Industry or regional sub-stations', 'special/', 0, 1596019821, 1596019821);

-- --------------------------------------------------------

--
-- Table structure for table `pb_visitlogs`
--

DROP TABLE IF EXISTS `pb_visitlogs`;
CREATE TABLE IF NOT EXISTS `pb_visitlogs` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `salt` varchar(32) NOT NULL DEFAULT '',
  `date_line` varchar(15) NOT NULL DEFAULT '',
  `type_name` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `salt` (`salt`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pb_words`
--

DROP TABLE IF EXISTS `pb_words`;
CREATE TABLE IF NOT EXISTS `pb_words` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `replace_to` varchar(50) NOT NULL DEFAULT '',
  `expiration` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `word` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
