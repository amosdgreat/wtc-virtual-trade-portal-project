<?php
/**
 * The Name:English
 * Description:Space
 */
$arrTemplate['_space_offer'] = "Supply and demand information";
$arrTemplate['_space_product'] = "Products";
$arrTemplate['_space_news'] = "News";
$arrTemplate['_space_hr'] = "Recruitment";
$arrTemplate['_space_album'] = "Company album";
$arrTemplate['_space_honour'] = "Honor";
$arrTemplate['_space_intro'] = "About Us";
$arrTemplate['_pms_from_space'] = "This is your home page message received";
$arrTemplate['_undefined_image'] = "Undefined";
$arrTemplate['_contact_us'] = "Contact us";
$arrTemplate['_feed_back_to_company'] = "Message to the enterprise";
$arrTemplate['_you_are_welcome'] = "Welcome";
?>