<?php
/**
 * The Name:简体中文
 * Description:邮件
 */
$arrTemplate['_get_passwd_subject'] = "取回密码说明";
$arrTemplate['_thx_for_your_reg'] = "感谢您注册{1}";
$arrTemplate['_after_update_please'] = "更新密码后,请您及时到";
$arrTemplate['_office_room_update_passwd'] = "商务室更新您的密码,以防丢失.";
$arrTemplate['_dear_user'] = "尊敬的客户";
$arrTemplate['_a_test_email'] = "这是一封来自{1}的测试邮件。";
$arrTemplate['_a_test_email_delete'] = "这是一封来自<a href=\"".URL."\" target='_blank'>{1}</a>的测试邮件，你可以删除它。";
$arrTemplate['_pls_active_your_account'] = "请激活您的帐号";
?>